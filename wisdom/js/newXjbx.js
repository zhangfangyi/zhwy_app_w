mui.plusReady(function() {
 	 var btnCode = plus.webview.currentWebview().btnCode;  
 	 var MenuType = plus.webview.currentWebview().xjMenuType;

	 Typepicker();
 	 addImg();
 	 submit(btnCode,MenuType);
 	 
 	 
 	 mui.init({
			swipeBack: true ,//启用右滑关闭功能
			beforeback: function() {
		        var list = plus.webview.currentWebview().opener();
		        mui.fire(list, 'wybxrefresh',{
		        	type:MenuType,
		        });
		        return true;
			   }
		});
			var deceleration = mui.os.ios ? 0.003 : 0.0009;
			mui('.mui-scroll-wrapper').scroll({
				deceleration: deceleration,
				indicators: false
			});	
			//返回按钮
			mui('.smailNav').on('tap', '.back', function(e) {
				mui.back();
			});
 	 
 
 	 
});	 


function Typepicker(){
		mui.ajax(ajaxDomain + "/wordbook/getWordBookByCode.do",{
					data:{
						 code : 3001
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						 var setData= [];
						 var data = data.data;
							if(data.length>0){
//								$(".typeQT").text(data[0].name).attr('typeId',data[0].id);
								for(var i=0;i<data.length;i++){
									var obj = {};
									obj["value"] = data[i].name;
									obj["text"] = data[i].name;
									obj['typeId'] = data[i].id;
									var cop = $.extend(true,{},data[i],obj);
									setData.push(cop);
								}
							}
						mui('.xjbxlx').on('tap', '.typeQT', function(e) {
		 						var picker = new mui.PopPicker();
            					picker.setData(setData);
            					picker.show(function (selectItems) {
								   $(".typeQT").text(selectItems[0].value).attr('typeId',selectItems[0].typeId);
    							})
						});
						 
					},
					error:function(xhr,type,errorThrown){
						mui.toast('网络无法连接', {
								duration: 'long', 
								type: 'div'
							});
					}
			})
}	

//添加图片
var isTjiao = true;	
var imagesList = [];
function addImg(){
	mui(".imgBox").on("tap", "#itemImg", function(e) {
		var imgLength = $(".imgBox img").size();
		if(imgLength > 2){
				mui.toast('最多添加2张图片', {
						duration: 'long', 
						type: 'div'
					});
			return false;
		}
			if(mui.os.plus){
				var a = [{
					title: "拍照"
				}, {
					title: "从手机相册选择"
				}];
				plus.nativeUI.actionSheet({
					title: "添加图片",
					cancel: "取消",
					buttons: a
				}, function(b) {
					switch (b.index) {
						case 0:
							break;
						case 1:
							getImage();
							break;
						case 2:
							galleryImg();
							break;
						default:
							break
					}
				})	
			}
			
		});
}

 function getImage() {
					var c = plus.camera.getCamera();
					c.captureImage(function(e) {  
						plus.io.resolveLocalFileSystemURL(e, function(entry) {
							var s = entry.toLocalURL() + "?version=" + new Date().getTime();
							var imgCop = $("<div></div>").addClass("imgCop").css({
								"position":"relative",
								/*"background":"red",*/
							});
							var colseB = $("<div style='' class='closeBtn'></div>");
							imgCop.html(colseB);
							var img = $('<img/>').attr({"src":s});
							img.css({"margin-left":"0"});
							imgCop.append(img);
							var bgDiv = $("<div/>").css({
								"opacity":0.5,
								"width":"100%",
								"height":"100%",
								"position":"absolute",
								"left":"0",
								"top":"0.2rem",
								"background":"#555",
								"z-index":2,
								"line-height":"100%",
								"text-align":"center",
								"color":"#fff",
								"line-height":"1.5rem"
							}).html("正在上传").addClass("bgDiv");
							imgCop.append(bgDiv);
							isTjiao = false;
							$(".imgBox").prepend(imgCop);
							uploadHead(s,img);
							img.hover(
			         			function(e){
			         				e.stopPropagation();
			         				$(this).siblings(".closeBtn").show();
			         			},function(e){
			         				e.stopPropagation();
			         				$(this).siblings(".closeBtn").hide();
			         			}
			         		);
			         		colseB.on('tap',function(e){
			         			e.stopPropagation();
			         			var result= confirm("是否删除此图片？");
			         			if(result){
			         				var imgId = $(this).siblings("img").attr("imgId");
			         				deleteById(imgId);
				         			for(var i=0;i<imagesList.length;i++){
				         				if(imgId==imagesList[i]){
				         					imagesList = imagesList.splice(i,1);
				         				}
				         			}
				         			$(this).parents(".imgCop").remove();
			         			}
			         		})
						}, function(e) {
							mui.toast("读取拍照文件错误");
						});
					}, function(s) {
						mui.toast("读取拍照文件错误");
					}, {
						filename: "_doc/img" + new Date().getTime()
					});
					//
				}


	function galleryImg() {
			plus.gallery.pick(function(a) {
				plus.io.resolveLocalFileSystemURL(a, function(entry) { 
							var s = entry.toLocalURL() + "?version=" + new Date().getTime();
							var imgCop = $("<div></div>").addClass("imgCop").css({
								"position":"relative",
								/*"background":"red",*/
							});
							var colseB = $("<div style='' class='closeBtn'></div>");
							imgCop.html(colseB);
							var img = $('<img/>').attr({"src":s});
							img.css({"margin-left":"0"});
							imgCop.append(img);
							var bgDiv = $("<div/>").css({
								"opacity":0.5,
								"width":"100%",
								"height":"100%",
								"position":"absolute",
								"left":"0",
								"top":"0.2rem",
								"background":"#555",
								"z-index":2,
								"line-height":"100%",
								"text-align":"center",
								"color":"#fff",
								"line-height":"1.5rem"
							}).html("正在上传").addClass("bgDiv");
							imgCop.append(bgDiv);
							isTjiao = false;
							$(".imgBox").prepend(imgCop);
							uploadHead(s,img);
							img.hover(
			         			function(e){
			         				e.stopPropagation();
			         				$(this).siblings(".closeBtn").show();
			         			},function(e){
			         				e.stopPropagation();
			         				$(this).siblings(".closeBtn").hide();
			         			}
			         		);
			         		colseB.on('tap',function(e){
			         			e.stopPropagation();
			         			var result= confirm("是否删除此图片？");
			         			if(result){
			         				var imgId = $(this).siblings("img").attr("imgId");
			         				deleteById(imgId);
				         			for(var i=0;i<imagesList.length;i++){
				         				if(imgId==imagesList[i]){
				         					imagesList = imagesList.splice(i,1);
				         				}
				         			}
				         			$(this).parents(".imgCop").remove();
			         			}
			         		})
						}, function(e) {
					console.log("读取拍照文件错误：" + e.message);
				});
			}, function(a) {}, {
				filter: "image"
			})
		};



 function uploadHead(imgPath,img) {
					var task = plus.uploader.createUpload(ajaxDomain + "/resource/upload.do", {
							method: "POST"
						},
						function(t, status) { //上传完成
							if(status == 200) {
								console.log(t.responseText);
								var fileBean = $.parseJSON(t.responseText);
								if(fileBean.result == "success") {
									mui.toast("上传成功！");
									isTjiao = true;
									var data = fileBean.data;
									img.attr("imgId",data.id);
									imagesList.push(data.id);
									img.siblings(".bgDiv").remove();
								} else {
									mui.toast(fileBean.resume);
								}
							} else {
								mui.toast("上传失败！");
							}
						}
					);
					compressImage(imgPath, new Date().getTime() + ".jpg", "img", function(namePath) {
						task.addFile(namePath, {
							key: "file"
						});
						task.start();
					});
				}	 
		
 
 	//压缩图片
function compressImage(url, filename, divid, backFn) {
					var name = "_doc/" + divid + "-" + filename; //_doc/upload/F_ZDDZZ-1467602809090.jpg
					plus.zip.compressImage({
							src: url, //src: (String 类型 )压缩转换原始图片的路径
							dst: name, //压缩转换目标图片的路径
							quality: 90, //quality: (Number 类型 )压缩图片的质量.取值范围为1-100
							overwrite: true //overwrite: (Boolean 类型 )覆盖生成新文件
						},
						function(event) {
							//uploadf(event.target,divid);
							var path = name; //压缩转换目标图片的路径
							//event.target获取压缩转换后的图片url路
							//filename图片名称
							backFn(path);
						},
						function(error) {
							plus.nativeUI.toast("压缩图片失败，请稍候再试");
						});
				}

var count = 0;
function submit(btnCode,MenuType){
	$(".qxbtn").on('tap',function(){
		if(count == 0){
			count++;
			if(isTjiao == false){
						mui.toast('图片正在上传', {
							duration: 'long', 
							type: 'div'
						});
						count = 0
						return false;
			}
			var datalist = JSON.parse(localStorage.getItem("dataList"));
			var userType = datalist.data.user;
			var describe = $(".tcp_content").val();
			var address = $("#address").val();
			var type = $(".typeQT").text();
			var typeId = $(".typeQT").attr('typeId');
			if(type == '请选择'){
				mui.toast('请选择报修类型');
				count = 0;
				return false;
			}
			if(describe ==""){
				mui.toast('描述不能为空');
				count = 0;
				return false;
			}
			if(address == ''){
				mui.toast('地址不能为空');
				count = 0;
				return false;
			}
			mui.ajax(ajaxDomain + "/repair_new/clickButton.do",{
						data:{
							 userType  : userType,
							 menuType : MenuType,
							 btnType :btnCode,
							 parms:JSON.stringify({
					           userId: userId,
				 	           baseId:baseId,
				 	           userName: userName,
				               describe: describe,
					           address: address,
					           type: type,
					           images:imagesList,
					           typeId:typeId
		   					})
						},
	   					dataType:'json',
						type:'post',//HTTP请求类型
						success:function(data){ 
							count = 0
							if(data.result == "success"){
								 mui.toast('添加成功');
								 setTimeout(function(){ 
										mui.back();
								 },400)
							}
						}
				})
			
			}
	})
	
}



//删除上传图片
function deleteById(id){
	mui.ajax(ajaxDomain + "/resource/deleteById.do",{
								data:{
									id : id
								},
			 					dataType:'json',
								type:'post',//HTTP请求类型
								success:function(data){ 
								
								},
								error:function(xhr,type,errorThrown){
									//异常处理；
								}
							});
}


