mui.plusReady(function() {
	window.addEventListener('refresh1', function(e) { //执行刷新
		//location.reload();
		var val = e.detail.val;
		if(val == 0) {
			pageStart = 1;
			getRepairList1(0, baseId);
			$(".mui-pull-bottom-tips").show();
		} else if(val == 1) {
			pageStart = 1;
			getRepairList1(1, baseId);
			$(".mui-pull-bottom-tips").show();
		}
	});
	//判断用户角色
	//var userRol = "commonUser";
	//var userRol = "leaderUser";
	//var userRol = "baseManager";
	var role = localStorage.getItem("role");
	//url = "wybx.html";
	if(role.indexOf("基地领导") != -1 || role.indexOf("物业领导") != -1) {
		var userRol = "leaderUser";
	} else if(role.indexOf("基地经理") != -1) {
		var userRol = "baseManager"
	} else {
		var userRol = "commonUser";
	}
	//获取数据 
	var count; //总数 
	var pageStart = 1;
	var firstLoad = true;
	var self;

	function getRepairList(status, baseId) {
		var djson = {
			userId: userId,
			status: status,
			page: pageStart,
			baseId: baseId,
			limit: 5
		}
		mui.ajax(ajaxDomain + "/repair/getRepairList.do", {
			data: {
				data: JSON.stringify(djson)
			},
			dataType: 'json',
			type: 'post', //HTTP请求类型
			success: function(data) {
				var needData = data.data;
				count = data.count;
				//count = data.count;
				//判断添加元素
				var nodes = $(".mainBodyUl");
				if(status == 0) {
					var isUrge = true;
					$(".moreBox").show();
				} else if(status == 1) {
					var isUrge = true;
					$(".moreBox").show();
				} else if(status == 2) {
					var isUrge = false;
					$(".moreBox").hide();
				} else if(status == 3) {
					var isUrge = false;
					$(".moreBox").hide();
				}
				//创建dom
				if(needData.length > 0) {

					var node = [];
					for(var j = 0, len = needData.length; j < len; j++) {
						var dom = [
							'<li class="mainBodyLi">',
							'<div class="conBox" bxId="', needData[j].id, '">',
							'<div class="desText">',
							'<span id="desTextSpan">', needData[j].content, '</span>',
							(function(j) {
								if(needData[j].status == 0 || needData[j].status == 1 || needData[j].status == 2) {
									if(needData[j].urge == "1") {
										return '<div class="desStatus" >催办</div>';

									}
								}

							})(j),
							'</div>',
							'<div class="desTime">', needData[j].createtime, '<span style="margin-left:10px">(', needData[j].datedetail, ')</span>', '</div>',
							'</div>',
							'<div class="btnBox">',
							(function(j) {
								if(isUrge) {
									var cop = [];
									if(userRol != "commonUser") {
										return;
									}
									if(needData[j].urge == 0) {
										//
										var createtime = needData[j].createtime.replace(/-/g, "/");
										var adate1 = new Date(createtime);
										var adate2 = new Date();
										var s1 = adate1.getTime();
										var s2 = adate2.getTime();
										var total = (parseInt(s2) - parseInt(s1)) / 60000;
										if(total < 2160) {
											var btnCls1 = '<span class="btnCls btnCB" zid="' + needData[j].id + '">催办</span>';
											cop.push(btnCls1);
											var btnCls2 = '<span class="btnCls btnQX" zid="' + needData[j].id + '">取消</span>';
											cop.push(btnCls2);

										} else {
											getRepairUrge(needData[j].id);
										}

									}

									return cop.join('');
								}
							})(j),
							'</div>',
							'</li>',
						].join('');
						node.push(dom);
					}
					nodes.append(node.join(''));
					if(firstLoad) {
						$(".mui-pull-bottom-tips").hide();
						firstLoad = false;
					}
					pageStart++;
				} else {
					//$(".mui-pull-bottom-tips").hide();
					/*nodes.empty();
					//加入暂无数据
					var noData = $("<div/>").css({
						"text-align":"center",
						"width":"100%",
						"height":"10rem"
					}).addClass("noData");
					nodes.append(noData);*/
				}
			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
			}
		});
	}

	function getRepairListDefault(status, baseId) {
		var djson = {
			userId: userId,
			status: status,
			page: pageStart,
			baseId: baseId,
			limit: 5
		}
		mui.ajax(ajaxDomain + "/repair/getRepairList.do", {
			data: {
				data: JSON.stringify(djson)
			},
			dataType: 'json',
			type: 'post', //HTTP请求类型
			success: function(data) {
				var needData = data.data;
				count = data.count;
				//count = data.count;
				$(".mui-pull-bottom-tips").hide();
				//判断添加元素
				var nodes = $(".mainBodyUl");
				if(status == 0) {
					var isUrge = true;
					$(".moreBox").show();
				} else if(status == 1) {
					var isUrge = true;
					$(".moreBox").show();
				} else if(status == 2) {
					var isUrge = false;
					$(".moreBox").hide();
				} else if(status == 3) {
					var isUrge = false;
					$(".moreBox").hide();
				}
				//创建dom

				if(needData.length > 0) {
					nodes.empty();
					var node = [];
					for(var j = 0, len = needData.length; j < len; j++) {
						var dom = [
							'<li class="mainBodyLi">',
							'<div class="conBox" bxId="', needData[j].id, '">',
							'<div class="desText">',
							'<span id="desTextSpan">', needData[j].content, '</span>',
							(function(j) {
								if(needData[j].status == 0 || needData[j].status == 1 || needData[j].status == 2) {
									if(needData[j].urge == "1") {
										return '<div class="desStatus" >催办</div>';
									}
								}
							})(j),
							'</div>',
							'<div class="desTime">', needData[j].createtime, '<span style="margin-left:10px">(', needData[j].datedetail, ')</span>', '</div>',
							'</div>',
							'<div class="btnBox">',
							(function(j) {
								if(isUrge) {
									var cop = [];
									if(needData[j].urge == 0) {
										//
										if(userRol != "commonUser") {
											return;
										}
										var createtime = needData[j].createtime.replace(/-/g, "/");
										var adate1 = new Date(createtime);
										var adate2 = new Date();
										var s1 = adate1.getTime();
										var s2 = adate2.getTime();
										var total = (parseInt(s2) - parseInt(s1)) / 60000;
										if(total < 2160) {
											var btnCls1 = '<span class="btnCls btnCB" zid="' + needData[j].id + '">催办</span>';
											cop.push(btnCls1);
											var btnCls2 = '<span class="btnCls btnQX" zid="' + needData[j].id + '">取消</span>';
											cop.push(btnCls2);

										} else {
											getRepairUrge(needData[j].id);
										}

									}

									return cop.join('');
								}
							})(j),
							'</div>',
							'</li>',
						].join('');
						node.push(dom);
					}
					nodes.append(node.join(''));
					if(firstLoad) {
						$(".mui-pull-bottom-tips").hide();
						firstLoad = false;
					}
					pageStart++;
				} else {
					$(".mui-pull-bottom-tips").hide();
					nodes.empty();
					//加入暂无数据
					var noData = $("<div/>").css({
						"text-align": "center",
						"width": "100%",
						"height": "10rem"
					}).addClass("noData");
					nodes.append(noData);
				}
			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
			}
		});
	}
	//催办接口
	function getRepairUrge(id) {
		mui.ajax(ajaxDomain + "/repair/urge.do", {
			data: {
				id: id
			},
			dataType: 'json',
			type: 'post', //HTTP请求类型
			success: function(data) {

			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
			}
		});
	}
	//取消接口
	function getRepairCancel(id) {
		mui.ajax(ajaxDomain + "/repair/cancel.do", {
			data: {
				id: id
			},
			dataType: 'json',
			type: 'post', //HTTP请求类型
			success: function(data) {

			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
			}
		});
	}
	//上拉刷新
	function getRepairList1(status, baseId) {
		var djson = {
			userId: userId,
			status: status,
			baseId: baseId,
			page: 1,
			limit: 5
		}
		mui.ajax(ajaxDomain + "/repair/getRepairList.do", {
			data: {
				data: JSON.stringify(djson)
			},
			dataType: 'json',
			type: 'post', //HTTP请求类型
			success: function(data) {
				var needData = data.data;
				count = data.count;
				//count = data.count;
				//创建dom
				$(".mainBodyUl").empty();
				var nodes = $(".mainBodyUl");
				if(status == 0) {
					var isUrge = true;
					$(".moreBox").show();
				} else if(status == 1) {
					var isUrge = true;
					$(".moreBox").show();
				} else if(status == 2) {
					var isUrge = false;
					$(".moreBox").hide();
				} else if(status == 3) {
					var isUrge = false;
					$(".moreBox").hide();
				}
				if(needData.length > 0) {
					var node = [];
					//判断添加元素

					for(var j = 0, len = needData.length; j < len; j++) {
						var dom = [
							'<li class="mainBodyLi">',
							'<div class="conBox" bxId="', needData[j].id, '">',
							'<div class="desText">',
							'<span id="desTextSpan">', needData[j].content, '</span>',
							(function(j) {
								if(needData[j].status == 0 || needData[j].status == 1 || needData[j].status == 2) {
									if(needData[j].urge == "1") {
										return '<div class="desStatus" >催办</div>';
									}
								}
							})(j),
							'</div>',
							'<div class="desTime">', needData[j].createtime, '<span style="margin-left:10px">(', needData[j].datedetail, ')</span>', '</div>',
							'</div>',
							'<div class="btnBox">',
							(function(j) {
								if(isUrge) {
									var cop = [];
									if(needData[j].urge == 0) {
										//
										if(userRol != "commonUser") {
											return;
										}
										var createtime = needData[j].createtime.replace(/-/g, "/");
										var adate1 = new Date(createtime);
										var adate2 = new Date();
										var s1 = adate1.getTime();
										var s2 = adate2.getTime();
										var total = (parseInt(s2) - parseInt(s1)) / 60000;
										if(total < 2160) {
											var btnCls1 = '<span class="btnCls btnCB" zid="' + needData[j].id + '">催办</span>';
											cop.push(btnCls1);
											var btnCls2 = '<span class="btnCls btnQX" zid="' + needData[j].id + '">取消</span>';
											cop.push(btnCls2);

										} else {
											getRepairUrge(needData[j].id);
										}

									}

									return cop.join('');
								}
							})(j),
							'</div>',
							'</li>',
						].join('');
						node.push(dom);
					}
					nodes.append(node.join(''));
					pageStart = 2;
				} else {
					$(".mui-pull-bottom-tips").show();
					/*nodes.empty();
					//加入暂无数据
					var noData = $("<div/>").css({
						"text-align":"center",
						"width":"100%",
						"height":"10rem",
					}).addClass("noData");
					nodes.append(noData);*/
				}

			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
			}
		});
	}
	//获取基地经理数据

	if(userRol == "commonUser" || userRol == "leaderUser" || userRol == "baseManager") { //用户和用户领导
		if(userRol == "leaderUser" || userRol == "baseManager") {
			$(".seeBX").show();

			$(".seeBX").on('tap', function() {
				var id = $(this).attr("class");
				mui.openWindow({
					url: "wybxMange.html",
					id: id,
					styles: {
						popGesture: "close"
					},
					show: {
						aniShow: "pop-in"
					},
					extras: {
						"tag": "wyyb"
					},
					waiting: {
						autoShow: false
					}
				});
			});
			//$(".moreBox").remove(); 
		}
		//创建头部内容
		var domLi = '<li class= "topCommonLi active" code="0">全部</li>' +
			'<li class="topCommonLi" code="1">待处理</li>' +
			'<li class="topCommonLi" code="2">已处理</li>' +
			'<li class="topCommonLi" code="3">已完成</li>';
		$(".topUl").append(domLi);
		//页面初始化
		getRepairListDefault(0, baseId);
		var tap_first = false;
		$(document).on('tap', ".topCommonLi", function() {
			if(!tap_first) {
				tap_first = true;
				var index = $(this).index();
				if(!$(this).hasClass("active")) {
					$(this).addClass('active');
					$(this).siblings().removeClass('active');
					//if($(".mainBodyUl").eq(index).html()==""){
					pageStart = 1;
					getRepairListDefault(index, baseId);
					//$(".mui-pull-bottom-tips").hide();
					//}
					//$(".contentDiv .cntBigCop").eq(index).show();
					//$(".contentDiv .cntBigCop").eq(index).siblings().hide();
				}
				setTimeout(function() {
					tap_first = false;
				}, 1000);
			}
		});
		//点击催办，取消
		$(document).on('tap', ".btnCls", function() {
			var id = $(this).attr("zid");
			if($(this).hasClass("btnCB")) { //催办
				$(this).parents(".mainBodyLi").find(".desText").append('<div class="desStatus" >催办</div>')
				$(this).parent().remove();
				getRepairUrge(id);

			} else { //取消
				var result = confirm("取消后将删除此条报修记录，是否继续？");
				if(result) {
					getRepairCancel(id);
					$(this).parents(".mainBodyLi").remove();
				}

			}

		});
		//点击条目进入详情页面
		$(document).on('tap', '.conBox', function() {
			var bxId = $(this).attr("bxId");
			mui.openWindow({
				url: "wyxq.html",
				id: "wyxq",
				show: {
					aniShow: "pop-in"
				},
				extras: {
					"tag": "wyxq",
					"bxId": bxId
				},
				waiting: {
					autoShow: false
				}
			});
		});
		(function($, jq) {
			//阻尼系数
			var deceleration = mui.os.ios ? 0.003 : 0.0009;
			$('.mui-scroll-wrapper').scroll({
				bounce: false,
				indicators: true, //是否显示滚动条
				deceleration: deceleration
			});
			$.ready(function() {
				$('#test').pullToRefresh({
					down: {
						callback: function() {
							var self = this;
							var statusActive = jq(".topUl li.active").attr("code");
							//									/jq(".mui-pull-bottom-tips").hide();
							self.endPullUpToRefresh(false);
							setTimeout(function() {
								getRepairList1(statusActive, baseId);
								self.endPullDownToRefresh(true);
								self.refresh(true);

							}, 100);
						}
					},
					up: {
						callback: function() {
							var self = this;
							var list = $(".mainBodyLi").length;
							var statusActive = jq(".topUl li.active").attr("code");
							jq(".mui-pull-bottom-tips").show();
							setTimeout(function() {
								getRepairList(statusActive, baseId);
								if(count <= list) {
									self.endPullUpToRefresh(true)
								} else {
									self.endPullUpToRefresh(false)
								}
							}, 100);
						}
					}
				});
			});
		})(mui, jQuery);

	} else if(userRol == "baseManager") {
		//查看报修
		$(".seePic").show();
		$(".seePic").on('tap', function() {
			var id = $(this).attr("class");
			mui.openWindow({
				url: "wyyb.html",
				id: id,
				styles: {
					popGesture: "close"
				},
				show: {
					aniShow: "pop-in"
				},
				extras: {
					"tag": "wyyb"
				},
				waiting: {
					autoShow: false
				}
			});
		});
		$(".moreBox").hide();
		//基地经理所在基地
		getBaseListByUserId();
		//获取数据
		getRepairListDefault(0, baseId);

	}
	//点击新增
	$(".morePic").on('tap', function() {
		var index = $(".topCommonLi.active").index();
		mui.openWindow({
			url: "wyxjbx.html",
			id: baseId,
			styles: {
				popGesture: "close"
			},
			show: {
				aniShow: "pop-in"
			},
			extras: {
				"tag": "wyxjbx",
				"type": index
			},
			waiting: {
				autoShow: false
			}
		});
	})
	//返回按钮
	mui('.smailNav').on('tap', '.back', function(e) {
		mui.back();
	});
	mui.init({
		swipeBack: true //启用右滑关闭功能
	});
})