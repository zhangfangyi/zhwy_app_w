var Dropdwon = (function() {

	function $getClass(dom) {
		return $('.' + dom)
	}

	function $appendHtml(config) {
		var arr = config.dataArr,
			type = config.type,
			title = config.title,
			bodyDom = $('body'),
			arrLen = arr.length;
		var html = '';
		html += "<div class='dropdwon display_none' style='width:100%;'>";
		html += "<div class='down_cont' style='width:100%;'>";
		html += "  <div class='title'>";
		html += "    <span class='cacel' id='cacel'>取消</span>";
		html += "    <span class='name' id='titleName'>" + title + "</span>";
		html += "    <span class='finsih' id='finsih' data-type='" + type + "'>完成</span>";
		html += "  </div>";
		html += "  <div class='content' style='height: 260px;overflow-y:scroll;'>";

		if(arr) {
			for(var i = 0; i < arrLen; i++) {
				html += "<div class='mui-input-row mui-checkbox mui-left " + type + "' style='border-bottom: 1px solid #ededed;width:100%;'>";
				html += "<label class='tag'>" + arr[i].text + "</label>";
				html += "<input name='checkbox1' value='" + arr[i].text + "' class='inchecks' type='checkbox' id='" + arr[i].baseId + "'>";
				html += "</div>";
			}
		} else {
			html += "请传入数据！！"
		}
		html += "  </div>";
		html += "</div>";
//				html += "<div class='other_box'>";
		html += "</div>";
		html += "</div>";
		bodyDom.append(html);
		//		$initActiveClass()
	}

	function $dropdwonShow(bool) {
		let drop = $getClass('dropdwon');
		return bool ? drop.show(0) : drop.hide(0)
	}

	$('body').on('click', '#cacel', function() {
		$dropdwonShow(false)
	});
	$('body').on('click', '.other_box', function() {
		$dropdwonShow(false)
	});
	function domClick(config) {
		this.succe = config.succe;
		this.type = config.type;
		$appendHtml(config);
		$dropdwonShow(true);
		var $this = this;
		$('body').on('click', '#finsih', function(config) {
			var ress = getcheckBoxRess('inchecks');
			if(ress.length > 0) {
				$dropdwonShow(false);
			}else{
				$dropdwonShow(false);
			}
			$this.succe(ress);
			return ress
		})
		
	}

	function getcheckBoxRess(classNamee) {
		var rdsobjh = document.getElementsByClassName(classNamee);
		var checkVala;
		var checkida;
		var vaidlist = [];
		var vaidlistval =[];
		for(i = 0; i < rdsobjh.length; i++) {
			if(rdsobjh[i].checked) {
				checkVala = rdsobjh[i].value;
				checkida = rdsobjh[i].id;
				vaidlist.push(checkida)
				vaidlistval.push(checkVala)
			}
		}
		return vaidlist;
		

	}
	return domClick
})()