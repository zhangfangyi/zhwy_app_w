 mui.plusReady(function() {
	(function($,jq) {
				//阻尼系数
				var deceleration = mui.os.ios?0.003:0.0009;
				$('.mui-scroll-wrapper').scroll({
					bounce: false,
					indicators: true, //是否显示滚动条
					deceleration:deceleration
				});
				$.ready(function() {
					$('#test').pullToRefresh({
							down: {
								callback: function() {
									var self = this;
									jq(".mui-pull-bottom-tips").hide();
									self.endPullUpToRefresh(false);
									setTimeout(function() {
										getGgListData();
										self.endPullDownToRefresh(true);
										self.refresh(true);
										
									}, 100);
								}
							},
							up: {
								callback: function() {
									var self = this;
									
									jq(".mui-pull-bottom-tips").show();
									setTimeout(function() {
										pulldownRefresh();  
										var list = $(".tzgg_item").length;
										if(count<=list){  
											 self.endPullUpToRefresh(true)
										}else{
											self.endPullUpToRefresh(false)
										}
									}, 100);
								}
							}
						});
				});
	})(mui,jQuery);
var count; //总数
var pageStart = 1;
var firstLoad = true;
var baseId = localStorage.getItem("baseId");
var deceleration = mui.os.ios ? 0.003 : 0.0009;
//返回按钮
mui('.smailNav').on('tap', '.back', function(e) {
	mui.back();
});

	//参加
mui('.cntSmailCop').on('tap', '.tzgg_item', function(e) {
	var listId = $(this).attr("id");
	$(this).find("h1").css("color","#767676");
	mui.openWindow({
		url: "tzggxq.html",
		id: "tzggxq",
		show: {
			aniShow: "pop-in"
		},
		extras: {
			"listId": listId
		},
		waiting: {
			autoShow: false
		}
	});
});
		
function pulldownRefresh(){
	  setTimeout(function(){ 
		  mui.ajax(ajaxDomain + "/noticeInfo/getNotices.do",{
						data:{
							baseId:baseId,
							page:pageStart,
							limit:6
						},
	 					dataType:'json',
						type:'post',//HTTP请求类型
						async:false,
						success:function(data){  
							count = data.count;
							var scroll = $("#abcd");
							var data = data.data;
							for(var i =0,len=data.length;i<len;i++){
								var a=data[i].title.length>=20?data[i].title.substr(0,20):data[i].title;
								var str = $('<div class="tzgg_item">'+
										'<p>'+(function(){
											return data[i].title.length>30?data[i].title.substr(0,30)+"...":data[i].title
										})()+'</p>'+
										'<span class="clock">'+ data[i].createtime +'</span>'+
									'</div>').attr("id",data[i].id);
								scroll.append(str);
							}
							if(firstLoad){
								if(data.length==0){
									scroll.empty();
								 		//加入暂无数据
									 		var noData = $("<div/>").css({
									 			"text-align":"center",
									 			"width":"100%",
									 			"height":"10rem"
									 		}).addClass("noData");
									 		scroll.append(noData);
								}
								$(".mui-pull-bottom-tips").hide();
								firstLoad = false;
							}
							pageStart++;
						},
						error:function(xhr,type,errorThrown){
							//异常处理；
						}
					});
					
	  },100) 
	
}
pulldownRefresh(); 	

function getGgListData(){
	mui.ajax(ajaxDomain + "/noticeInfo/getNotices.do",{
					data:{
						baseId:baseId,
						page:1,
						limit:6
					},
 					dataType:'json',
					type:'post',//HTTP请求类型
					async:false,
					success:function(data){ 
						var scroll = $("#abcd");
						scroll.empty();
						var data = data.data;
						for(var i =0,len=data.length;i<len;i++){
							var str = $('<div class="tzgg_item">'+
									'<p>'+ data[i].title +'</p>'+
									'<span class="clock">'+ data[i].createtime +'</span>'+
								'</div>').attr("id",data[i].id);
							scroll.append(str);
						}
						pageStart=2;
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
					}
				});
				
}

})
