 mui.plusReady(function() {
 	
// 	wxjdconH();
 	
	var id = plus.webview.currentWebview().xqId; 
	var menuType = plus.webview.currentWebview().xqMenuType;
 	repairDetail(id,menuType);
 	btnsClick(id,menuType);//按钮
 	
 	
 	 mui.init({
			swipeBack: true ,//启用右滑关闭功能
			beforeback: function() {
		        var list = plus.webview.currentWebview().opener();
		        mui.fire(list, 'wybxrefresh',{
		        	type:menuType,
		        }); 
		        return true;
			   }
		});
			var deceleration = mui.os.ios ? 0.003 : 0.0009;
			mui('.mui-scroll-wrapper').scroll({
				deceleration: deceleration,
				indicators: false
			});	
			//返回按钮
			mui('.smailNav').on('tap', '.back', function(e) {
				mui.back();
			});
 	
 	
 	mui.previewImage();
 	
 
 }); 
 
 //设置高度
 function wxjdconH(btn){
 	$('body').css('overflow','hidden');
	$('body').css('height','100%');
 	var windowH = $(window).height();
	var sqxqH = $(".sqxq").height();
	var bxxqTopH = $(".bxxqTop").height();
	var wybxBotbtnH = $(".wybxBotbtn").height();
	var wxjdconH = windowH - sqxqH - bxxqTopH -wybxBotbtnH  ;
	if(btn){
		$(".wxjdcon").height(windowH - sqxqH - bxxqTopH);
	}else{
		$(".wxjdcon").height(wxjdconH -10);
	}
 }
 
//详情
function repairDetail(id,menuType){
		var datalist = JSON.parse(localStorage.getItem("dataList"));
		var userType = datalist.data.user;
		mui.ajax(ajaxDomain + "/repair_new/getRepairDetail.do",{
					data:{
						 id:id,
						 userType:userType,
						 menuType:menuType
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					
					success:function(data){ 
//						console.log(JSON.stringify(data))
						var proData = data.data.progress;//进度
						var btns = data.data.btns;//按钮
						var arr = Object.keys(btns);
						var len = arr.length;
						var data = data.data.detail;
						$(".xqtit").text(data.describe);
						$(".bxrName").text(data.username);
						$('.bxrAdress').text(data.basename);
						$('.bxrTime').text(data.createtime);
						$('.bxlx').text(data.type);
						$('.bxdh').text(data.ordernumber);
						$('.bxms').text(data.describe);
						$('.jtdz').text(data.address);
						$('.bxrPhone a').html(data.phonenumber);
						$('.bxrPhone a').attr("href","tel:"+data.phonenumber);
						$('.dpd span').text(datalist.data.repairType[data.status]);
						
						var typeId = data.typeid;
						localStorage.setItem('urge',data.urge);//urge 存储
						
						var images = data.images;
						for(var i=0;i<images.length;i++){
							var img = $('<img src="'+ajaxDomain+images[i].PHOTONAME+'"  data-preview-src="" data-preview-group="1" />');
							$('.ckdt').prepend(img);
						}
						for(var i = 0;i<proData.length;i++){
							var str = $('<div class="wxjddetails">'+
										   	'<div class="wxjddtltic">'+
										   		'<div class="delTime">'+
										   			'<span>'+ proData[i].createtime.substring(11,16) +'</span><br>'+
										   			'<span>'+ proData[i].createtime.substring(5,7)+'/'+ proData[i].createtime.substring(8,10) +'</span>'+
										   		'</div>	'+
										   		'<img class="delImg" src=" '+ ajaxDomain + proData[i].photoname+' " alt="" />'+
										   		'<div class="delName">'+
										   			'<span>'+ proData[i].username +'</span>'+
										   			'<span style="'+xqSetColor(proData[i].accepttype)+'" >'+ datalist.data.userType[proData[i].accepttype]  +'</span>'+
										   		'</div>'+
										   	'</div>'+
									   '</div>');
							var wxjddtl = $('<div class="wxjddtl">'+
										    	 '<p>'+ proData[i].content +'<p>' +
										    '</div>');
							var proImg = proData[i].images;
							if(proImg){
								for(var j=0;j<proImg.length;j++){
									var img = '<img src=" '+ ajaxDomain + proImg[j].PHOTONAME+' " data-preview-src="" data-preview-group="2" />';
									wxjddtl.append(img)
								}
							}
							str.append(wxjddtl);	   
							$('.wxjdcon').append(str);
						}
						$.each(btns, function(k,val) {
							console.log(k)
							if(len == 1){
								var btn = $('<div class="btnClick qxbtn">'+val+'</div>').attr("type",k);
								$('.wybxBotbtn').append(btn);
							}else{
								if(k == "9"){//修改
									$('.bxlximg').show().attr({'typeId':typeId,'btntype':k});
								}else{
									var btn = $('<div class="btnClick towBtn">'+val+'</div>').attr("type",k);			
									$('.wybxBotbtn').append(btn);
								} 
							}
						});
						
						if(len == 0){
							wxjdconH('nobtn');
							$('.wybxBotbtn').hide();
							$('.wxjd').css("margin-bottom",'-1rem');
						}else{
							wxjdconH(); 
						}
						
					},
					error:function(xhr,type,errorThrown){
						mui.toast('网络无法连接', {
								duration: 'long', 
								type: 'div'
							});
					}
			})

}


function xqSetColor(accepttype){
	var datalist = JSON.parse(localStorage.getItem("dataList"));
	var color =  "color:"+datalist.data.userTypeColor[accepttype]
		+";border:1px solid"+datalist.data.userTypeColor[accepttype];
	return color;
	
	
}

//按钮事件
function btnsClick(id,menuType){
	//修改小类
	mui(".sqlist").on("tap", ".bxlximg", function(e) {
			var typeId = $(this).attr('typeId');
			var btntype = $(this).attr('btntype');
			mui.ajax(ajaxDomain + "/wordbook/getWordBookItemListByWbId.do",{
					data:{ 
						 wbId:typeId
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						var setData= [];
						for(var i=0;i<data.data.length;i++){
							var obj = {};
							obj["text"] = data.data[i].name;
							obj["typeId"] = data.data[i].id;
							obj["value"] = data.data[i].name;
							var cop = $.extend(true,{},data.data[i],obj);
							setData.push(cop);
						}
						var picker = new mui.PopPicker();
            					picker.setData(setData);
            					picker.show(function (selectItems) {
            					var itemName = selectItems[0].value;
            					var datalist = JSON.parse(localStorage.getItem("dataList"));
								var userType = datalist.data.user;
            					 	mui.ajax(ajaxDomain + "/repair_new/clickButton.do",{
										data:{
											 userType  : userType,
											 menuType : menuType,
											 btnType :btntype,
											 parms:JSON.stringify({
									           userId: userId,
								 	           baseId:baseId,
								 	           userName: userName,
								 	           repairId :id,
								 	           type: itemName,
								 	           typeId:typeId
						   					})
										},
					   					dataType:'json',
										type:'post',//HTTP请求类型
										success:function(data){ 
											$('.bxlx').text(itemName);
										}
									})
            					
            					
            					
            					   
            			    })
					} 
			})
		
	})


	//底部按钮
	mui(".wybxBotbtn").on("tap", ".btnClick", function(e) {
		var btntype = $(this).attr("type");
		var datalist = JSON.parse(localStorage.getItem("dataList"));
		var userType = datalist.data.user;
		if(btntype == "3"){//退回
				mui.openWindow({
						url: "newTh.html",
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"thid":id,
							"thmenuType":menuType,
							"thbtntype":btntype
						},
						waiting: {
							autoShow: false
						}
				});
		}else if(btntype == "4"){//4 维修完毕
			mui.openWindow({
						url: "newWxwb.html",
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"wxid":id,
							"wxmenuType":menuType,
							"wxbtntype":btntype
						},
						waiting: {
							autoShow: false
						}
				});
		}else if(btntype == "6"){//评价
			mui.openWindow({
						url: "newTjpj.html",
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"pjid":id,
							"pjmenuType":menuType,
							"pjbtntype":btntype
						},
						waiting: {
							autoShow: false
						}
				});
		}else if(btntype == "5"){//结单
			mui.openWindow({
						url: "newJd.html",
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"jdid":id,
							"jdmenuType":menuType,
							"jdbtntype":btntype
						},
						waiting: {
							autoShow: false
						}
				});
		}else if(btntype == "2"){//派单
			if(userType == 2){ //给工程部、基地经理
				var dispatchType = datalist.data.dispatchType;
				var setData= [];
				$.each(dispatchType, function(k,val) {
									var obj = {};
									obj["urge"] = k;
									obj["text"] = val;
									var cop = $.extend(true,{},val,obj);
									setData.push(cop);
				});
				var picker = new mui.PopPicker();
            					picker.setData(setData);
            					picker.show(function (selectItems) {
            						var urge = selectItems[0].urge;
								   	mui.ajax(ajaxDomain + "/repair_new/clickButton.do",{
										data:{
											 userType  : userType,
											 menuType : menuType,
											 btnType :btntype,
											 parms:JSON.stringify({
									           userId: userId,
								 	           baseId:baseId,
								 	           userName: userName,
								 	           repairId :id,
								 	           urge:urge
						   					})
										},
					   					dataType:'json',
										type:'post',//HTTP请求类型
										success:function(data){ 
											if(data.result == "success"){ 
												 mui.toast("派单成功");
												 setTimeout(function(){ 
														mui.back();
												 },400)
											}
										}
									})
    							})
			}else{
				//派单给维修工
				var urge = localStorage.getItem('urge');
				mui.ajax(ajaxDomain + "/repair_new/getRepairUsers.do",{
					data:{ 
						baseId: baseId,
						urge: urge
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						var setData= [];
						for(var i=0;i<data.data.length;i++){
							var obj = {};
							obj["text"] = data.data[i].username;
							obj["userid"] = data.data[i].userid;
							var cop = $.extend(true,{},data.data[i],obj);
							setData.push(cop);
						}
						var picker = new mui.PopPicker();
            					picker.setData(setData);
            					picker.show(function (selectItems) {
            					var repairUserId = selectItems[0].userid;
            					var repairUserName = selectItems[0].text;
            					  	mui.ajax(ajaxDomain + "/repair_new/clickButton.do",{
										data:{
											 userType  : userType,
											 menuType : menuType,
											 btnType :btntype,
											 parms:JSON.stringify({
									           userId: userId,
								 	           baseId:baseId,
								 	           userName: userName,
								 	           repairId :id,
								 	           repairUserId: repairUserId,
	           								   repairUserName:repairUserName,
	           								   urge:urge
						   					})
										},
					   					dataType:'json',
										type:'post',//HTTP请求类型
										success:function(data){ 
											if(data.result == "success"){ 
												 mui.toast("派单成功");
												 setTimeout(function(){ 
														mui.back();
												 },400)
											}
										}
									})
            			})
					}
				})
			}
		}else if(btntype == "7"){//取消
			mui.ajax(ajaxDomain + "/repair_new/clickButton.do",{
					data:{
						 userType  : userType,
						 menuType : menuType,
						 btnType :btntype,
						 parms:JSON.stringify({
				           userId: userId,
			 	           baseId:baseId,
			 	           userName: userName,
			 	           repairId :id
	   					})
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						if(data.resume == "成功"){ 
							 mui.toast("取消成功");
							 setTimeout(function(){ 
									mui.back();
							 },400)
						}
					}
			})
		}
	})
}
 
 
