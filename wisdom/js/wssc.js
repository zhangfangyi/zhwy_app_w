mui.init({
	swipeBack: true, //启用右滑关闭功能
	keyEventBind: {
		backbutton: true  //关闭back按键监听
	}
});


//滚动初始化
var deceleration = mui.os.ios ? 0.000003 : 0.0009;
var menusApp = new Vue({
	el:'#articleTabs',
	data:function(){
		return {
			menus:[
				{name:'生鲜水产'},{name:'乳制品'},{name:'粮油干货'},{name:'零食糕点'},
				{name:'生鲜水产'},{name:'乳制品'},{name:'粮油干货'},{name:'零食糕点'},
				{name:'生鲜水产'},{name:'乳制品'},{name:'粮油干货'},{name:'零食糕点'},
				{name:'生鲜水产'},{name:'乳制品'},{name:'粮油干货'},{name:'零食糕点'},
				{name:'生鲜水产'},{name:'乳制品'},{name:'粮油干货'},{name:'零食糕点'}
			],
			activeIndex:0
		}
	},
	methods:{
		menuHandle:function(menuIndex){
			this.activeIndex = menuIndex;
			//测试代码
			for(var i=0; i<goodsApp.items.length; i++){
				goodsApp.items[i].name = this.menus[this.activeIndex].name;
			}
		}
	},
	mounted:function(){
		var _this = this;
		
		/*var index = localStorage.getItem('shtTabIndex');
		_this.activeIndex = index;*/
		mui('.scroll-articleLeft').scroll({
			deceleration: deceleration,
			indicators: false,
			bounce: true
		});
	}
})

var goodsApp = new Vue({
	el:'#articleGoods',
	data:function(){
		return {
			items:[
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'},
				{name:'水果',img:'css/images/example_shtItem1.png',weight:'500g',price:'22.5',unit:'份'}
			],
		}
	},
	methods:{
		addToGWCH:function(index){
			headerApp.gwchVal += 1;
		}
	},
	mounted:function(){
		var _this = this;
		mui('.scroll-articleRight').scroll({
			deceleration: deceleration,
			indicators: false
		});
		
		setTimeout(function(){
			for(var i=0; i<_this.items.length; i++){
				_this.items[i].name = menusApp.menus[menusApp.activeIndex].name;
			}
		},300)//测试代码
		
	}
})

var headerApp = new Vue({
	el:'#header-sht',
	data:{
		val:'',
		gwchVal:0
	},
	methods:{
		searchHandle:function(){
			if(this.val === ''){
				if(menusApp.menus[0].name === '搜索结果'){
					menusApp.menus.splice(0,1);
				};
			}else{
				if(menusApp.menus[0].name === '搜索结果'){
					return;
				};
				menusApp.menus.unshift({
					name:'搜索结果'
				});
				menusApp.activeIndex = 0;
			}
		}
	}
})


