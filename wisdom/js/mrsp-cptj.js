//获取登录用户相关信息
var baseId = localStorage.getItem('baseId');
//获取页面参数
var params = null;
var opener = null;
var isTjiao = true;
var curImgPath = '';
mui.plusReady(function () {
   //获取页面参数
	params = plus.webview.currentWebview();
	opener = params.opener();
	var userId = localStorage.getItem('userId');
	if(params.type!=undefined&&params.type=="edit"){
		$(".submitCop1").show();
	}
	//全局参数
	var baseSelector = null;
	// 设置滚动系数
	var deceleration = mui.os.ios ? 0.000003 : 0.0009;
	//框架实例化
	mui.init({
		swipeBack: true, //启用右滑关闭功能
		keyEventBind: {
			backbutton: true  //关闭back按键监听
		}
	});
	//刷新页面
	window.addEventListener('refreshCPBJ', function(e){//执行刷新
		location.reload();
	});
	
	var u = navigator.userAgent;
	var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
	 if (isIOS) {
	   var originalHeight=document.documentElement.clientHeight || document.body.clientHeight;
	   window.onresize=function(){
	        var  resizeHeight=document.documentElement.clientHeight || document.body.clientHeight;
	        //软键盘弹起与隐藏  都会引起窗口的高度发生变化
	        if(resizeHeight*1<originalHeight*1){ //resizeHeight<originalHeight证明窗口被挤压了
	        	mui.scrollTo("10%",0)
		         　　  		plus.webview.currentWebview().setStyle({
		              　　		height:originalHeight
		           　	});
	        }
		}

	 	plus.webview.currentWebview().setStyle({
	 　　		softinputMode: "adjustResize"  // 弹出软键盘时自动改变webview的高度
	 	});
  	}
	
	//页面组件初始化
	var pageApp = new Vue({
		el:'#pageContent',
		data:{
				name:'',//菜品名称
				createdate:params.addDate,
				type:params.addType,
				baseId:'',
				cookerData:{
					baseId:'',
					name:'',
					id:''
				},
				baseName:''
		},
		created:function(){
			cookerData = JSON.parse(localStorage.getItem('cookerData'));
			this.cookerData = cookerData;
			this.name = localStorage.getItem('foodAdd_foodName');
		},
		mounted:function(){
			document.getElementById('pageContent').style.opacity = '1';
			var self = this;
			//初始化基地选择
			mui.ajax(ajaxDomain + '/baseInfo/getBaseListByUserId',{
				data:{
					userId:userId
				},
				type:'post',//HTTP请求类型
				success:function(data){
					var _baseId = JSON.parse(localStorage.getItem('memoryDateData')).activeBaseId;
					if(!_baseId){
						_baseId = localStorage.getItem('baseId');
					}
					if(data.result === 'success'){
						var selectorVal = data.data;
						for(var i=0; i<selectorVal.length; i++){
							if( _baseId=== selectorVal[i].id){
								self.baseId = selectorVal[i].id;
								self.baseName = selectorVal[i].name;
							};
						};
						if(!self.baseName){
							self.baseId = selectorVal[0].id;
							self.baseName = selectorVal[0].name;
						}
					};
				},
				error:function(xhr,type,errorThrown){
					//异常处理；
					mui.toast('无法连接网络');
				}
			});
		},
		methods:{
			//打开厨师选择页面
			openCHSHWindow:function(){
				var self = this;
				if(!this.baseId){
					mui.toast('基地数据有误，无法查询厨师信息');
					return;
				};
				//记录当前信息
				localStorage.setItem('foodAdd_foodName',this.name);
				localStorage.setItem('foodAdd_baseId',this.baseId);
				//记录当前图片情况
				var imgContainer = $('#loadImgBox');
				var cptjImgData = {
					src:'',
					id:''
				};
				if(imgContainer.size() == 1){
					cptjImgData.src = imgContainer.attr('src');
					cptjImgData.id = imgContainer.attr('imgId');
				};
				var cptjImgDataStr = JSON.stringify(cptjImgData);
				localStorage.setItem('cptjImgData',cptjImgDataStr);
				//return
				mui.openWindow({
					url: "mrspxzcs.html",
					id: "foodMaster",
					styles: {
						popGesture: "close"
					},
					show: {
						aniShow: "pop-in"
					},
					extras: {
						"tag": "mrspxzcs",
						"baseId":self.baseId
					},
					waiting: {
						autoShow: false
					}
				});
			},
			//开启基地列表
			showSelector:function(){
				var self = this;
				/*if(baseSelector !== null){
					baseSelector.show(function(selectItems){
						self.baseId = selectItems[0].id;
						self.baseName = selectItems[0].name;
						//如果厨师此时已选择过而基地变化了则提示重新选择厨师
						if(self.cookerData.name){
							if(self.cookerData.baseId !== self.baseId){
								//重置缓存值
								var cookerData = {
									id:'',
									baseId:'',
									name:''
								};
								localStorage.setItem('cookerData',JSON.stringify(cookerData));
								self.cookerData = cookerData;
								mui.toast('您已重新选择了基地，请重新选择厨师');
							};
						};
					});
				}*/
			},
			//保存
			addHandle:function(){
				if(isTjiao == false){
					mui.toast('图片正在上传', {
						duration: 'long', 
						type: 'div'
					});
					return;
				}
				var submitParams = {
					name:this.name,
					createdate:this.createdate,
					type:this.type,
					baseid:this.baseId,
					masterid:this.cookerData.id,
					rfid:$('#loadImgBox').attr('imgId')
				};
				if(params.type == 'edit'){
					submitParams.id = params.caipinId;
				};
				if(submitParams.name === ''){
					mui.toast('菜品名称不能为空');
					return;
				};
				if(submitParams.masterid === ''){
					mui.toast('所属厨师不可为空');
					return;
				};
				if(submitParams.baseid === ''){
					mui.toast('所属基地不可为空');
					return;
				};
				
				//判断厨师与基地是否一致
				if(this.baseId !== this.cookerData.baseId){
					mui.toast('当前所选厨师与当前基地不匹配，请重新选择！');
					return;
				};
				
				//添加菜品
				//接口调用
				mui.ajax(ajaxDomain + '/cook/addCookFood.do',{
					data:submitParams,
					type:'post',//HTTP请求类型 
					success:function(data){
						if(data.result === 'success'){
							mui.toast('菜品添加成功！');
							//跳转回菜品页面并定位到之前选中的日期中
							mui.fire(opener, 'refreshMRSP',{thank:1});
							return true;
						};
						
						
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('无法连接网络');
					}
				});
			},
			//删除
			delHandle:function(){
				var submitParams = {
					name:this.name,
					createdate:this.createdate,
					type:this.type,
					baseid:this.baseId,
					masterid:this.cookerData.id,
					rfid:params.imgId
				};
				submitParams.id = params.caipinId;
				var result= confirm("是否确认删除？");
	     			if(result){
	     				//接口调用
						mui.ajax(ajaxDomain + '/cook/delCookFood.do',{
							data:submitParams,
							type:'post',//HTTP请求类型 
							success:function(data){
								if(data.result === 'success'){
									mui.toast('菜品删除成功！');
									//跳转回菜品页面并定位到之前选中的日期中
									mui.fire(opener, 'refreshMRSP',{thank:1});
									return true;
								};				
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								mui.toast('无法连接网络');
							}
						});
	     			}
				
				}
			}
		})
	
		//菜品编辑时带过来的图片数据
		if(params.imageUrl!=undefined){
			renderLoadImg(ajaxDomain+params.imageUrl,params.imgId);
		}
		//添加或修改时当前添加的图片数据存在时
		var _cptjImgData = JSON.parse(localStorage.getItem('cptjImgData'))
		if(_cptjImgData.id){
			renderLoadImg(_cptjImgData.src,_cptjImgData.id);
		}
	
		//设置菜品图片
		mui(document).on("tap", "#itemImg", function(e) {
			if(mui.os.plus){
				var a = [{
					title: "拍照"
				}, {
					title: "从手机相册选择"
				}];
				plus.nativeUI.actionSheet({
					title: "设置菜品图片",
					cancel: "取消",
					buttons: a
				}, function(b) {
					switch (b.index) {
						case 0:
							break;
						case 1:
							getImage();
							break;
						case 2:
							galleryImg();
							break;
						default:
							break
					}
				})	
			}
			
		});
		
		var imagesList = '';
		function getImage() {
			var c = plus.camera.getCamera();
			c.captureImage(function(e) {
				plus.io.resolveLocalFileSystemURL(e, function(entry) {
					var s = entry.toLocalURL() + "?version=" + new Date().getTime();
					var img = renderLoadImg(s,'',true);
					isTjiao = false;
					uploadHead(s,img);//上传图片
			
				}, function(e) {
					console.log("读取拍照文件错误：" + e.message);
				});
			}, function(s) {
				console.log("error" + s);
			}, {
//				filename: "_doc/head.jpg"
				filename: "_doc/img" + new Date().getTime()
			})
		}
		
		function galleryImg() {
			plus.gallery.pick(function(a) {
				plus.io.resolveLocalFileSystemURL(a, function(entry) { 
					var s = entry.toLocalURL() + "?version=" + new Date().getTime();
					var img = renderLoadImg(s,'',true);
					isTjiao = false;
					uploadHead(s,img);//上传图片
				}, function(e) {
					console.log("读取拍照文件错误：" + e.message);
				});
			}, function(a) {}, {
				filter: "image"
			})
		};
		
		//图片上传
		function uploadHead(imgPath,img) {
				var task = plus.uploader.createUpload(ajaxDomain + "/resource/upload.do", {
						method: "POST"
					},
					function(t, status) { //上传完成
						if(status == 200) {
							var fileBean = $.parseJSON(t.responseText);
							if(fileBean.result == "success") {
								isTjiao = true;	
								mui.toast("上传成功！");
								var data = fileBean.data;
								img.attr("imgId",data.id);
								imagesList=data.id;
								curImgPath = data.filepath;
								img.siblings(".bgDiv").remove();
							} else {
								mui.toast(fileBean.resume);
							}
						} else {
							mui.toast("上传失败！");
						}
					}
				);
				
				compressImage(imgPath, new Date().getTime() + ".jpg", "img", function(namePath) {
					task.addFile(namePath, {
						key: "file"
					});
					task.start();
				});
			}
		
			//压缩图片
			function compressImage(url, filename, divid, backFn) {
				var name = "_doc/" + divid + "-" + filename; //_doc/upload/F_ZDDZZ-1467602809090.jpg
				plus.zip.compressImage({
					src: url, //src: (String 类型 )压缩转换原始图片的路径
					dst: name, //压缩转换目标图片的路径
					quality: 90, //quality: (Number 类型 )压缩图片的质量.取值范围为1-100
					overwrite: true //overwrite: (Boolean 类型 )覆盖生成新文件
				},
				function(event) {
					//uploadf(event.target,divid);
					var path = name; //压缩转换目标图片的路径
					//event.target获取压缩转换后的图片url路
					//filename图片名称
					backFn(path);
				},
				function(error) {
					plus.nativeUI.toast("压缩图片失败，请稍候再试");
				});
			}
			
			function renderLoadImg(src,imgId,showLoading){
				$('.imgCop').remove();
				var imgId =  params.imgId
				var imgCop = $("<div></div>").addClass("imgCop").css({
					"position":"relative",
				});
				imgCop.html("<div style='' class='closeBtn'></div>");
				//判断是否有已经选择的图片
				var img = $('<img/>').attr('id','loadImgBox').attr({"src":src}).attr('imgId',imgId);
				img.css("margin-left","0");
				imgCop.append(img);
				if(showLoading){
					var bgDiv = $("<div/>").css({
						"opacity":0.5,
						"width":"100%",
						"height":"100%",
						"position":"absolute",
						"left":"0",
						"top":"0px",
						"background":"#555",
						"z-index":2,
						"line-height":"100%",
						"text-align":"center",
						"color":"#fff",
						"line-height":"1.5rem"
					}).html("正在上传").addClass("bgDiv");
					imgCop.append(bgDiv);
				}
				$(".imgBox").prepend(imgCop);
				img.hover(
	     			function(e){
	     				e.stopPropagation();
	     				$(this).siblings(".closeBtn").show();
	     			},function(e){
	     				e.stopPropagation();
	     				$(this).siblings(".closeBtn").hide();
	     			}
	     		);
	     		$(document).on("tap",".closeBtn",function(e){
	     			e.stopPropagation();
	     			var result= confirm("是否删除此图片？");
	     			if(result){
	     				//deleteById(imgId);
	         			$(this).parents(".imgCop").remove();
	     			}
	     		})
	     		return img;
			}
		
				
		});


