//获取页面参数
var params = null;
var opener = null;
mui.plusReady(function () {
   //获取页面参数
	params = plus.webview.currentWebview();
	opener = params.opener();
	
	//mui 初始化
	mui.init({
		swipeBack: true, //启用右滑关闭功能
		keyEventBind: {
			backbutton: true  //开启back按键监听
		},
		beforeback: function() {
	　　　　 	//获得父页面的webview
	        var list = plus.webview.currentWebview().opener();
	　　　　 	//触发父页面的自定义事件(refresh),从而进行刷新
	        mui.fire(list, 'refreshCPBJ');
	        //返回true,继续页面关闭逻辑
	        return true;
		}
	});
	var deceleration = mui.os.ios ? 0.000003 : 0.0009;
	
	var u = navigator.userAgent;
	 var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
	  if (isIOS) {
	   var originalHeight=document.documentElement.clientHeight || document.body.clientHeight;
	    window.onresize=function(){
	        var  resizeHeight=document.documentElement.clientHeight || document.body.clientHeight;
	        //软键盘弹起与隐藏  都会引起窗口的高度发生变化
	        if(resizeHeight*1<originalHeight*1){ //resizeHeight<originalHeight证明窗口被挤压了
	         mui.scrollTo("10%",0)
	//         　　  plus.webview.currentWebview().setStyle({
	//               　　 height:originalHeight
	//            　});
	        }
	    }
	
	
	 plus.webview.currentWebview().setStyle({
	 　　softinputMode: "adjustResize"  // 弹出软键盘时自动改变webview的高度
	 });
   
  }
	
	//页面主组件构建
	var pageApp = new Vue({
		el:'#appContainer',
		data:function(){
			return {
				cookerList:[],
				editText:'编辑',
				isAllSelect:false,
				isEdit:false,//是否启用编辑状态
				showGuiter:true,//是否转换添加厨师表单,
				newCookerName:''//新增的厨师姓名
			}
		},
		created:function(){
			this.mergeCookerList();
		},
		mounted:function(){
			var self = this;
			//滚动初始化
			mui('.mui-scroll-wrapper').scroll({
				deceleration: deceleration,
				indicators: false
			});
		},
		methods:{
			//厨师项点击事件函数
			itemHandle:function(index){
				var self = this;
				//编辑状态下
				if(this.isEdit){
					this.cookerList[index].isSelect = !this.cookerList[index].isSelect;
				}else{
					//设置选择值,选择后直接跳转
					var theData = {
						id:self.cookerList[index].id,
						name:self.cookerList[index].name,
						baseId:self.cookerList[index].baseid
					};
					localStorage.setItem('cookerData',JSON.stringify(theData));
					params.close();
					mui.fire(opener, 'refreshCPBJ');return true;
				};
			},
			//删除厨师事件函数
			deleteCookerList:function(){
				var self = this;
				var deleteArr = [];
				for(var i=0; i<this.cookerList.length; i++){
					if(this.cookerList[i].isSelect === true){
						deleteArr.push(this.cookerList[i].id);
					};
				};
				if(deleteArr.length === 0){
					mui.toast('请先选择要删除的厨师');
					return;
				};
				//接口调用
				mui.ajax(ajaxDomain + '/cook/deleteCookMaster.do',{
					data:{
						ids:deleteArr
					},
					type:'post',//HTTP请求类型 
					success:function(data){
						if(data.result === 'success'){
							mui.toast('删除成功');
							self.isEdit = false;
							self.editText = '编辑';
							self.mergeCookerList();
						};
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('无法连接网络');
					}
				});
			},
			//全选按钮事件函数
			toggleAllSelect:function(){
				if(this.isAllSelect){
					for(var i=0; i<this.cookerList.length; i++){
						this.cookerList[i].isSelect = false;
					};
					this.isAllSelect = false;
				}else{
					for(var i=0; i<this.cookerList.length; i++){
						this.cookerList[i].isSelect = true;
					};
					this.isAllSelect = true;
				}
			},
			//启用编辑状态
			editHandle:function(){
				if(this.editText === '编辑'){
					this.editText = '取消';
					this.isEdit = true;
				}else{
					this.editText = '编辑';
					this.isEdit = false;
				};
			},
			//取消新增厨师
			cancelAddNewCooker:function(){
				var self = this;
				this.newCookerName = '';
				this.showGuiter = true;
				this.$refs.cookerinput.blur();
			},
			//展示新增厨师form
			showAddCookerForm:function(){
				var self = this;
				this.showGuiter = false;
				self.newCookerName = '';
				self.$refs.cookerinput.focus();
			},
			//新增厨师提交
			doAddNewCooker:function(){
				var self = this;
				if(this.newCookerName === ''){
					mui.toast('姓名不可为空');
				};
				//添加厨师
				mui.ajax(ajaxDomain + '/cook/addCookMaster.do',{
					data:{
						baseid:params.baseId,
						name:self.newCookerName
					},
					type:'post',//HTTP请求类型 
					success:function(data){
						if(data.result === 'success'){
							mui.toast('添加成功');
							self.$refs.cookerinput.blur();
							self.showGuiter = true;
							self.mergeCookerList();
						}
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('无法连接网络');
					}
				});
			},
			//更新厨师列表数据
			mergeCookerList:function(){
				var self = this;
				//初始化基地选择
				mui.ajax(ajaxDomain + '/cook/getMasterList.do',{
					data:{
						baseId:params.baseId
					},
					type:'post',//HTTP请求类型
					success:function(data){
						if(data.result === 'success'){
							var result = data.data;
							for(var i=0; i<result.length; i++){
								result[i].isSelect = false;
							};
							self.cookerList = result;
						}
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('无法连接网络');
					}
				});
			}
		}
	})
})

