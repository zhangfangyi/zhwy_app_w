 mui.plusReady(function() {
 	window.addEventListener('addressRefresh', function(e){//执行刷新
	    	location.reload();
	});
 	
 		var cardnumber = localStorage.getItem("cardnumber");
		mui.ajax(ajaxDomain + "/deliveryAddress/findByCardnumber.do",{
					data:{ 
						cardnumber:cardnumber
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
					$("#listAds").html('');
					    var data = data.data;
						for (var i=0;i<data.length;i++) {
							var str= '<div class="mui-row wddzList">'+
//									  '<div class="wdListS">'+
//									  	'<div class="wdSImg"></div>'+
//									  '</div>'+
									  '<div class="wdListR" id='+ data[i].id +'>'+
									  	'<div class="wdlistImg"></div>'+
									  '</div>'+
									  '<div class="wdListL" style="margin-left:0.3rem">'+
									  	 '<div class="xmdhmr">'+
									  	 	'<span>'+data[i].consignee+'</span>'+
									  	 	'<span>'+data[i].phone + 
//									  	 		'<span class="ms"></span>'+ 
									  	 		(function(){
									  	 			if(data[i].isDefault == 1){
									  	 				return '<span class="ms"></span>';
									  	 			}else{
									  	 				return '';
									  	 			}
									  	 		})()+
									  	 	'</span>'+ 
									  	 '</div>'+ 
									  	'<div class="xmdhdz">'+
									  	 	'<span>'+data[i].area + data[i].streetAddress+'</span>'+
									  	 '</div>'+
									  '</div>'+
								'</div>';
							$("#listAds").append(str);	
						}
					}, 
					error:function(xhr,type,errorThrown){
						mui.toast('网络无法连接', {
								duration: 'long', 
								type: 'div'
							});
					}
				});
 	
 	
 	//新增
 	mui('.zfTop').on('tap', '.dzadd', function(e) {
					var url = $(this).attr("url");
					var type ="add";
					mui.openWindow({
						url: url,
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							type : type
						},
						waiting: {
							autoShow: false
						}
					});
	});
 	
 	 
 	//编辑
 	mui('#listAds').on('tap', '.wdListR', function(e) {
 					var url = "address.html";
 					var id = $(this).attr("id");
					var type ="edit";
					mui.openWindow({
						url: url,
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							type : type,
							addressId: id
						},
						waiting: {
							autoShow: false
						}
					});
 	})
 	
 })
