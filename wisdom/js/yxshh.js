/*mui.init({
	//swipeBack: true //启用右滑关闭功能
});*/
//滚动初始化
var deceleration = mui.os.ios ? 0.000003 : 0.0009;
var cardNumber = localStorage.getItem('cardnumber');
//食堂菜单部分
var shtApp = new Vue({
	el: '#pageApp',
	data: function() {
		return {
			bannerItems: [],
			foodItems: [],
			marketItems: [],
			showMarketMore: false
		}
	},
	methods: {
		foodItemHandle: function(index) {
			localStorage.setItem('shtTabIndex', this.foodItems[index].CODE);
			localStorage.setItem('merchantId', this.foodItems[index].MERCHANTCODE);
			localStorage.setItem('merchantName', this.foodItems[index].MERCHANTNAME);
			var merchantId = this.foodItems[index].MERCHANTCODE;
			mui.ajax(ajaxDomain + '/goodType/getGoodType.do', {
				data: {
					merchantCode: merchantId
				},
				type: 'post', //HTTP请求类型
				success: function(data) {
					if(data.result === 'success') {
						var resume = data.resume;
						var openTime = data.model;
						if(resume == "0") {
							$("#popover .popscst").html("当前不在食堂开放时间段");
							$("#popover .poptime").html(openTime);
							mui("#popover").popover('toggle', document.getElementById("div"));
						} else {
							var ws = plus.webview.currentWebview();
							ws.loadURL("shitang-index.html");
						}

					};
				},
				error: function(xhr, type, errorThrown) {
					//异常处理；
					mui.toast('无法连接网络');
				}
			});
		},
		makerItemHandle: function(index) {
			//外部商家
			var cardnumber = localStorage.getItem("cardnumber");
			var userName = localStorage.getItem("userRealName");
			var userPhone = localStorage.getItem("userPhone");
			if(this.marketItems[index].MERCHANTTYPE == '2') {
				localStorage.setItem('shtTabIndex', '');
				localStorage.setItem('merchantId', this.marketItems[index].CODE);
				localStorage.setItem('merchantName', this.marketItems[index].MERCHANTNAME);
				var merchantCode = this.marketItems[index].CODE;
				console.log(merchantCode)
				localStorage.setItem('theMerchartCode', merchantCode);
				var main = plus.webview.currentWebview().parent();
				mui.ajax(ajaxDomain + '/motu/queryRule.do', { //  /enjoysLife/loginMetromall ， /enjoysLife/loginSL
					data: {
						merchantCode: merchantCode
					},
					type: 'post', //HTTP请求类型
					success: function(data) {
						console.log(JSON.stringify(data.data))
						if(data.result == "success") {
							var isOpen = data.data.isOpen;
							var openTime = data.data.openTime;
							if(isOpen == 0) {
								$("#popover .popscst").html("当前不在商家开放时间段");
								$("#popover .poptime").html(openTime);
								mui("#popover").popover('toggle', document.getElementById("div"));
							} else {
								var ws = plus.webview.currentWebview();
								ws.loadURL("shitang-index.html");
							}
						}
					},
					error: function(xhr, type, errorThrown) {
						mui.toast("网络连接异常")
					}
				});
			};
			//外部商城
			/*if(this.marketItems[index].MERCHANTTYPE == '3'){
				if(this.marketItems[index].MERCHANTNAME === '麦德龙'){
					var merchantCode = this.marketItems[index].CODE;
					var main = plus.webview.currentWebview().parent(); 
						mui.ajax(ajaxDomain + '/enjoysLife/loginMetromall.do', { //  /enjoysLife/loginMetromall ， /enjoysLife/loginSL
								data: {
									cardnumber:cardnumber,
									userName:userName,
									userPhone:userPhone,
									merchantCode:merchantCode
								},
								type: 'post', //HTTP请求类型
								crossDomain: true,
								success: function(data) {
									if(data.result == "success"){ 
										var isOpen = data.isOpen;
										var openTime = data.openTime;
										if(isOpen == 0){
											$("#popover .popscst").html("当前不在商城开放时间段");
											$("#popover .poptime").html(openTime);
											mui("#popover").popover('toggle', document.getElementById("div"));
										}else{
											mui.openWindow({
												url:'outWeb.html',
												createNew:true,
												extras:{
												 	outName:'麦德龙'
												}
											})
										}
									}
								},
								error: function(xhr, type, errorThrown) {
									mui.toast("网络连接异常")
								}
					});
				};
				if(this.marketItems[index].MERCHANTNAME === '苏粮'){
					var main = plus.webview.currentWebview().parent();  
					var merchantCode = this.marketItems[index].CODE;
						mui.ajax(ajaxDomain + '/enjoysLife/loginSL.do', { //  /enjoysLife/loginMetromall ， /enjoysLife/loginSL
								data: {
									cardnumber:cardnumber,
									userName:userName,
									userPhone:userPhone,
									merchantCode:merchantCode
								},
								type: 'post', //HTTP请求类型
								crossDomain: true,
								success: function(data) {
									if(data.result == "success"){ 
										var isOpen = data.isOpen;
										var openTime = data.openTime;
										if(isOpen == 0){
											$("#popover .popscst").html("当前不在商城开放时间段");
											$("#popover .poptime").html(openTime);
											mui("#popover").popover('toggle', document.getElementById("div"));
										}else{ 
											mui.openWindow({
												url:'outWeb.html',
												createNew:true,
												extras:{
												 	outName:'苏粮'
												}
											})
										}
									}
								},
								error: function(xhr, type, errorThrown) {
									mui.toast("网络连接异常")
								}
					});
				};
			};*/
			//外部商城第二版
			if(this.marketItems[index].MERCHANTTYPE == '3') {
				var merchantCode = this.marketItems[index].CODE;
				localStorage.setItem('theMerchartCode', merchantCode);
				var main = plus.webview.currentWebview().parent();
				mui.ajax(ajaxDomain + '/enjoysLife/loginSC.do', { //  /enjoysLife/loginMetromall ， /enjoysLife/loginSL
					data: {
						cardnumber: cardnumber,
						userName: userName,
						userPhone: userPhone,
						merchantCode: merchantCode
					},
					type: 'post', //HTTP请求类型
					success: function(data) {
						//						console.log(JSON.stringify(data))
						if(data.result == "success") {
							var isOpen = data.isOpen;
							var openTime = data.openTime;
							if(isOpen == 0) {
								$("#popover .popscst").html("当前不在商城开放时间段");
								$("#popover .poptime").html(openTime);
								mui("#popover").popover('toggle', document.getElementById("div"));
							} else {
								mui.openWindow({
									url: 'outWeb.html',
									createNew: true,
									extras: {
										outName: '商城'
									}
								})
							}
						}
					},
					error: function(xhr, type, errorThrown) {
						mui.toast("网络连接异常")
					}
				});
			};
		}
	},
	mounted: function() {
		var self = this;
		mui.ajax(ajaxDomain + '/enjoysLife/getEnjoysLife.do', {
			data: {},
			type: 'get', //HTTP请求类型
			success: function(data) {
				if(data.result === 'success') {
					//数据整理
					//banner图
					for(var i = 0; i < data.goodIsBanner.length; i++) {
						data.goodIsBanner[i].PHOTONAME = ajaxDomain + data.goodIsBanner[i].PHOTONAME;
					};
					self.bannerItems = data.goodIsBanner; //data.goodIsBanner.concat(data.goodIsBanner);
					//食堂列表
					for(var i = 0; i < data.goodType.length; i++) {
						data.goodType[i].PHOTONAME = ajaxDomain + data.goodType[i].PHOTONAME;
					};
					self.foodItems = data.goodType;
					// 商城或商家
					for(var i = 0; i < data.merchant.merchantList.length; i++) {
						data.merchant.merchantList[i].PHOTONAME = ajaxDomain + data.merchant.merchantList[i].PHOTONAME;
						data.merchant.merchantList[i].bgStyle = {
							backgroundImage: 'url(' + data.merchant.merchantList[i].PHOTONAME + ')'
						};
					};
					self.marketItems = data.merchant.merchantList;
					self.showMarketMore = data.merchant.isMore;

					//获得slider插件对象
					setTimeout(function() {
						var gallery = mui('.mui-slider');
						gallery.slider({
							interval: 2000 //自动轮播周期，若为0则不自动播放，默认为0；
						});
					}, 1200);
				}
			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
				console.log(type);
			}
		});
		mui('.mui-scroll-wrapper').scroll({
			deceleration: deceleration,
			indicators: false
		});
	}
})