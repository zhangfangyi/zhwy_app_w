//获取登录用户相关信息
var baseId = localStorage.getItem('baseId');
var userId = localStorage.getItem('userId');
// 设置滚动系数
var deceleration = mui.os.ios ? 0.000003 : 0.0009;
//全局参数
var baseSelector = null;
var dateSelector = null;
//框架实例化
mui.init({
	keyEventBind: {
		backbutton: false  //关闭back按键监听
	}
});

var pageApp = new Vue({
	el:'#pageContainer',
	data:function(){
		return {
			typeText:'菜品排行',
			typeVal:'CS',//CS or PJ
			rowsData:[],
			limit:10,
			curPage:0,
			totalPage:1,
			baseId:'',
			baseName:'',
			timeArea:''
		}
	},
	created:function(){
		
	},
	mounted:function(){
		var self = this;
		//滚动初始化
		mui('.mui-scroll-wrapper').scroll({
			deceleration: deceleration,
			indicators: false
		});
		
		//初始化基地选择
		mui.ajax(ajaxDomain + '/baseInfo/getBaseListByUserId',{
			data:{
				userId:userId
			},
			type:'post',//HTTP请求类型
			success:function(data){
				if(data.result === 'success'){
					//正确逻辑应用
					if(data.data.length === 0){
						self.baseId = '',
						self.baseName = '';
					}else if(data.data.length === 1){
						self.baseId = data.data[0].id,
						self.baseName = data.data[0].name;
						mui.toast('您有且只有一个基地，已为您默认选中此基地');
					}else{
						var selectBaseIndex = 0;
						self.baseId = data.data[0].id,
						self.baseName = data.data[0].name;
						//初始化基地选择
						baseSelector = new mui.PopPicker();
						var selectorVal = data.data;
						for(var i=0; i<selectorVal.length; i++){
							selectorVal[i].value = selectorVal[i].id;
							selectorVal[i].text = selectorVal[i].name;
						};
						baseSelector.setData(selectorVal);
						baseSelector.pickers[0].setSelectedIndex(selectBaseIndex,1000);
					}
				};
			},
			error:function(xhr,type,errorThrown){
				//异常处理；
				mui.toast('无法连接网络');
			}
		});
		
		//上拉加载初始化
		mui('#pullFresh').pullToRefresh({
			up: {
				auto:true,
				callback: function(){
					 var _self = this;
					setTimeout(function() {
						self.mergeData('concat');
					}, 200);
				}
			}
		});
		
		//初始化日期 默认提供二十四周选择
		var weeker = new  WeekData();
		var weeksData = [weeker.curWeek];
		for(var i=0; i<23; i++){
			weeker.getPreWeek();
			weeksData.push(weeker.curWeek);
		};
		//初始化基地选择
		dateSelector = new mui.PopPicker();
		for(var i=0; i<weeksData.length; i++){
			weeksData[i].value = weeksData[i][0].formatDate.replace(/-/g,"/") + ' - ' + weeksData[i][6].formatDate.replace(/-/g,"/");
			weeksData[i].text = weeksData[i][0].formatDate.replace(/-/g,"/") + ' - ' + weeksData[i][6].formatDate.replace(/-/g,"/");
		};
		this.timeArea = weeksData[0].value;
		dateSelector.setData(weeksData);
		dateSelector.pickers[0].setSelectedIndex(0,1000);
		
	},
	methods:{
		//菜品排行与厨师排行的切换
		sortTypeHandle:function(){
			if(this.typeText === '厨师排行'){
				this.typeText = '菜品排行';
				this.typeVal = 'CS';
			}else{
				this.typeText = '厨师排行';
				this.typeVal = 'PJ';
			};
			//上拉刷新重置
			this.curPage = 0;
			this.totalPage=1;
			mui('#pullFresh').pullToRefresh().refresh(true);
			this.mergeData();
		},
		//获取评价列表
		mergeData:function(actionStr){ // 
			var self = this;
			this.curPage++;
			//接口调用
			mui.ajax(ajaxDomain + '/cook/getRank.do',{
				type:'post',//HTTP请求类型 
				data:{
					data:JSON.stringify({
						baseId:self.baseId,
						start:self.timeArea.split(' - ')[0].replace(/\//g,'-'),
						end:self.timeArea.split(' - ')[1].replace(/\//g,'-'),
						by:self.typeVal,// CS PJ
						page:self.curPage,
						limit:self.limit
					})
				},
				async:false,
				success:function(data){
					if(data.result === 'success'){
						if(data.data === null){
							data.data = [];
						};
						if(actionStr === 'concat'){
							self.rowsData = self.rowsData.concat(data.data);
						}else{
							self.rowsData = data.data;
						};
						self.totalPage = Math.ceil(data.count/self.limit);
						if(self.totalPage === 0){
							self.totalPage = 1;
						};
						mui('#pullFresh').pullToRefresh().endPullUpToRefresh(self.curPage == self.totalPage); 
						if(self.curPage == self.totalPage){
						setTimeout(function(){
								var btnTips = document.getElementsByClassName('mui-pull-bottom-tips')[0];
								btnTips.style.display = 'none';
							},200);
						}else{
							var btnTips = document.getElementsByClassName('mui-pull-bottom-tips')[0];
							btnTips.style.display = 'block';
						};
					};
				},
				error:function(xhr,type,errorThrown){
					//异常处理；
					mui.toast('无法连接网络');
				}
			});
		},
		//开启基地列表
		showSelector:function(){
			var self = this;
			if(baseSelector !== null){
				baseSelector.show(function(selectItems){
					self.baseId = selectItems[0].id,
					self.baseName = selectItems[0].name;
					//上拉刷新重置
					self.curPage = 0;
					self.totalPage=1;
					mui('#pullFresh').pullToRefresh().refresh(true);
					self.mergeData();
				});
			}
		},
		//开启时间列表
		showDateSelector:function(){
			var self = this;
			if(dateSelector !== null){
				dateSelector.show(function(selectItems){
					self.timeArea = selectItems[0].value,
					//上拉刷新重置
					self.curPage = 0;
					self.totalPage=1;
					mui('#pullFresh').pullToRefresh().refresh(true);
					self.mergeData();
				});
			}
		}
	}
})

