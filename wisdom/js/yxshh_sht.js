mui.init({
	swipeBack: false, //启用右滑关闭功能
	keyEventBind: {
		backbutton: true //关闭back按键监听
	}
});
//设置自定义刷新事件
window.addEventListener('refreshshtIndex', function(e) { //执行刷新
	location.reload();
});
//获取输入数据
var menuCode = localStorage.getItem('shtTabIndex');
var cardNumber = localStorage.getItem('cardnumber');
var merchantId = localStorage.getItem('merchantId');
localStorage.setItem('shtAddressCheckData', '');
var goodsLimit = 10;
//交互逻辑控制变量
var actionType = 'doNormal'; // doSearch
//滚动初始化
var deceleration = mui.os.ios ? 0.000003 : 0.0009;
var menusApp = new Vue({
	el: '#articleTabs',
	data: function() {
		return {
			menus: [],
			activeIndex: 0
		}
	},
	created: function() {
		var self = this;
		mui.ajax(ajaxDomain + '/goodType/getGoodType.do', {
			data: {
				merchantCode: merchantId
			},
			type: 'post', //HTTP请求类型
			success: function(data) {
				console.log()
				if(data.result === 'success') {
					self.menus = data.data;
					//设置选中项
					for(var i = 0; i < self.menus.length; i++) {
						if(self.menus[i].CODE === menuCode) {
							self.activeIndex = i;
							break;
						};
					};
				};
			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
				mui.toast('无法连接网络');
			}
		});
	},
	methods: {
		menuHandle: function(menuIndex) {
			this.activeIndex = menuIndex;
			var self = this;
			//当点击的是搜索结果时
			if(self.menus[menuIndex].NAME === '搜索结果') {
				headerApp.searchHandle();
				return;
			};
			//重置参数
			actionType = 'doNormal';
			goodsApp.page = 0;
			goodsApp.limit = goodsLimit;
			goodsApp.items = [];
			mui('#pullFresh_items').pullToRefresh().refresh(true);
			renderItemsData();
		}
	},
	mounted: function() {
		var _this = this;
		mui('.scroll-articleLeft').scroll({
			deceleration: deceleration,
			indicators: false,
			bounce: true
		});

	}
})

var goodsApp = new Vue({
	el: '#articleGoods',
	data: function() {
		return {
			items: [],
			page: 0,
			limit: goodsLimit
		}
	},
	methods: {
		//添加至购物车
		addToGWCH: function(index, $event) {
			var self = this;
			//判断是否超出限购数或库存
			if(headerApp.tipByGoodId(self.items[index].ID)) {
				return;
			};
			//提交到购物车
			mui.ajax(ajaxDomain + '/shoppingcart/add.do', {
				data: {
					goodId: self.items[index].ID,
					userId: cardNumber,
					num: 1,
					merchantCode: merchantId
				},
				type: 'post', //HTTP请求类型
				success: function(data) {
					if(data.result === 'success') {
						mui.toast('添加成功');
						var ele = $($event.target);
						var target = ele.parents('.good-item').eq(0).find('.good-itemImg');
						var imgSrc = target.attr('src');
						var startLeft = target.offset().left;
						var startTop = target.offset().top;
						var layerImg = $('<img />');
						layerImg.attr('src', imgSrc);
						layerImg.css({
							'position': 'absolute',
							'top': startTop, //startTop,
							'left': startLeft,
							'width': '1.5rem',
							'height': '1.5rem',
							'opacity': '0.8',
							'z-index': '10000' //startLeft

						});
						$('body').append(layerImg);
						layerImg.animate({
							'width': '20px',
							'height': '20px',
							'opacity': '0.2',
							'top': headerApp.gwchPosition.top,
							'left': headerApp.gwchPosition.left
						}, 250, function() {
							layerImg.remove();
							headerApp.getGwchData();
						})

					};
				},
				error: function(xhr, type, errorThrown) {
					//异常处理；
					mui.toast('无法连接网络');
				}
			});
			//headerApp.gwchVal += 1;
		}
	},
	mounted: function() {
		var self = this;
		mui('.scroll-articleRight').scroll({
			deceleration: deceleration,
			indicators: false
		});
	}
})
renderPullFresh();
//renderItemsData();
//上拉加载方法封装
function renderPullFresh() {
	//上拉加载初始化
	mui('#pullFresh_items').pullToRefresh({
		up: {
			auto: true,
			callback: function() {
				var _self = this;
				setTimeout(function() {
					renderItemsData();
				}, 200);
			}
		}
	});
}

function renderItemsData() {
	goodsApp.page++;
	if(actionType === 'doNormal') {
		mui.ajax(ajaxDomain + '/goodInfo/getGoodInfo.do', {
			data: {
				typeId: menusApp.menus.length > 0 ? menusApp.menus[menusApp.activeIndex].CODE : menuCode,
				page: goodsApp.page,
				limit: goodsApp.limit
			},
			type: 'post', //HTTP请求类型
			success: function(data) {
				if(data.result === 'success') {
					var result = data.data.data;
					for(var i = 0; i < result.length; i++) {
						result[i].PHOTONAME && (result[i].PHOTONAME = ajaxDomain + result[i].PHOTONAME);
					};
					goodsApp.items = goodsApp.items.concat(result);
					//设置上拉刷新状态
					mui('#pullFresh_items').pullToRefresh().endPullUpToRefresh(data.data.isBottom);
					if(data.data.isBottom) {
						setTimeout(function() {
							var btnTips = document.getElementsByClassName('mui-pull-bottom-tips')[0];
							btnTips.style.display = 'none';
						}, 200);
					} else {
						var btnTips = document.getElementsByClassName('mui-pull-bottom-tips')[0];
						btnTips.style.display = 'block';
					};
				};
			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
				mui.toast('无法连接网络');
			}
		});
	};
	if(actionType === 'doSearch') {
		mui.ajax(ajaxDomain + '/goodInfo/searchGood.do', {
			data: {
				name: headerApp.val,
				merchantCode: merchantId,
				page: goodsApp.page,
				limit: goodsApp.limit
			},
			type: 'post', //HTTP请求类型
			success: function(data) {
				if(data.result === 'success') {
					var result = data.data.data;
					for(var i = 0; i < result.length; i++) {
						result[i].PHOTONAME && (result[i].PHOTONAME = ajaxDomain + result[i].PHOTONAME);
					};
					goodsApp.items = goodsApp.items.concat(result);
					mui('#pullFresh_items').pullToRefresh().endPullUpToRefresh(data.data.isBottom);
					if(data.data.isBottom) {
						setTimeout(function() {
							var btnTips = document.getElementsByClassName('mui-pull-bottom-tips')[0];
							btnTips.style.display = 'none';
						}, 200);
					} else {
						var btnTips = document.getElementsByClassName('mui-pull-bottom-tips')[0];
						btnTips.style.display = 'block';
					};
				};
			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
				mui.toast('无法连接网络');
			}
		});
	}
}

var headerApp = new Vue({
	el: '#header-sht',
	data: {
		val: '',
		gwchVal: []
	},
	created: function() {
		this.getGwchData();
	},
	mounted: function() {
		var gwchEleOffset = $('.header-gouwuche').offset();
		this.gwchPosition = {
			top: '0.54rem',
			left: '6.8rem'
		}
	},
	methods: {
		backHandle: function() {
			plus.webview.getWebviewById("tab-webview-yxsh.html").loadURL("tab-webview-yxsh.html");
		},
		//判断商品选购量与库存
		tipByGoodId: function(goodId) {
			var result = false;
			for(var i = 0; i < this.gwchVal.length; i++) {
				if(this.gwchVal[i].GOODID === goodId) {
					if(this.gwchVal[i].SHOPPINGNUM >= this.gwchVal[i].SPQ) {
						result = true;
						mui.toast('您已达到此商品的最大可购数，已不能继续选购！');
						break;
					}
				}
			};
			return result;
		},
		getGwchData: function() {
			var self = this;
			mui.ajax(ajaxDomain + '/shoppingcart/findByUserId.do', {
				data: {
					cardnumber: cardNumber,
					merchantCode: merchantId
				},
				type: 'post', //HTTP请求类型
				success: function(data) {
					if(data.result === 'success' && data.data !== null) {
						self.gwchVal = data.data;
					};
				},
				error: function(xhr, type, errorThrown) {
					//异常处理；
					mui.toast('无法连接网络');
				}
			});
		},
		searchHandle: function() {
			if(this.val === '') {
				if(menusApp.menus[0].NAME === '搜索结果') {
					menusApp.menus.splice(0, 1);
					menusApp.menuHandle(0);
				};
			} else {
				//添加搜索结果菜单
				if(menusApp.menus[0].NAME !== '搜索结果') {
					menusApp.menus.unshift({
						NAME: '搜索结果'
					});
					menusApp.activeIndex = 0;
				};

				//数据请求
				var self = this;
				//重置参数
				actionType = 'doSearch';
				goodsApp.page = 0;
				goodsApp.limit = goodsLimit;
				goodsApp.items = [];
				mui('#pullFresh_items').pullToRefresh().refresh(true);
				renderItemsData();
			}
		},
		toggleShade: function() {
			if(shadeApp.showDom === false) {
				if(this.gwchVal.length === 0) {
					mui.toast('购物车中还没有选购商品呢，请先选择商品哦~');
					return;
				};
				shadeApp.checkItems = this.gwchVal;
			};
			shadeApp.showDom = !shadeApp.showDom;
		}
	}
})

var shadeApp = new Vue({
	el: '#shade',
	data: {
		showDom: false,
		checkItems: [],
	},
	methods: {
		checkedToMore: function(index) {
			var self = this;
			if(headerApp.tipByGoodId(this.checkItems[index].GOODID)) {
				return;
			};
			var curSelectNum = self.checkItems[index].SHOPPINGNUM;
			mui.ajax(ajaxDomain + '/shoppingcart/modifyNum.do', {
				data: {
					id: self.checkItems[index].ID,
					num: ++curSelectNum
				},
				type: 'post', //HTTP请求类型
				success: function(data) {
					if(data.result === 'success') {
						++self.checkItems[index].SHOPPINGNUM;
						mui.toast('商品增加成功');
						if(self.checkItems[index].SHOPPINGNUM === 1) {
							mui.ajax(ajaxDomain + '/shoppingcart/add.do', {
								data: {
									goodId: self.checkItems[index].GOODID,
									userId: cardNumber,
									num: 1,
									merchantCode: merchantId
								},
								type: 'post', //HTTP请求类型
								success: function(data) {
									if(data.result === 'success') {
										headerApp.getGwchData();
									};
								},
								error: function(xhr, type, errorThrown) {
									//异常处理；
									mui.toast('无法连接网络');
								}
							});
						};
					};
				},
				error: function(xhr, type, errorThrown) {
					//异常处理；
					mui.toast('无法连接网络');
				}
			});
		},
		checkedToLess: function(index) {
			var self = this;
			var curSelectNum = self.checkItems[index].SHOPPINGNUM;
			mui.ajax(ajaxDomain + '/shoppingcart/modifyNum.do', {
				data: {
					id: self.checkItems[index].ID,
					num: --curSelectNum
				},
				type: 'post', //HTTP请求类型
				success: function(data) {
					if(data.result === 'success') {
						--self.checkItems[index].SHOPPINGNUM;
						mui.toast('商品减少成功');
						if(self.checkItems[index].SHOPPINGNUM === 0) {
							self.checkItems.splice(index, 1);
							headerApp.getGwchData();
						};
					};
				},
				error: function(xhr, type, errorThrown) {
					//异常处理；
					mui.toast('无法连接网络');
				}
			});
			//this.checkItems[index].checkCounts -= 1;
		},
		hideThis: function() {
			this.showDom = false;
		}
	},
	computed: {
		getTotal: function() {
			var result = 0;
			for(var i = 0; i < this.checkItems.length; i++) {
				result += (this.checkItems[i].PRICE * this.checkItems[i].SHOPPINGNUM);
			};
			return result.toFixed(2);
		}
	},
	mounted: function() {
		var self = this;
		mui('.scroll-checkedItems').scroll({
			deceleration: deceleration,
			indicators: true
		});

		//结算跳转
		mui('.shade-container').on('tap', '.balance-btn', function(e) {
			//判断是否有数据
			if(headerApp.gwchVal.length === 0) {
				mui.toast('亲，当前购物车中没有有效选择的商品哦，请先选购想要的商品吧~')
				return;
			};
			self.showDom = false;
			headerApp.gwchVal = [];
			var webview = mui.openWindow({
				url: 'sht-ddqr.html',
				extras: {
					merchantId: merchantId
				}
			});
		});
	}
})