		//测试数据
		window.addEventListener('refresh1', function(e) { //执行刷新
			location.reload();
		});
		mui.plusReady(function() {
			var role = localStorage.getItem("role");
			//url = "wybx.html";
			if(role.indexOf("基地领导") != -1) {
				var statusType = false; //true用户
				var usertype = 2;
			} else if(role.indexOf("基地经理") != -1) {
				var statusType = false; //true用户
				var usertype = 2;
			} else {
				var usertype = 1;
				var statusType = true; //true用户
			}
			var self = plus.webview.currentWebview();
			var bxId = self.bxId;
			var finalTime = self.finalTime1;
			var username = localStorage.getItem("userName");

			//获取详情数据
			function getRepairDetails() {
				mui.ajax(ajaxDomain + "/repair/getRepairDetails.do", {
					data: {
						id: bxId
					},
					dataType: 'json',
					type: 'post', //HTTP请求类型
					success: function(data) {
						//创建头部内容
						var data1 = data.data.repair;
						if(data1.urge == 1) {
							$(".topRow1_span2").show().html("催办");
						}
						if(data1.status >= 3) {
							$(".leaderDiv").hide();
							$(".submitCop").hide();
						}

						//状态
						if(data.data.isDo) { //显示维修完毕
							firstLoad = true;
							$(".submit").html("维修完毕");
						} else {
							if(finalTime == "undefined" || finalTime == undefined) {
								$(".submit").html("到达现场");
							} else {
								$(".submit").html("到达   现场（剩余" + finalTime + "分钟）");
							}
						}
						if(data1.status == 1) {
							$(".submitCop").hide();
						}
						$(".topRow1_span1").html("报修人: " + data1.username);
						$(".topRow2_span1").html(data1.createtime);
						$(".topRow3_span1").html("隶属基地: " + data1.basename);
						//创建中间内容
						$(".centerRow1").html(data1.content);
						/*$(".centerImg img").attr({
							"src":ajaxDomain+data1.photoname
						});*/
						//放入图片
						var data2 = data.data.files;
						if(data2.length > 0) {
							for(var i = 0; i < data2.length; i++) {
								var img = $("<img/>").attr({
									"src": ajaxDomain + data2[i].photoname
								});
								$(".centerImg").append(img);
							}
						}
						//放入已处理完毕内容
						if(data1.status == 3) {
							$(".bxTextCon").show();
							$(".bxPciCon").show();
							var data5 = data.data.mend;
							var div = $("<div/>").css({}).html(data5.content);
							$(".bxTextCon").append(div);

							var data4 = data.data.materiel;
							if(data4.length > 0) {
								/*for(var i=0;i<data4.length;i++){
									var div = $("<div/>").css({
										
									}).html(data4[i].rcname +"X"+ data4[i].num);
									$(".bxTextCon").append(div);
								}*/
							};

							/*var data3 = data.data.mendpic;
										if(data3.length>0){
											for(var i=0;i<data3.length;i++){
												var img = $("<img/>").attr({
													"src":ajaxDomain+data3[i].photoname
												}).css({
														"height": "3.92rem",
														"width":"100%"
												});
												$(".bxPciCon").append(img);
											}
										} */

						}

						var bottomData = data.data.messages;
						var items = [];
						for(var i = 0, len = bottomData.length; i < len; i++) {
							var item = [
								'<li class="bottomLi">',
								'<div class="li_left">',
								'<img src="', ajaxDomain + bottomData[i].photoname, '"/>',
								'</div>',
								'<div class="li_right">',
								'<div class="li_right_row1">',
								'<span class="li_right_span1">', bottomData[i].username, '</span>',
								'<span class="li_right_span2 ', (function() {
									if(bottomData[i].usertype == "1") {
										return 'cls0';
									} else {
										return 'cls1';
									}
								})(), '">', (function() {
									var usertype = bottomData[i].usertype;
									if(usertype == "1") {
										return "报修人";
									} else if(usertype == "2") {
										return "基地经理";
									} else if(usertype == "3") {
										return "基地领导";
									} else if(usertype == "4") {
										return "维修工人";
									}
								})(), '</span>',
								'<span class="li_right_span3">', (function() {
									var item = bottomData[i].createtime.substring(0, 11);
									return item;
								})(), '</span>',
								'</div>',
								'<p class="li_right_row2">', !bottomData[i].content ? "" : bottomData[i].content, '</p>',
								'<div class="imgBoxs">',
								(function(i) {
									if(bottomData[i].jpgs == undefined || bottomData[i].jpgs.length == 0) {
										return;
									}
									if(bottomData[i].jpgs.length != 0) {
										var node = [];
										for(var j = 0; j < bottomData[i].jpgs.length; j++) {
											if(j == 0) {
												var temp1 = "0px"
											} else {
												var temp1 = "13px";
											}
											var img = "<img src='" + ajaxDomain + bottomData[i].jpgs[j].photoname +
												"' data-preview-src='' data-preview-group='1'  style='display:inline-block;width:1.76rem;height:1.48rem;margin-left:" + temp1 + "'/>";
											node.push(img);
										};
										return node.join('');
									}
								})(i), '</div>',
								'</div>',
								'</li>',
							].join('');
							items.push(item);
						};
						$(".bottomUl").append(items.join(''));
						//底部内容

					},
					error: function(xhr, type, errorThrown) {
						//异常处理；
					}
				});
			}

			var firstLoad = false;
			//到达现场（剩余15分钟）

			getRepairDetails();
			mui.init({
				beforeback: function() {　　
					//获得父页面的webview
					var list = plus.webview.currentWebview().opener();　　
					//触发父页面的自定义事件(refresh),从而进行刷新
					mui.fire(list, 'refresh1');
					//返回true,继续页面关闭逻辑
					return true;
				},
				swipeBack: true //启用右滑关闭功能
			});
			var deceleration = mui.os.ios ? 0.003 : 0.0009;
			mui('.mui-scroll-wrapper').scroll({
				deceleration: deceleration,
				indicators: false
			});

			//返回按钮
			mui('.smailNav').on('tap', '.back', function(e) {
				mui.back();
			});

			//点击立即处理
			$(document).on('tap', '.submit', function() {
				if(!firstLoad) { //到达现场
					mui.openWindow({
						url: "wyToSpot.html",
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"tag": "wyToSpot",
							"bxId": bxId,
						},
						waiting: {
							autoShow: false
						}
					});
				} else { //维修完毕
					mui.openWindow({
						url: "wywxCompletedEngineer.html",
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"tag": "wywxCompletedEngineer",
							"bxId": bxId,
						},
						waiting: {
							autoShow: false
						}
					});
				}

			})

		});