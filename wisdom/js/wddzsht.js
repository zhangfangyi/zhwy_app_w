 mui.plusReady(function() {
 	window.addEventListener('addressRefresh', function(e){//执行刷新
	    	location.reload();
	});
	
	var cardnumber = localStorage.getItem("cardnumber");
 	
 	var contentApp = new Vue({
 		el:'#listAds',
 		data:function(){
 			return {
 				addressList:[],
 				activeIndex:-1
 			}
 		},
 		created:function(){
 			var self = this;
 			mui.ajax(ajaxDomain + "/deliveryAddress/findByCardnumber.do",{
					data:{ 
						cardnumber:cardnumber
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						if(data.result === 'success'){
							var result = data.data;
							for(var i=0; i<self.addressList.length; i++){
								if(self.addressList[i].isDefault == 1){
									self.activeIndex = i;
								}
							}
							self.addressList = result;
						}
					}, 
					error:function(xhr,type,errorThrown){
						mui.toast('网络无法连接', {
							duration: 'long', 
							type: 'div'
						});
					}
				});
 		},
 		mounted:function(){
 			
 		},
 		methods:{
 			itemSelect:function(index){
 				this.activeIndex = index;
 				localStorage.setItem('shtAddressCheckData',JSON.stringify(this.addressList[index]));
 			},
 			editAddress:function($event,index){
 				var addressId = this.addressList[index].id;
 				$event.stopPropagation();
 				mui.openWindow({
					url: 'address.html',
					styles: {
						popGesture: "close"
					},
					createNew:true,
					show: {
						aniShow: "pop-in"
					},
					extras: {
						type : 'edit',
						addressId:addressId
					},
					waiting: {
						autoShow: false
					}
			});
 			}
 		}
 	})
 	
 	//新增
 	mui('.zfTop').on('tap', '.dzadd', function(e) {
					var url = $(this).attr("url");
					var type ="add";
					mui.openWindow({
						url: url,
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							type : 'add'
						},
						waiting: {
							autoShow: false
						}
					});
	});
 	
 	 
 	//编辑
 	mui('#listAds').on('tap', '.wdListR', function(e) {
 					e.stopPropagation();
 					var url = "address.html";
 					var id = $(this).attr("id");
					var type ="edit";
					mui.openWindow({
						url: url,
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							type : type,
							addressId: id
						},
						waiting: {
							autoShow: false
						}
					});
 	})
 	
 })

