window.addEventListener('refresh1', function(e) { //执行刷新
	location.reload();
});
$(function() {
	//判断用户角色
	//var userRol = "commonUser";
	//var userRol = "leaderUser";
	//var userRol = "baseManager";
	var role = localStorage.getItem("role");
	//url = "wybx.html";
	if(role.indexOf("基地领导") != -1 || role.indexOf("物业领导") != -1) {
		var userRol = "leaderUser";
	} else if(role.indexOf("基地经理") != -1) {
		var userRol = "baseManager"
	} else {
		var userRol = "commonUser";
	}
	//获取数据 
	var count; //总数 
	var pageStart = 1;
	var firstLoad = true;
	var self;
	var defaultBaseId = "";
	var selfCon, selfConNum = false;

	function getRepairList(status, baseId, doms) {
		var djson = {
			userId: userId,
			status: status,
			page: pageStart,
			baseId: baseId,
			limit: 5
		}
		mui.ajax(ajaxDomain + "/repair/getRepairList.do", {
			data: {
				data: JSON.stringify(djson)
			},
			dataType: 'json',
			type: 'post', //HTTP请求类型
			success: function(data) {
				var needData = data.data;
				count = data.count;
				//count = data.count;
				//创建dom
				if(needData.length > 0) {
					//判断添加元素
					var nodes = doms;
					var node = [];
					for(var j = 0, len = needData.length; j < len; j++) {
						var dom = [
							'<li class="mainBodyLi">',
							'<div class="conBox" bxId="', needData[j].id, '">',
							'<div class="desText">',
							'<span id="desTextSpan">', needData[j].content, '</span>',
							(function(j) {
								if(needData[j].status == "1" || needData[j].status == "2" || needData[j].status == "0") {
									if(needData[j].urge == "1") {
										return '<div class="desStatus" >催办</div>';
									}
								}
							})(j),
							'</div>',
							'<div class="desTime">', needData[j].createtime, '</div>',
							'</div>',
							'<div class="btnBox">',
							'</div>',
							'</li>',
						].join('');
						node.push(dom);
					}
					nodes.append(node.join(''));
					if(firstLoad) {
						$(".mui-pull-bottom-tips").hide();
						firstLoad = false;
					}
					pageStart = 2;

				} else {
					$(".mui-pull-bottom-tips").hide();
					doms.empty();
					//加入暂无数据
					var noData = $("<div/>").css({
						"text-align": "center",
						"width": "100%",
						"height": "10rem"
					}).addClass("noData");
					doms.append(noData);
				}

			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
			}
		});
	}

	function getRepairListAgo(status, baseId, doms) {
		var djson = {
			userId: userId,
			status: status,
			page: pageStart,
			baseId: baseId,
			limit: 5
		}
		mui.ajax(ajaxDomain + "/repair/getRepairList.do", {
			data: {
				data: JSON.stringify(djson)
			},
			dataType: 'json',
			type: 'post', //HTTP请求类型
			success: function(data) {
				var needData = data.data;
				count = data.count;
				//count = data.count;
				//创建dom
				if(needData.length > 0) {
					//判断添加元素
					var nodes = doms;
					var node = [];
					for(var j = 0, len = needData.length; j < len; j++) {
						var dom = [
							'<li class="mainBodyLi">',
							'<div class="conBox" bxId="', needData[j].id, '">',
							'<div class="desText">',
							'<span id="desTextSpan">', needData[j].content, '</span>',
							(function(j) {
								if(needData[j].status == "1" || needData[j].status == "2" || needData[j].status == "0") {
									if(needData[j].urge == "1") {
										return '<div class="desStatus" >催办</div>';
									}
								}
							})(j),
							'</div>',
							'<div class="desTime">', needData[j].createtime, '</div>',
							'</div>',
							'<div class="btnBox">',
							/*(function(j) {
								var cop = [];
								if(needData[j].urge==0){
									var btnCls1 = '<span class="btnCls btnCB" zid="'+needData[j].id+'">催办</span>';
									cop.push(btnCls1);
								}
								if(needData[j].urge!=2){
									var btnCls2 = '<span class="btnCls btnQX" zid="'+needData[j].id+'">取消</span>';
									cop.push(btnCls2);
								}
								return cop.join('');
							})(j),*/
							'</div>',
							'</li>',
						].join('');
						node.push(dom);
					}
					$(nodes).append(node.join(''));
					pageStart++;
				}

			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
			}
		});
	}
	//上拉刷新
	function getRepairList1(status, baseId, doms) {
		var djson = {
			userId: userId,
			status: status,
			baseId: baseId,
			page: 1,
			limit: 5
		}
		mui.ajax(ajaxDomain + "/repair/getRepairList.do", {
			data: {
				data: JSON.stringify(djson)
			},
			dataType: 'json',
			type: 'post', //HTTP请求类型
			success: function(data) {
				var needData = data.data;
				count = data.count;
				//count = data.count;
				//创建dom
				if(needData.length > 0) {
					var node = [];
					var nodes = doms;
					$(nodes).empty();
					for(var j = 0, len = needData.length; j < len; j++) {
						var dom = [
							'<li class="mainBodyLi">',
							'<div class="conBox" bxId="', needData[j].id, '">',
							'<p class="desText">', needData[j].content, '</p>',
							'<div class="desTime">', needData[j].createtime, '</div>',
							'</div>',
							'<div class="btnBox">',
							/*(function(j) {
								var cop = [];
								if(needData[j].urge==0){
									var btnCls1 = '<span class="btnCls btnCB" zid="'+needData[j].id+'">催办</span>';
									cop.push(btnCls1);
								}
								if(needData[j].urge!=2){
									var btnCls2 = '<span class="btnCls btnQX" zid="'+needData[j].id+'">取消</span>';
									cop.push(btnCls2);
								}
								return cop.join('');
							})(j),*/
							'</div>',
							'</li>',
						].join('');
						node.push(dom);
					}
					$(nodes).append(node.join(''));
					pageStart = 2;
				}

			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
			}
		});
	}
	//获取基地经理数据
	function getBaseListByUserId() {
		mui.ajax(ajaxDomain + "/baseInfo/getBaseListByUserId.do", {
			data: {
				userId: userId
			},
			dataType: 'json',
			type: 'post', //HTTP请求类型
			success: function(data) {
				var data = data.data;
				//创建头部内容
				var dom1 = [
					'<li class="baseLi">',
					'<div class="baseBox">',
					'<div class="baseNameCur" activeIndex = 0>',

					'</div>',
					/*'<div class="baseNameBox" style="display:none">',
							
					'</div>',*/
					'</div>',
					'<div class="baseDownBtn showUserPicker1">',

					'</div>',
					'</li>',
					'<li class="topCommonLi1 active">待处理</li>',
					'<li class="topCommonLi1 ">已处理</li>',
					'<li class="topCommonLi1">已完成</li>',
				].join('');
				$(".topUl").append(dom1);
				var setData = [];
				/*	for(var i=0;i<data.length;i++){
						var bindData = $.extend(true,{},data[i]);
						var div = $("<div/>").addClass("baseName").data("domdata",bindData).html(data[i].name);
						$(".baseNameBox").append(div);
					}*/
				if(data.length > 0) {
					for(var i = 0; i < data.length; i++) {
						var obj = {};
						obj["text"] = data[i].name;
						obj["value"] = data[i].name;
						obj["index"] = i;
						var cop = $.extend(true, {}, data[i], obj);
						setData.push(cop);
					}
					$(".baseNameCur").html(data[0].name);
					defaultBaseId = data[0].id;
				} else {
					$(".baseNameCur").html("未设定基地");
					defaultBaseId = baseId;
				}

				//获取数据---默认显示第一个基地的数据
				var doms = $(".cntBigCop .mainBodyUl");
				getRepairList(1, defaultBaseId, doms);
				(function($, doc, jq) {
					$.init();
					var curNum = 1;
					$.ready(function() {
						var userPicker = new $.PopPicker();
						userPicker.setData(setData);
						var showUserPickerButton = doc.getElementsByClassName('showUserPicker1');
						var userResult = doc.getElementsByClassName('baseNameCur');
						for(var i = 0; i < showUserPickerButton.length; i++) {
							(function(i) {
								showUserPickerButton[i].addEventListener('tap', function(event) {
									userPicker.show(function(items) {
										userResult[i].innerText = items[0].text;
										userResult[i]["bb"] = items[0].id;
										var index = items[0].index;
										var baseId = items[0].id;
										defaultBaseId = baseId;
										jq(".cntBigCop .mainBodyUl").empty();
										var doms = jq(".cntBigCop .mainBodyUl");
										pageStart = 1;
										getRepairList(1, defaultBaseId, doms);
										if(selfConNum) {
											selfCon.endPullUpToRefresh(false);
											jq(".mui-pull-bottom-tips").hide();
										}

										//返回 false 可以阻止选择框的关闭
										//return false; 
									});
								}, false);
							})(i)

						}

					});
				})(mui, document, $);

				(function($, jq) {
					//阻尼系数 
					var deceleration = mui.os.ios ? 0.003 : 0.0009;
					$('.mui-scroll-wrapper').scroll({
						bounce: false,
						indicators: true, //是否显示滚动条
						deceleration: deceleration
					});

					$.ready(function() {
						$("#test").pullToRefresh({
							down: {
								callback: function() {
									selfCon = this;
									var activeIndex = jq(".topCommonLi1.active").index();
									jq(".mui-pull-bottom-tips").hide();
									selfCon.endPullUpToRefresh(false);
									setTimeout(function() {
										var doms = $(".cntBigCop .mainBodyUl");
										getRepairList1(activeIndex, defaultBaseId, doms);
										selfCon.endPullDownToRefresh(true);
										selfCon.refresh(true);
									}, 100);
								}
							},
							up: {
								callback: function() {
									selfCon = this;
									selfConNum = true;
									//获取当前基地
									var activeIndex = jq(".topCommonLi1.active").index();
									jq(".mui-pull-bottom-tips").show();
									setTimeout(function() {
										var doms = $(".cntBigCop .mainBodyUl");
										getRepairListAgo(activeIndex, defaultBaseId, doms);
										var list = $(".mainBodyLi").length;
										if(count <= list) {
											selfCon.endPullUpToRefresh(true)
										} else {
											selfCon.endPullUpToRefresh(false)
										}
									}, 500);
								}
							}
						});
					});
				})(mui, jQuery);

				//点击下拉事件 
				/*$(".baseDownBtn").on('tap',function(){
					$(".baseNameBox").slideToggle();
				}); */
				//点击选择基地
				/*$(".baseName").on('tap',function(){ 
					var index = $(this).index();
					$(".baseNameCur").attr({
						"activeIndex":index
					}).html($(this).html());
					$(this).parent().slideToggle();
					$('.topCommonLi1').eq(0).addClass('active').siblings().removeClass('active');
					var baseId = $(this).data("domdata").id;
					console.log(baseId)
					defaultBaseId = baseId;
					$(".cntBigCop .mainBodyUl").empty();
				 	var doms = $(".cntBigCop .mainBodyUl");
				 	pageStart=1;
					getRepairList(1,defaultBaseId,doms);
					if(selfConNum){
						selfCon.endPullUpToRefresh(false);
						$(".mui-pull-bottom-tips").hide();
					}
					
				});*/
				//左右切换 
				var tap_first = false;
				$(".topCommonLi1").on('tap', function() {
					if(!tap_first) {
						tap_first = true;
						if(!$(this).hasClass("active")) {
							$(".cntBigCop .mainBodyUl").empty();
							$(".mui-pull-bottom-tips").hide();
							var doms = $(".cntBigCop .mainBodyUl");
							if($(this).html() == "待处理") {
								pageStart = 1;
								getRepairList(1, defaultBaseId, doms);
								$(".moreBox").show();
							} else if($(this).html() == "已处理") {
								pageStart = 1;
								getRepairList(2, defaultBaseId, doms);
								$(".moreBox").hide();
							} else {
								pageStart = 1;
								getRepairList(3, defaultBaseId, doms);
								$(".moreBox").hide();
							}
							if(selfConNum) {
								selfCon.endPullUpToRefresh(false);
								$(".mui-pull-bottom-tips").hide();
							}
							$(this).addClass('active');
							$(this).siblings().removeClass('active');
							/*$(".contentDiv .cntBigCop").eq(activeBaseIndex).find(".mainBodyUl").eq(index).show();
							$(".contentDiv .cntBigCop").eq(activeBaseIndex).find(".mainBodyUl").eq(index).siblings().hide();*/
						}
						setTimeout(function() {
							tap_first = false;
						}, 1000);
					}
				});
				//点击条目进入详情页面
				$(document).on('tap', '.conBox', function() {
					var bxId = $(this).attr("bxId");
					mui.openWindow({
						url: "wyxq.html",
						id: "wyxq",
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"tag": "wyxq",
							"bxId": bxId
						},
						waiting: {
							autoShow: false
						}
					});
				});

			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
			}
		});
	}

	if(userRol == "baseManager" || userRol == "leaderUser") {
		//查看报修
		$(".seePic").show();
		$(".seePic").on('tap', function() {
			var id = $(this).attr("class");
			mui.openWindow({
				url: "wyyb.html",
				id: id,
				styles: {
					popGesture: "close"
				},
				show: {
					aniShow: "pop-in"
				},
				extras: {
					"tag": "wyyb"
				},
				waiting: {
					autoShow: false
				}
			});
		});
		//$(".moreBox").hide();
		//基地经理所在基地
		getBaseListByUserId();

	}
	//点击新增
	$(".morePic").on('tap', function() {
		//var id = $(this).attr("class");
		mui.openWindow({
			url: "wyxjbx.html",
			id: defaultBaseId,
			styles: {
				popGesture: "close"
			},
			show: {
				aniShow: "pop-in"
			},
			extras: {
				"tag": "wyxjbx"
			},
			waiting: {
				autoShow: false
			}
		});
	})
	//返回按钮
	mui('.smailNav').on('tap', '.back', function(e) {
		mui.back();
	});
	mui.init({
		swipeBack: true //启用右滑关闭功能
	});

})