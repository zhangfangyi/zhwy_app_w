 mui.plusReady(function() {
 	var listId = plus.webview.currentWebview().xqId;
 	mui.ajax(ajaxDomain + "/orderInfo/getOrderInfoDetail.do",{
					data:{
						 id:listId
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						var xqData = data.data;
						if(xqData.LOGISTICINFO){
							$("#xm").text(xqData.USERNAMEREAL);
							$(".dh").text(xqData.PHONENUMBER);
							$(".dizhi").text(xqData.ADDRESS);
							$("#wbdz").text(xqData.LOGISTICINFO);
							$("#time").text(xqData.DATETIME);
							$("#message").show();
						}else{ 
							var headStr='<div class="article-before" id="addressInfo">'+
											'<div class="before-part1">'+
												'<div class="before-left">'+
													'<div class="icon-positionWrap">'+
														'<img src="css/images/icon_position.png"/ class="icon-position" />'+
													'</div>'+
													'<div class="address-detail">'+
														'<p class="address-title">自提地点:</p>'+
														'<p class="address-address" style="font-size: 0.36rem;">'+xqData.ADDRESS+'</p>'+
													'</div>'+
												'</div>'+
											'</div>'+
										'<div class="before-part2" v-if="showTimeDom">'+
											'<p class="takeTime-title">自提时间：</p>'+
											'<div class="takeTime-detail">'+
												'<p class="takeTime-date">'+xqData.DATETIME+'</p>'+
											'</div>'+
										'</div>'+
									'</div>';
							$(".mui-scroll").prepend(headStr);	
						}
						var goodsData = data.data.GOODSDATA;
						$(".xqlist").html('');
						for(var i=0;i<goodsData.length;i++){
							var str = '<div class="ddzl">'+
											'<div class="ddzlLeft">'+
												'<div class="ddzlLeftImg">'+
													'<img src=' +ajaxDomain + goodsData[i].PHOTONAME+ '>'+
												'</div>'+
											'</div>'+
											'<div class="ddzlRight">'+
												'<ul>'+
													'<li>'+ goodsData[i].GOODNAME +'</li>'+
													'<li>'+ goodsData[i].NUMS+goodsData[i].NORMS +'</li>'+
													'<li>'+goodsData[i].PRICE+'<span>×'+goodsData[i].BAYCOUNT+'</li>'+
												'</ul>'+
											'</div>'+
										'</div>';
							$(".xqlist").append(str);	
						};
						$("#fgfs").text(xqData.PAYTYPE);
						$("#ddbh").text(xqData.ORDERID);
						$("#ddrq").text(xqData.DATETIME);
						$("#ddzje").text(xqData.PAYSUM);
						
					}, 
					error:function(xhr,type,errorThrown){
						mui.toast('网络无法连接', {
								duration: 'long', 
								type: 'div'
							});
					}
				});
 	
 	
 });