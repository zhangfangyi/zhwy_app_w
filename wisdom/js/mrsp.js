//获取页面参数
var params = null;
var opener = null;
var headerApp = null;
var foodApp = null;
//全局参数
var baseSelector = null;
mui.plusReady(function () {
	//获取页面参数
	params = plus.webview.currentWebview();
	opener = params.opener();
})
//
	var test = false;
	var roleAction = {};
	//初始化当前用户职级并根据职级确定操作权限
	var role = test?'厨师长,员工':localStorage.getItem('role').split(','),roleAction = null;
	if(role.indexOf('厨师长') !== -1){
		roleAction = {
			doComment:true,
			doAddItem:true,
			doViewReport:true,
			doBaseCheck:true
		};
	}else if(role.indexOf('基地经理') !== -1){
		roleAction = {
			doComment:true,
			doAddItem:false,
			doViewReport:true,
			doBaseCheck:true
		};
	}else if(role.indexOf('基地领导') !== -1){
		roleAction = {
			doComment:true,
			doAddItem:false,
			doViewReport:true,
			doBaseCheck:true
		};
	}else if(role.indexOf('餐饮领导') !== -1){
		roleAction = {
			doComment:true,
			doAddItem:false,
			doViewReport:true,
			doBaseCheck:true
		};
	}else{
		roleAction = {
			doComment:true,
			doAddItem:false,
			doViewReport:false,
			doBaseCheck:false
		};
	};
	
	//时间数据初始化
	var weeker = new WeekData();
	weeker.eightDays = JSON.parse(JSON.stringify(weeker.featureSevenDaysTime));
	weeker.eightDays.push(weeker.curToday);
	//获取登录用户相关信息
	var baseId = localStorage.getItem('baseId');
	var userId = localStorage.getItem('userId');
	
	//mui初始化
	mui.init({
		swipeBack: false, //启用右滑关闭功能
		gestureConfig:{
			tap: true //默认为true
	   }
	});
	// 设置滚动系数
	var deceleration = mui.os.ios ? 0.000003 : 0.0009;
	//刷新页面
	window.addEventListener('refreshMRSP', function(e){//执行刷新
		location.reload();
	});
	var firstActiveDayIndex = weeker.getDayIndexInWeekData(weeker.curToday);
	//头部组件初始化
	headerApp = new Vue({
		el:'#headerContainer',
		data:function(){
			return {
				roleAction:roleAction,
				weekData:[],//weeker.curWeek
				menus:[
					{
						text:'早餐',
						val:1,
						on:'on'
					},
					{
						text:'中餐',
						val:2,
						on:''
					},
					{
						text:'晚餐',
						val:3,
						on:''
					}
				],
				activeDayIndex:firstActiveDayIndex,
				activeMenuIndex:0,
				selectBaseId:roleAction.doBaseCheck?'':baseId,
				baseName:roleAction.doBaseCheck?'':localStorage.getItem("baseName"),
				showMoreIcon:false
			}
		},
		created:function(){
			var memoryDateData = localStorage.getItem('memoryDateData');
			if(memoryDateData !== null){
				memoryDateData = JSON.parse(memoryDateData);
				weeker.curWeek = memoryDateData.weekData;
				this.weekData = memoryDateData.weekData;
				this.activeDayIndex = memoryDateData.activeDayIndex;
				this.activeMenuIndex = memoryDateData.activeMenuIndex;
				this.selectBaseId = memoryDateData.activeBaseId;
				for(var i=0; i<3; i++){
					if(this.activeMenuIndex === i ){
						this.menus[i].on = 'on';
					}else{
						this.menus[i].on = '';
					};
				};
			}
			this.weekData = weeker.curWeek;
		},
		mounted:function(){
			var self = this;
			
			//初始化基地选择
			if(roleAction.doBaseCheck){
				mui.ajax(ajaxDomain + '/baseInfo/getBaseListByUserId',{
					data:{
						userId:userId
					},
					async:false,
					type:'post',//HTTP请求类型
					success:function(data){
						if(data.result === 'success'){
							//正确逻辑应用
							if(data.data.length === 0){
								self.baseId = '',
								self.baseName = '';
							}else if(data.data.length === 1){
								self.selectBaseId = data.data[0].id;
								self.baseName = data.data[0].name;
							}else{
								var selectBaseIndex = 0;
								self.showMoreIcon = true;
								self.selectBaseId = data.data[0].id;
								self.baseName = data.data[0].name;
								//初始化基地选择
								baseSelector = new mui.PopPicker();
								var selectorVal = data.data;
								for(var i=0; i<selectorVal.length; i++){
									selectorVal[i].value = selectorVal[i].id;
									selectorVal[i].text = selectorVal[i].name;
									if(selectorVal[i].id === self.selectBaseId){
										self.selectBaseId = data.data[i].id;
										self.baseName = data.data[i].name;
									}
								};
								baseSelector.setData(selectorVal);
								baseSelector.pickers[0].setSelectedIndex(selectBaseIndex,1000);
							}
						};
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('无法连接网络');
					}
				})
			}
			
		},
		methods:{
			//基地更换
			toggleBase:function(){
				if(!roleAction.doBaseCheck){
					return;
				}
				var self = this;
				if(baseSelector !== null){
					baseSelector.show(function(selectItems){
						self.selectBaseId = selectItems[0].id;
						headerApp.baseName = selectItems[0].name;
						//更新数据
						foodApp.mergeData();
					});
				}
			},
			//日期点击事件函数
			dayHandle:function(index){
				var self = this;
				this.activeDayIndex = index;
				foodApp.showAddItem = headerApp.roleAction.doAddItem && headerApp.weekData[headerApp.activeDayIndex].isNextSeven;
				//更新数据
				foodApp.mergeData();
			},
			//早中晚按钮事件函数
			menuHandle:function(index){
				for(var i=0; i<3; i++){
					if(index === i ){
						this.menus[i].on = 'on';
						this.activeMenuIndex = i;
					}else{
						this.menus[i].on = '';
					};
				};
				foodApp.showAddItem = headerApp.roleAction.doAddItem && headerApp.weekData[headerApp.activeDayIndex].isNextSeven;
				//更新数据
				foodApp.mergeData();
			},
			//报表跳转
			viewReport:function(){
				mui.openWindow({
					url: "mrspLevel.html",
					id: "mrspLevel",
					show: {
						aniShow: "pop-in"
					},
					extras: {
					"tag": "mrspLevel"
					},
					waiting: {
						autoShow: false
					}
				});
			},
			//返回
			goToOpener:function(){
				mui.back();
			}
		}
	});
	
	//菜品组件初始化
	foodApp = new Vue({
		el:'#foodsContainer',
		data:function(e){
			return {
				roleAction:roleAction,
				foodList:[],
				activeSlideIndex:1,
				showAddItem:false
			} 
		},
		created:function(){
			
		},
		methods:{
			//获取菜品列表并更新列表
			mergeData:function(){
				if(test){
					this.foodList = testData.concat(testData);
					return;
				};
				var self = this;
				mui.ajax(ajaxDomain + '/cook/getCookFood.do',{
					data:{
						type:headerApp.menus[headerApp.activeMenuIndex].val,
						date:headerApp.weekData[headerApp.activeDayIndex].formatDate,
						baseId:headerApp.selectBaseId,
						userId:userId
					},
					type:'post',//HTTP请求类型
					success:function(data){
						if(data.result === 'success'){
							var _data = data.data;
							for(var i=0; i<_data.length; i++){
								_data[i].showCommentBtn = true;
								if(_data[i].isevaluate){
									_data[i].showCommentBtn = false;
								};
								if(weeker.eightDays.indexOf(headerApp.weekData[headerApp.activeDayIndex].formatDate) !== -1 ){
									_data[i].showCommentBtn = false;
								};
								if(_data[i].photoname){
									_data[i].photoStyle = {
										'background-image':'url('+ajaxDomain+_data[i].photoname+')'
									};
								}else{
									_data[i].photoStyle = {
										'background-image':'url(css/images/shipu_ico.png)'
									};
								}
								
							};
							self.foodList = _data;
						};
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('无法连接网络');
					}
				});
			},
			//菜品编辑
			editHandle:function(index,$event){
				$event.stopPropagation();
				var cookerData = {
					name:this.foodList[index].mastername,
					id:this.foodList[index].masterid,
					baseId:this.foodList[index].baseid
					
				};
				localStorage.setItem('cookerData',JSON.stringify(cookerData));
				localStorage.setItem('foodAdd_foodName',this.foodList[index].name);
				localStorage.setItem('foodAdd_baseId',this.foodList[index].baseid);
				//记录当前选中的日期情况
				var memoryDateData = {
					weekData:headerApp.weekData,
					activeDayIndex:headerApp.activeDayIndex,
					activeMenuIndex:headerApp.activeMenuIndex,
					activeSlideIndex:this.activeSlideIndex,
					activeBaseId:headerApp.selectBaseId
				};
				localStorage.setItem('memoryDateData',JSON.stringify(memoryDateData));
				localStorage.setItem('cptjImgData',JSON.stringify(JSON.stringify({src:'',id:''})));
				// 菜品新增
				mui.openWindow({
					url: "mrspEdit.html",
					id: "foodAdd",
					show: {
						aniShow: "pop-in"
					},
					createNew:true,
					extras: {
						'type':'edit',
						'tag': 'mrspReview',
						'addType':headerApp.menus[headerApp.activeMenuIndex].val,
						'addDate':headerApp.weekData[headerApp.activeDayIndex].formatDate,
						'caipinId':this.foodList[index].id,
						'imageUrl':this.foodList[index].photoname,
						'imgId':this.foodList[index].rfid
					},
					waiting: {
						autoShow: false
					}
				});
			},
			//评论
			commentHandle:function(index){
				var self = this;
				//已评论
				if(!this.foodList[index].showCommentBtn){
					mui.toast('您已评论或暂不可评论');
				}else{
					// 跳转评论页
					mui.openWindow({
						url: "mrspReview.html",
						id: "mrspReview",
						createNew:true,
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"tag": "mrspReview",
							"foodData":self.foodList[index]
						},
						waiting: {
							autoShow: false
						}
					});
				};
			},
			//新增菜品
			addFoodItem:function(){
				var cookerData = {
					name:'',
					id:'',
					baseId:''
				};
				localStorage.setItem('cookerData',JSON.stringify(cookerData));
				localStorage.setItem('foodAdd_foodName','');
				localStorage.setItem('foodAdd_baseId','');
				//记录当前选中的日期情况
				var memoryDateData = {
					weekData:headerApp.weekData,
					activeDayIndex:headerApp.activeDayIndex,
					activeMenuIndex:headerApp.activeMenuIndex,
					activeSlideIndex:this.activeSlideIndex,
					activeBaseId:headerApp.selectBaseId
				};
				localStorage.setItem('memoryDateData',JSON.stringify(memoryDateData));
				localStorage.setItem('cptjImgData',JSON.stringify(JSON.stringify({src:'',id:''})));
				// 菜品新增
				mui.openWindow({
					url: "mrspEdit.html",
					id: "foodAdd",
					show: {
						aniShow: "pop-in"
					},
					createNew:true,
					extras: {
						'tag': 'mrspReview',
						'addType':headerApp.menus[headerApp.activeMenuIndex].val,
						'addDate':headerApp.weekData[headerApp.activeDayIndex].formatDate
					},
					waiting: {
						autoShow: false
					}
				});
			}
		},
		mounted:function(){
			var self = this;
			var memoryDateData = localStorage.getItem('memoryDateData');
			if(memoryDateData !== null){
				memoryDateData = JSON.parse(memoryDateData);
				this.activeSlideIndex = memoryDateData.activeSlideIndex;
				this.showAddItem = headerApp.roleAction.doAddItem && headerApp.weekData[headerApp.activeDayIndex].isNextSeven;
			};
			//更新数据
			this.mergeData();
			//slide 左右切换回调
			document.getElementById('slider').addEventListener('slide', function(e) {
				var slideNumber = e.detail.slideNumber;
				if (slideNumber === 0) {
					weeker.getPreWeek();
				    headerApp.weekData = weeker.curWeek;
				    headerApp.activeDayIndex = 0;
				    self.activeSlideIndex = slideNumber;
				    self.mergeData();
				};
				if(slideNumber === 1) {
					//区分两种情况
					if(slideNumber > self.activeSlideIndex){
						weeker.getNextWeek();
					};
					if(slideNumber < self.activeSlideIndex){
						weeker.getPreWeek();
					};
					headerApp.weekData = weeker.curWeek;
				    headerApp.activeDayIndex = 0;
				    self.activeSlideIndex = slideNumber;
				    self.mergeData();
				};
				if(slideNumber === 2) {
					weeker.getNextWeek();
				    headerApp.weekData = weeker.curWeek;
				    headerApp.activeDayIndex = 0;
				    self.activeSlideIndex = slideNumber;
				    self.mergeData();
				}
				foodApp.showAddItem = headerApp.roleAction.doAddItem && headerApp.weekData[headerApp.activeDayIndex].isNextSeven;
			});
		},
		computed:{
			// 是否有有效数据
			hasList:function(){
				var result = '';
				if(this.foodList.length === 0){
					result = 'noData';
				}else{
					
				};
				if(headerApp.roleAction.doAddItem && headerApp.weekData[headerApp.activeDayIndex].isNextSeven){
					result = '';
				};
				return result;
			}
		}
	})
	
//})


