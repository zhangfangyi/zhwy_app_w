mui.plusReady(function() {
	showTop();
	eventClick();

	window.addEventListener('wybxrefresh', function(e) { //执行数据更新
		var menuType = e.detail.type;
		mui('.mui-scroll-wrapper').scroll().scrollTo(0, 0, 100);
		count = 1;
		mui('#test').pullToRefresh().refresh(true);
		$("#topApp .list[menuType='" + menuType + "']").addClass("active").siblings().removeClass("active");
		showList(menuType);
	});

	mui.init();
	var deceleration = mui.os.ios ? 0.003 : 0.0009;
	mui('.mui-scroll-wrapper').scroll({
		deceleration: deceleration,
		indicators: false
	});
	//返回按钮
	mui('.smailNav').on('tap', '.back', function(e) {
		mui.back();
	});

});
var selectObj = {
	startTime: null,
	endTime: null,
	baseId: null,
	status: null
};
var count = 1;
//页面菜单及相应信息
var idstr = JSON.parse(localStorage.idstr);
var ROLENAME = idstr.ROLENAME
//console.log(JSON.stringify(idstr.ROLENAME))
function showTop() {
	mui.ajax(ajaxDomain + "/repair_new/getElementByUserId.do", {
		data: {
			userId: userId,
			baseId: baseId
		},
		dataType: 'json',
		type: 'post', //HTTP请求类型
		success: function(data) {
			localStorage.setItem("dataList", JSON.stringify(data));
			var menus = data.data.menus;
			//						var arr = Object.keys(menus);
			var len = menus.length;
			var muilen = 12 / len;
			var muixs = 'mui-col-xs-' + muilen;
			var muism = 'mui-col-sm-' + muilen;
			for(var i = 0; i < menus.length; i++) {
				var divList = $('<div class="list">' + menus[i].name + '</div>').addClass(muixs, muism).attr({
					'menuType': menus[i].code
				});
				if(menus[i].marker) {
					var menusNum = $('<span class="num">' + menus[i].marker + '</span>');
					if(menus.length > 2) {
						menusNum.css('right', '0.1rem');
					}
					divList.append(menusNum);
				}
				if(menus[i].child) { //是否有下级选项 存储数据
					divList.data('data', menus[i].child)
				}
				if(i == 0) {
					divList.addClass("active");
				}
				$("#topApp").append(divList);
			}
			var menuType = $("#topApp .active").attr("menuType");
			showList(menuType);
		},
		error: function(xhr, type, errorThrown) {
			mui.toast('网络无法连接', {
				duration: 'long',
				type: 'div'
			});
		}
	});
}

(function($, jq) {
	//阻尼系数
	var deceleration = mui.os.ios ? 0.003 : 0.0009;
	$('.mui-scroll-wrapper').scroll({
		bounce: false,
		indicators: true, //是否显示滚动条
		deceleration: deceleration
	});
	$.ready(function() {
		$('#test').pullToRefresh({
			up: {
				callback: function() {
					var self = this;
					setTimeout(function() {
						var menuType = jq("#topApp .active").attr("menuType");
						var list = jq(".mainBodyLi").size();
						count++;
						upload(menuType, list);
					}, 1000);
				}
			}
		});
	});
})(mui, jQuery);
//展示数据
function showList(menuType) {
	var datalist = JSON.parse(localStorage.getItem("dataList"));
	var userType = datalist.data.user;
	mui.ajax(ajaxDomain + "/repair_new/getRepairDataList.do", {
		data: {
			page: 1,
			limit: 7,
			parms: JSON.stringify({
				userType: userType,
				menuType: menuType,
				userId: userId,
				baseId: baseId,
				startTime: selectObj.startTime,
				endTime: selectObj.endTime,
				baseId1: selectObj.baseId,
				status: selectObj.status
			})
		},
		dataType: 'json',
		type: 'post', //HTTP请求类型
		success: function(data) {
			var code = data.code;
			var numberCirle = data.count;
			$("#topApp .active .num").text(numberCirle);
			code == 1 ? $(".morePic").show().attr('code', code) : $(".morePic").hide();
			$(".mainBodyUl").empty();
			var data = data.data;
			for(var i = 0; i < data.length; i++) {
				var str = $('<li class="mainBodyLi">' +
					'<div class="xqclick" id=' + data[i].id + '>' +
					'<div class="mui-row content">' +
					'<div class="title mui-col-xs-8 mui-col-sm-8 mui-ellipsis">【' + data[i].basename + '】' + data[i].describe + '</div>' +
					'<div class="assess mui-col-xs-4 mui-col-sm-4"><span style="' + setColor(data[i].status) + '">' + datalist.data.repairType[data[i].status] + '</span></div>' +
					'</div>' +
					'<div class="mui-row contbot">' +
					'<div class="time mui-col-xs-7 mui-col-sm-7">' + data[i].createtime + '</div>' +
					'<div class="name mui-col-xs-5 mui-col-sm-5">' +
					'<span>' + data[i].username + '</span>' +
					'<img src=' + ajaxDomain + data[i].photoname + ' alt="" />' +
					'</div>' +
					'</div>' +
					'</div>' +
					'</li>');
				//维修按钮
				if(Object.keys(data[i].btns).length != 0) {
					var menus = data[i].btns;
					var arr = Object.keys(menus);
					var len = arr.length;
					var muilen = 12 / len;
					var muixs = 'mui-col-xs-' + muilen;
					var muism = 'mui-col-sm-' + muilen;

					var btn = $('<div class="mui-row worker"/>');
					$.each(data[i].btns, function(k, val) {
						var listbtn = $('<div class="maintain">').text(val).attr('btntype', k).addClass(muixs, muism);
						btn.append(listbtn);
					});
					str.append(btn);
				}
				$(".mainBodyUl").append(str);
				if(menuType == 1) {
					$('.name').hide();
				}
			}
		},
		error: function(xhr, type, errorThrown) {
			mui.toast('网络无法连接', {
				duration: 'long',
				type: 'div'
			});
		}
	});
}

function setColor(status) {
	var datalist = JSON.parse(localStorage.getItem("dataList"));
	var color = "color:" + datalist.data.repairTypeColor.color[status] +
		";background-color:" + datalist.data.repairTypeColor.backgroundColor[status];
	return color;
}

//头部点击切换
function eventClick() {
	//头部切换
	mui('#topApp').on('tap', '.list', function(e) {
		if(!$(this).hasClass("active")) {
			$(this).addClass('active').siblings().removeClass("active");
			var menuType = $(this).attr('menuType');
			mui('.mui-scroll-wrapper').scroll().scrollTo(0, 0, 100);
			count = 1;
			mui('#test').pullToRefresh().refresh(true);
			showList(menuType);
			var listData = $(this).data('data');
			if(listData) {
				if(selectObj.date || selectObj.baseId || selectObj.status) {
					$('.ztselect').show();
					$('.cntBigCop').css('margin-top', '3rem')
				} else {
					$('.ztselect').show().html('');
					$('.cntBigCop').css('margin-top', '3rem')
					var len = listData.length;
					var muilen = 12 / len;
					var muixs = 'mui-col-xs-' + muilen;
					var muism = 'mui-col-sm-' + muilen;
					for(var i = 0; i < listData.length; i++) {
						var str = $('<div class="mui-ellipsis">' + listData[i].name + '&nbsp&nbsp▾' + '</div>').addClass(muixs, muism);
						str.data('data', listData[i].value);
						str.attr('code', listData[i].code);
						str.attr('data-options', '{"type":"date"}');
						$("#ztselect").append(str);
					}
				}
			} else {
				$('.ztselect').hide();
				$('.cntBigCop').css('margin-top', '2.2rem')
			}
		}
	});
	mui('.ztselect').on('tap', 'div', function(e) {
		var code = $(this).attr('code');
		var _this = $(this);
		if(code == '10') { //开始时间
			//结束时间
			var dtPicker = new mui.DtPicker({
				"type": "date"
			});
			dtPicker.show(function(selectItems) {
				var y = selectItems.y.value;
				var m = selectItems.m.value;
				var d = selectItems.d.value;
				var date = y + '-' + m + '-' + d;

				_this.html(date.substring(2, 10) + '&nbsp&nbsp▾');
				selectObj.startTime = date;
				var menuType = $("#topApp .active").attr("menuType");
				mui('.mui-scroll-wrapper').scroll().scrollTo(0, 0, 100);
				mui('#test').pullToRefresh().refresh(true);
				count = 1;
				showList(menuType);
			});
			var resBtn = $('<button data-id="btn-res" class="mui-btn mui-btn-gray">重置</button>');
			$('.mui-dtpicker-header').prepend(resBtn);
			resBtn.on('tap', function() {
				dtPicker.dispose();
				selectObj.startTime = null;
				_this.html('开始时间' + '&nbsp&nbsp▾');
				var menuType = $("#topApp .active").attr("menuType");
				mui('.mui-scroll-wrapper').scroll().scrollTo(0, 0, 100);
				mui('#test').pullToRefresh().refresh(true);
				count = 1;
				showList(menuType);
			})

		} else if(code == '11') { //结束时间
			var dtPicker = new mui.DtPicker({
				"type": "date"
			});
			dtPicker.show(function(selectItems) {
				var y = selectItems.y.value;
				var m = selectItems.m.value;
				var d = selectItems.d.value;
				var date = y + '-' + m + '-' + d;
				_this.html(date.substring(2, 10) + '&nbsp&nbsp▾');
				selectObj.endTime = date;
				var menuType = $("#topApp .active").attr("menuType");
				mui('.mui-scroll-wrapper').scroll().scrollTo(0, 0, 100);
				mui('#test').pullToRefresh().refresh(true);
				count = 1;
				showList(menuType);
			});
			var resBtn = $('<button data-id="btn-res" class="mui-btn mui-btn-gray">重置</button>');
			$('.mui-dtpicker-header').prepend(resBtn);
			resBtn.on('tap', function() {
				dtPicker.dispose();
				selectObj.endTime = null;
				_this.html('结束时间' + '&nbsp&nbsp▾');
				var menuType = $("#topApp .active").attr("menuType");
				mui('.mui-scroll-wrapper').scroll().scrollTo(0, 0, 100);
				mui('#test').pullToRefresh().refresh(true);
				count = 1;
				showList(menuType);
			})
		} else if(code == '12') { //基地
			var data = JSON.parse(localStorage.getItem("dataList"));
			var user = data.data.user;
			var data = _this.data('data');
			var setData = [];
			if(user == "2") {
				var obj = {};
				obj["text"] = "工程部";
				obj["baseId"] = "5433f0cb889e444b84651d37008d3674";
				var cops = $.extend(true, "工程部", obj);
				setData.unshift(cops);
			}
			for(var i in data) {
				var obj = {};
				obj["text"] = data[i];
				obj["baseId"] = i;
				var cop = $.extend(true, data[i], obj);
				setData.push(cop);
			}
			if($(".dropdwon").length == 0) {
				var multiple = new Dropdwon({
					type: 'multiple', //是单选还是多选 单选 single 多选 multiple
					title: '基地', //选择框title
					required: false, //是否必填
					dataArr: setData,
					succe: function(res) { // 回调函数
						selectObj.baseId = res.join(',');
						var menuType = $("#topApp .active").attr("menuType");
						showList(menuType);
					}
				})
			}else{
				$(".dropdwon").show()
			}

		} else if(code == '13'){ //状态
			var data = _this.data('data');
			var setData = [];
			for(var i in data) {
				var obj = {};
				obj["text"] = data[i];
				obj["code"] = i;
				var cop = $.extend(true, data[i], obj);
				setData.push(cop);
			}
			if($(".drop").length == 0) {
				var mult = new Drop({
					type: 'multiple', //是单选还是多选 单选 single 多选 multiple
					title: '状态', //选择框title
					required: false, //是否必填
					dataArr: setData,
					success: function(resp) { // 回调函数
						selectObj.status = resp.join(',');
						var menuType = $("#topApp .active").attr("menuType");
						showList(menuType);
					}
				})
			}else{
				$(".drop").show()
			}

		}
	});

	//退回维修按钮
	mui('.mainBodyUl').on('tap', '.maintain', function(e) {
		var btntype = $(this).attr('btntype');
		var id = $(this).parent().prev().attr('id');
		var menuType = $("#topApp .active").attr("menuType");
		if(btntype == '3') { //退回
			mui.openWindow({
				url: "newTh.html",
				styles: {
					popGesture: "close"
				},
				show: {
					aniShow: "pop-in"
				},
				extras: {
					"thid": id,
					"thmenuType": menuType,
					"thbtntype": btntype
				},
				waiting: {
					autoShow: false
				}
			});
		} else { //4 维修
			mui.openWindow({
				url: "newWxwb.html",
				styles: {
					popGesture: "close"
				},
				show: {
					aniShow: "pop-in"
				},
				extras: {
					"wxid": id,
					"wxmenuType": menuType,
					"wxbtntype": btntype
				},
				waiting: {
					autoShow: false
				}
			});
		}
	});

	//新增
	mui('.moreBox').off().on('tap', '.morePic', function(e) {
		var btnCode = $(this).attr('code');
		var menuType = $("#topApp .active").attr("menuType");
		mui.openWindow({
			url: "newXjbx.html",
			styles: {
				popGesture: "close"
			},
			show: {
				aniShow: "pop-in"
			},
			extras: {
				"btnCode": btnCode,
				"xjMenuType": menuType
			},
			waiting: {
				autoShow: false
			}
		});
	});

	//列表点击事件
	mui('.mainBodyUl').on('tap', '.xqclick', function(e) {
		var id = $(this).attr('id');
		var menuType = $("#topApp .active").attr("menuType");
		mui.openWindow({
			url: "newBxxq.html",
			styles: {
				popGesture: "close"
			},
			show: {
				aniShow: "pop-in"
			},
			extras: {
				"xqId": id,
				"xqMenuType": menuType
			},
			waiting: {
				autoShow: false
			}
		});
	});

}

//jiazai
function upload(menuType, list) {
	var datalist = JSON.parse(localStorage.getItem("dataList"));
	var userType = datalist.data.user;
	mui.ajax(ajaxDomain + "/repair_new/getRepairDataList.do", {
		data: {
			page: count,
			limit: 7,
			parms: JSON.stringify({
				userType: userType,
				menuType: menuType,
				userId: userId,
				baseId: baseId,
				startTime: selectObj.startTime,
				endTime: selectObj.endTime,
				baseId1: selectObj.baseId,
				status: selectObj.status
			})
		},
		dataType: 'json',
		type: 'post', //HTTP请求类型
		success: function(data) {
			var code = data.code;
			code == 1 ? $(".morePic").show().attr('code', code) : $(".morePic").hide();
			var number = data.count;
			if(list >= number) {
				mui('#test').pullToRefresh().endPullUpToRefresh(true);
			} else {
				mui('#test').pullToRefresh().endPullUpToRefresh(false);
			}

			var data = data.data;
			for(var i = 0; i < data.length; i++) {
				var str = $('<li class="mainBodyLi">' +
					'<div class="xqclick" id=' + data[i].id + '>' +
					'<div class="mui-row content">' +
					'<div class="titlele mui-col-xs-8 mui-col-sm-8 mui-ellipsis">【' + data[i].basename + '】' + data[i].describe + '</div>' +
					'<div class="assess mui-col-xs-4 mui-col-sm-4"><span style="' + setColor(data[i].status) + '">' + datalist.data.repairType[data[i].status] + '</span></div>' +
					'</div>' +
					'<div class="mui-row contbot">' +
					'<div class="time mui-col-xs-7 mui-col-sm-7">' + data[i].createtime + '</div>' +
					'<div class="name mui-col-xs-5 mui-col-sm-5">' +
					'<span>' + data[i].username + '</span>' +
					'<img src=' + ajaxDomain + data[i].photoname + ' alt="" />' +
					'</div>' +
					'</div>' +
					'</div>' +
					'</li>');
				//维修按钮
				if(Object.keys(data[i].btns).length != 0) {
					var btn = $('<div class="mui-row worker">' +
						'<div class="maintain mui-col-xs-6 mui-col-sm-6">' +
						data[i].username +
						'</div>' +
						'<div class="sendback mui-col-xs-6 mui-col-sm-6">' +
						data[i].username +
						'</div>' +
						'</div>');
					str.append(btn);
				}
				$(".mainBodyUl").append(str);
			}
		},
		error: function(xhr, type, errorThrown) {
			mui.toast('网络无法连接', {
				duration: 'long',
				type: 'div'
			});
		}
	});

}