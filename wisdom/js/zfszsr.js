 mui.plusReady(function() {
 	var cardNumber = localStorage.getItem("cardnumber");
 	$(".dzadd").on('tap',function(){ 
 		var newPwd = $("#newPwd").val();
 		var oldPwd = $("#oldPwd").val();
 		mui.ajax(ajaxDomain + "/app/getRSAPublicKey.do", {
			data: {},
			//					dataType:'json',//服务器返回json格式数据
			type: 'post', //HTTP请求类型
			timeout: 5000, //超时时间设置为10秒；
			clossDomin: true,
			headers: {
				'Content-Type': 'application/json'
			},
			success: function(data) {
				newPwd = segmentEncrypt(newPwd, data);
				oldPwd = segmentEncrypt(oldPwd, data);
				updatePwd(newPwd,oldPwd,cardNumber)
			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
				mui.toast('网络无法连接');
			}
		});
 		
 	
 	})
 	
 });
 
 function updatePwd(newPwd,oldPwd,cardNumber){
 		mui.ajax(ajaxDomain + "/user/modifyPayPass.do",{ 
						data:{
							cardnumber: cardNumber,
							payPass:newPwd,
							oldPayPass:oldPwd
						},
	   					dataType:'json',
						type:'post',//HTTP请求类型
						success:function(data){  
							if(data.result == "success"){
								mui.toast('修改成功'); 
								mui.back();
							}else{
								mui.toast('修改失败');
							}
						},
						error:function(xhr,type,errorThrown){
							mui.toast('网络无法连接');
						}
					});
 }

//对数据分段加密
function segmentEncrypt(enStr, publikKey) {
	window.rsa = new JSEncrypt();
	window.rsa.setPublicKey(publikKey);
	var result = [],
		offSet = 0,
		cache = '',
		i = 1,
		inputLen = enStr.length;

	while(inputLen - offSet > 0) {
		if(inputLen - offSet > 117) {
			cache = window.rsa.encrypt(enStr.substring(offSet, 117 * i));
		} else {
			cache = window.rsa.encrypt(enStr.substring(offSet));
		}
		// 加密失败返回原数据
		if(cache === false) {
			return enStr;
		}
		offSet = i++ * 117;

		result.push(cache);
	}
	return JSON.stringify(result);
}