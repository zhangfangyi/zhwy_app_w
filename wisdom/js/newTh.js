mui.plusReady(function() {
	var id = plus.webview.currentWebview().thid; 
	var menuType = plus.webview.currentWebview().thmenuType;
	var btntype = plus.webview.currentWebview().thbtntype;
	
	thbtn(id,menuType,btntype);//退回保存按钮
	
	addImg();//图片上传
	
 	 mui.init();
	 var deceleration = mui.os.ios ? 0.003 : 0.0009;
	 mui('.mui-scroll-wrapper').scroll({
			deceleration: deceleration,
			indicators: false
	});	
			//返回按钮
	mui('.smailNav').on('tap', '.back', function(e) {
			mui.back();
	});
})

//添加图片
var isTjiao = true;	
var imagesList = [];
function addImg(){
	mui(".imgBox").on("tap", "#itemImg", function(e) {
		var imgLength = $(".imgBox img").size();
		if(imgLength > 2){
				mui.toast('最多添加2张图片', {
						duration: 'long', 
						type: 'div'
					});
			return false;
		}
			if(mui.os.plus){
				var a = [{
					title: "拍照"
				}, {
					title: "从手机相册选择"
				}];
				plus.nativeUI.actionSheet({
					title: "添加图片",
					cancel: "取消",
					buttons: a
				}, function(b) {
					switch (b.index) {
						case 0:
							break;
						case 1:
							getImage();
							break;
						case 2:
							galleryImg();
							break;
						default:
							break
					}
				})	
			}
			
		});
}


 function getImage() {
					var c = plus.camera.getCamera();
					c.captureImage(function(e) {  
						plus.io.resolveLocalFileSystemURL(e, function(entry) {
							var s = entry.toLocalURL() + "?version=" + new Date().getTime();
							var imgCop = $("<div></div>").addClass("imgCop").css({
								"position":"relative",
								/*"background":"red",*/
							});
							var colseB = $("<div style='' class='closeBtn'></div>");
							imgCop.html(colseB);
							var img = $('<img/>').attr({"src":s});
							img.css({"margin-left":"0"});
							imgCop.append(img);
							var bgDiv = $("<div/>").css({
								"opacity":0.5,
								"width":"100%",
								"height":"100%",
								"position":"absolute",
								"left":"0",
								"top":"0.2rem",
								"background":"#555",
								"z-index":2,
								"line-height":"100%",
								"text-align":"center",
								"color":"#fff",
								"line-height":"1.5rem"
							}).html("正在上传").addClass("bgDiv");
							imgCop.append(bgDiv);
							isTjiao = false;
							$(".imgBox").prepend(imgCop);
							uploadHead(s,img);
							img.hover(
			         			function(e){
			         				e.stopPropagation();
			         				$(this).siblings(".closeBtn").show();
			         			},function(e){
			         				e.stopPropagation();
			         				$(this).siblings(".closeBtn").hide();
			         			}
			         		);
			         		colseB.on('tap',function(e){
			         			e.stopPropagation();
			         			var result= confirm("是否删除此图片？");
			         			if(result){
			         				var imgId = $(this).siblings("img").attr("imgId");
			         				deleteById(imgId);
				         			for(var i=0;i<imagesList.length;i++){
				         				if(imgId==imagesList[i]){
				         					imagesList = imagesList.splice(i,1);
				         				}
				         			}
				         			$(this).parents(".imgCop").remove();
			         			}
			         		})
						}, function(e) {
							mui.toast("读取拍照文件错误");
						});
					}, function(s) {
						mui.toast("读取拍照文件错误");
					}, {
						filename: "_doc/img" + new Date().getTime()
					});
					//
				}


	function galleryImg() {
			plus.gallery.pick(function(a) {
				plus.io.resolveLocalFileSystemURL(a, function(entry) { 
							var s = entry.toLocalURL() + "?version=" + new Date().getTime();
							var imgCop = $("<div></div>").addClass("imgCop").css({
								"position":"relative",
								/*"background":"red",*/
							});
							var colseB = $("<div style='' class='closeBtn'></div>");
							imgCop.html(colseB);
							var img = $('<img/>').attr({"src":s});
							img.css({"margin-left":"0"});
							imgCop.append(img);
							var bgDiv = $("<div/>").css({
								"opacity":0.5,
								"width":"100%",
								"height":"100%",
								"position":"absolute",
								"left":"0",
								"top":"0.2rem",
								"background":"#555",
								"z-index":2,
								"line-height":"100%",
								"text-align":"center",
								"color":"#fff",
								"line-height":"1.5rem"
							}).html("正在上传").addClass("bgDiv");
							imgCop.append(bgDiv);
							isTjiao = false;
							$(".imgBox").prepend(imgCop);
							uploadHead(s,img);
							img.hover(
			         			function(e){
			         				e.stopPropagation();
			         				$(this).siblings(".closeBtn").show();
			         			},function(e){
			         				e.stopPropagation();
			         				$(this).siblings(".closeBtn").hide();
			         			}
			         		);
			         		colseB.on('tap',function(e){
			         			e.stopPropagation();
			         			var result= confirm("是否删除此图片？");
			         			if(result){
			         				var imgId = $(this).siblings("img").attr("imgId");
			         				deleteById(imgId);
				         			for(var i=0;i<imagesList.length;i++){
				         				if(imgId==imagesList[i]){
				         					imagesList = imagesList.splice(i,1);
				         				}
				         			}
				         			$(this).parents(".imgCop").remove();
			         			}
			         		})
						}, function(e) {
					console.log("读取拍照文件错误：" + e.message);
				});
			}, function(a) {}, {
				filter: "image"
			})
		};
		


 function uploadHead(imgPath,img) {
					var task = plus.uploader.createUpload(ajaxDomain + "/resource/upload.do", {
							method: "POST"
						},
						function(t, status) { //上传完成
							if(status == 200) {
								var fileBean = $.parseJSON(t.responseText);
								if(fileBean.result == "success") {
									mui.toast("上传成功！");
									isTjiao = true;
									var data = fileBean.data;
									img.attr("imgId",data.id);
									imagesList.push(data.id);
									img.siblings(".bgDiv").remove();
								} else {
									mui.toast(fileBean.resume);
								}
							} else {
								mui.toast("上传失败！");
							}
						}
					);
					compressImage(imgPath, new Date().getTime() + ".jpg", "img", function(namePath) {
						task.addFile(namePath, {
							key: "file"
						});
						task.start();
					});
				}	 
				

 	//压缩图片
function compressImage(url, filename, divid, backFn) {
					var name = "_doc/" + divid + "-" + filename; //_doc/upload/F_ZDDZZ-1467602809090.jpg
					plus.zip.compressImage({
							src: url, //src: (String 类型 )压缩转换原始图片的路径
							dst: name, //压缩转换目标图片的路径
							quality: 90, //quality: (Number 类型 )压缩图片的质量.取值范围为1-100
							overwrite: true //overwrite: (Boolean 类型 )覆盖生成新文件
						},
						function(event) {
							//uploadf(event.target,divid);
							var path = name; //压缩转换目标图片的路径
							//event.target获取压缩转换后的图片url路
							//filename图片名称
							backFn(path);
						},
						function(error) {
							plus.nativeUI.toast("压缩图片失败，请稍候再试");
						});
				}

var count = 0;
function thbtn(id,menuType,btntype){//退回保存
	mui('.wybxBotbtn').on('tap', '.qxbtn', function(e) { 
		if(count == 0){
			count++
		if(isTjiao == false){
					mui.toast('图片正在上传', {
						duration: 'long', 
						type: 'div'
					});
					count = 0
					return false;
		}
		var tcp_content = $('.tcp_content').val();
		if(tcp_content ==""){
			mui.toast('请输入退回原因');
			count = 0
			return false;
		}
		var datalist = JSON.parse(localStorage.getItem("dataList"));
		var userType = datalist.data.user;
		mui.ajax(ajaxDomain + "/repair_new/clickButton.do",{
						data:{
							 userType  : userType,
							 menuType : menuType,
							 btnType :btntype,
							 parms:JSON.stringify({
					           userId: userId,
				 	           baseId:baseId,
				 	           content:tcp_content,
				 	           userName: userName,
				 	           repairId :id,
				 	           images:imagesList
		   					})
						},
	   					dataType:'json',
						type:'post',//HTTP请求类型
						success:function(data){ 
							if(data.result == "success"){ 
								mui.toast("退回成功");
   								plusCommon.popToTarget("wybx",false,"wybxrefresh",{"type":menuType});//退回到物业报修页面
							}
						}
				})
		}
	});
}