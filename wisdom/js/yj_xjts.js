
var isTjiao = true;	
mui.plusReady(function () {
	var userId = localStorage.getItem("userId");
	var userName = localStorage.getItem("userName");
//	var baseId = localStorage.getItem("baseId");
	var baseId;
	var ldBaseId = plus.webview.currentWebview().yjtsBaseId;
	if(ldBaseId){
		baseId = ldBaseId;
	}else{
		baseId = localStorage.getItem("baseId");
	}
	
	var code = plus.webview.currentWebview().yjtsCode;
	//进入页面设置默认选中
	if(code == "1"){//意见
		$("#view").show();
		$("#complain").hide();
		$("#topApp li").eq(0).addClass("active").siblings().removeClass("active");
	}else{//投诉
		$("#view").hide();
		$("#complain").show();
		$("#topApp li").eq(1).addClass("active").siblings().removeClass("active");
	}
	
	
	//判断是否数据类型  //true用户层    领导层
$(".topCommonLi").on('tap',function(){
			var index = $(this).index();
			if(!$(this).hasClass("active")) {
				$(this).addClass('active');
				$(this).siblings().removeClass('active');
				$(".cntBigCop").eq(index).show();
				$(".cntBigCop").eq(index).siblings(".cntBigCop").hide();
			}
			
});
//点击中间tab
$(".tabLi").on('tap',function(){
	var index = $(this).index();
	if(!$(this).hasClass("active")) {
		$(this).addClass('active');
		$(this).siblings().removeClass('active');
		$(this).parents(".cntBigCop").find(".textareaCop li").eq(index).addClass('show').removeClass('hidden');
		$(this).parents(".cntBigCop").find(".textareaCop li").eq(index).siblings().removeClass('show').addClass('hidden');
	}
});

var imagesList = [];

$(".submit").on('tap',function(){
	if(isTjiao == false){
					mui.toast('图片正在上传', {
						duration: 'long', 
						type: 'div'
					});
					return;
				}
    var type = $(this).attr("type");
    if(type == "complain"){//投诉
    		var smailType = $("#complain .tabUl").find(".active").attr("type");
    		var content = $("#complain .textareaCop .show").find("textarea").val();
    		if(content == ''){
    			mui.toast('请输入投诉建议', {
					duration: 'long', 
					type: 'div'
				});
				return;
    		}
    		var data = {
							userId:userId,
							userName:userName,
							content:content,
							baseId:baseId,
							type:"2",
							smailType:smailType,
							images:imagesList
					};
      		mui.ajax(ajaxDomain + "/complain/addComplain.do",{
					data:{
						data : JSON.stringify(data)
					},
   					dataType:'json', 
					type:'post',//HTTP请求类型
					async:false,
					success:function(data){ 
						mui.toast('提交成功', {
							duration: 'long', 
							type: 'div'
						});
						setTimeout(function(){ 
							mui.back();
						},400)
					},
					error:function(xhr,type,errorThrown){
						mui.toast("网络异常");
					} 
			});
    }else{//意见
    	var smailType = $("#view .tabUl").find(".active").attr("type");
    	var content = $("#view .textareaCop .show").find("textarea").val();
    	if(content == ''){
    			mui.toast('请输入意见', {
					duration: 'long', 
					type: 'div'
				});
				return;
    		}
    	var data = {
							userId:userId,
							userName:userName,
							content:content,
							baseId:baseId,
							type:"1",
							smailType:smailType,
					};
		   		mui.ajax(ajaxDomain + "/complain/addComplain.do",{
					data:{
						data : JSON.stringify(data)
					},
   					dataType:'json', 
					type:'post',//HTTP请求类型
					async:false,
					success:function(data){ 
						mui.toast('提交成功', {
							duration: 'long', 
							type: 'div'
						});
						setTimeout(function(){
							mui.back();
						},400)
					},
					error:function(xhr,type,errorThrown){
						mui.toast("网络异常");
					} 
			});			
    	
    	
    }
})

$("#itemImg").on('tap',function(){
	var imgLength = $(".imgBox img").size();
	if(imgLength > 3){
			mui.toast('最多添加3张图片', {
					duration: 'long', 
					type: 'div'
				});
	}else{
		getImage();
	}
})


 function getImage() {
					var c = plus.camera.getCamera();
					c.captureImage(function(e) {  
						plus.io.resolveLocalFileSystemURL(e, function(entry) {
							var s = entry.toLocalURL() + "?version=" + new Date().getTime();
							
							var imgCop = $("<div></div>").addClass("imgCop").css({
								"position":"relative",
								"background":"red"
							});
							imgCop.html("<div style='' class='closeBtn'></div>");
							var img = $('<img/>').attr({"src":s
						});
							img.css("margin-left","0");
							imgCop.append(img);
							var bgDiv = $("<div/>").css({
								"opacity":0.5,
								"width":"100%",
								"height":"100%",
								"position":"absolute",
								"left":"0",
								"top":"0px",
								"background":"#555",
								"z-index":2,
								"line-height":"100%",
								"text-align":"center",
								"color":"#fff",
								"line-height":"1.5rem"
							}).html("正在上传").addClass("bgDiv");
							imgCop.append(bgDiv);
							isTjiao = false;
							$(".imgBox").prepend(imgCop);
							uploadHead(s,img);
							img.hover(
			         			function(e){
			         				e.stopPropagation();
			         				$(this).siblings(".closeBtn").show();
			         			},function(e){
			         				e.stopPropagation();
			         				$(this).siblings(".closeBtn").hide();
			         			}
			         		);
			         		$(document).on("tap",".closeBtn",function(e){
			         			e.stopPropagation();
			         			var result= confirm("是否删除此图片？");
			         			if(result){
			         				var imgId = $(this).siblings("img").attr("imgId");
			         				deleteById(imgId);
				         			for(var i=0;i<imagesList.length;i++){
				         				if(imgId==imagesList[i]){
				         					imagesList = imagesList.splice(i,1);
				         				}
				         			}
				         			$(this).parents(".imgCop").remove();
			         			}
			         		})
						}, function(e) {
							mui.toast("读取拍照文件错误");
						});
					}, function(s) {
						mui.toast("读取拍照文件错误");
					}, {
						filename: "_doc/img" + new Date().getTime()
					});
					//
				}

 
 function uploadHead(imgPath,img) {
					var task = plus.uploader.createUpload(ajaxDomain + "/resource/upload.do", {
							method: "POST"
						},
						function(t, status) { //上传完成
							if(status == 200) {
								var fileBean = $.parseJSON(t.responseText);
								if(fileBean.result == "success") {
									mui.toast("上传成功！");
									isTjiao = true;
									var data = fileBean.data;
									img.attr("imgId",data.id);
									imagesList.push(data.id);
									img.siblings(".bgDiv").remove();
								} else {
									mui.toast(fileBean.resume);
								}
							} else {
								mui.toast("上传失败！");
							}
						}
					);
					compressImage(imgPath, new Date().getTime() + ".jpg", "img", function(namePath) {
						task.addFile(namePath, {
							key: "file"
						});
						task.start();
					});
				}
 
 
 	//压缩图片
function compressImage(url, filename, divid, backFn) {
					var name = "_doc/" + divid + "-" + filename; //_doc/upload/F_ZDDZZ-1467602809090.jpg
					plus.zip.compressImage({
							src: url, //src: (String 类型 )压缩转换原始图片的路径
							dst: name, //压缩转换目标图片的路径
							quality: 90, //quality: (Number 类型 )压缩图片的质量.取值范围为1-100
							overwrite: true //overwrite: (Boolean 类型 )覆盖生成新文件
						},
						function(event) {
							//uploadf(event.target,divid);
							var path = name; //压缩转换目标图片的路径
							//event.target获取压缩转换后的图片url路
							//filename图片名称
							backFn(path);
						},
						function(error) {
							plus.nativeUI.toast("压缩图片失败，请稍候再试");
						});
				}
 
 
 
//返回按钮
mui('.smailNav').on('tap', '.back', function(e) {
	mui.back();
});

mui.init({
			swipeBack: true ,//启用右滑关闭功能
			beforeback: function() {
				var returnCode = $("#topApp .active").attr("level");
				var returnBaseId = baseId;
				
		        var list = plus.webview.currentWebview().opener();
		        mui.fire(list, 'refresh1',{
		        	val:returnCode,
		        	rbase: returnBaseId
		        });
		        return true;
			   }
		});
var deceleration = mui.os.ios ? 0.003 : 0.0009;
mui('.mui-scroll-wrapper').scroll({
	deceleration: deceleration,
	indicators: false
});	

})


//删除上传图片
function deleteById(id){
	mui.ajax(ajaxDomain + "/resource/deleteById.do",{
								data:{
									id : id
								},
			 					dataType:'json',
								type:'post',//HTTP请求类型
								success:function(data){ 
								
								},
								error:function(xhr,type,errorThrown){
									//异常处理；
								}
							});
}