mui.init({
	swipeBack: false, //启用右滑关闭功能
	keyEventBind: {
		backbutton: false  //关闭back按键监听
	}
});
 document.addEventListener("plusready", function() {
    plus.webview.currentWebview().opener().close("none");
});
var merchantId = localStorage.getItem('merchantId');
var payResultApp = new Vue({
	el:'#payResult',
	data:function(){
		return {
			orderPayer:'支付总价获取中...',
			orderList:'订单编号获取中...'
		};
	},
	created:function(){
		merchantId
		this.orderPayer = localStorage.getItem('orderPayer');
		this.orderList = localStorage.getItem('orderList');
	}
});

mui('body').on('tap', '.result-btn', function(e) {
   	mui.openWindow({
		url:'wddd.html',
		extras: {
			code:merchantId
		},
	});
});
/*mui('body').on('tap', '.btn-back', function(e) {
   	mui.openWindow({
		url:'shitang-index.html'
	});
});*/