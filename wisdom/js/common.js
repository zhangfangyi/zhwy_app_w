(function(doc, win) {
	var docEl = doc.documentElement,
		resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize',
		recalc = function() {
			var clientWidth = docEl.clientWidth;
			if(!clientWidth) return;
			if(clientWidth >= 750) {
				docEl.style.fontSize = '100px';
			} else {
				docEl.style.fontSize = 100 * (clientWidth / 750) + 'px';
			}
		};
	if(!doc.addEventListener) return;
	win.addEventListener(resizeEvt, recalc, false);
	doc.addEventListener('DOMContentLoaded', recalc, false);
})(document, window);

//var APP_UPDATE_URL = "http://zhwyapp.njhzinfo.com";
var APP_UPDATE_URL = "http://aipro.njhzinfo.com:50001/";

var timer;
   	var ajaxDomain =  "http://aipro.njhzinfo.com:8021/wpms";
	var wsDomain = "ws://121.42.192.146:8021/wpms";//服务器
//var ajaxDomain =  "http://aipro.njhzinfo.com:7005/wpms";
//var wsDomain = "ws://121.42.192.146:7005/wpms";///服务器7005
//	var ajaxDomain =  "http://192.168.0.109:8021/wpms";//我的
// var ajaxDomain = "http://192.168.0.182:8021/wpms";
 

var viewLogistics = "http://m.kuaidi100.com/result.jsp?nu=";



function objShow(obj) {
	timer = setInterval(function() {
		obj.scrollIntoView(false);
	}, 100);

}

function objHide(obj) {
	clearInterval(timer);
}
//获取角色
function getRole(roleArray){
	var newRole = [];
	var role = {
		"管理员":"4fee39f266644687bcead41ad780c001",
		"基地经理":"592a0f745fc8422794692ef35f71f229",
		"基地领导": "583535d9db8e487da5a44a36b5c897e3",
		"员工":"f5cfc2df44f64079b15fdb86ccf35eff",
		"厨师长":"a573bb95c6414f05aeed4f03327ed003",
		"厨师":"48926d7224ef46b4822eb5c82ce4ac90",
		"维修工人":"d42baf8c3be74839993a43d2596aa9d7",
		'外用工':'42238b2f93b6468c9f3f48c4f50a89a4',
		"物业领导":"ac7c746a15ad453e942df2c9a4d667f9",
		"餐饮领导":"47d851aa9fbd438c856dbb56715330ff",
		"意见投诉领导":"8f41bd80026548229725bb6766746211"
	};
	$.each(role,function(k,val){
		for(var i = 0; i<roleArray.length;i++){
			if(role[k] == roleArray[i]){
				newRole.push(k);
			}
		}
	})
	return newRole;
}
 
/****获取周数据，定义获取上一周下一周接口，周数据中提供是否是未来七天的字段提示****************************************/
var WeekData = function(){
	this.oneDayTime =  24*60*60*1000;
	this.today = new Date();
	this.nowTime = this.today.getTime();
	this.curToday = this.formatDate(this.nowTime);
	this.weekDay = this.today.getDay();
	this.featureSevenDaysTime();
	this.curWeekMondayTime = this.nowTime - ((this.weekDay===0?7:this.weekDay)-1)*this.oneDayTime;
	this.curWeek = this.getWeekData(this.curWeekMondayTime);
}

WeekData.prototype.featureSevenDaysTime = function(){
	this.featureSevenDaysTime = [];
	for(var i=1; i<8; i++){
		var theTime = this.nowTime + i*this.oneDayTime;
		var theDate = this.formatDate(theTime);
		this.featureSevenDaysTime.push(theDate);
	};
}

WeekData.prototype.getWeekData = function(mondayTime){
	var result =[];
	for(var i=0; i<7; i++){
		var curTime = mondayTime + i*this.oneDayTime;
		var curDate = this.formatDate(curTime);
		var dayVal = curDate.split('-')[2];
		var isNextSeven = this.featureSevenDaysTime.indexOf(curDate) === -1?false:true;
		var obj = {
			time:curTime,
			formatDate:curDate,
			dayVal:dayVal,
			isNextSeven:isNextSeven,
			weekText:(function(index){
				var result = '';
				switch(index){
					case 0:
						result = '一';
						break;
					case 1:
						result = '二';
						break;
					case 2:
						result = '三';
						break;
					case 3:
						result = '四';
						break;
					case 4:
						result = '五';
						break;
					case 5:
						result = '六';
						break;
					case 6:
						result = '日';
						break;
				};
				return result;
			})(i)
		};
		result.push(obj);
	};
	return result;
}

WeekData.prototype.getPreWeek = function(){
	var startTime = this.curWeek[0].time - 7*this.oneDayTime;
	this.curWeek = this.getWeekData(startTime);
}

WeekData.prototype.getNextWeek = function(){
	var startTime = this.curWeek[6].time + this.oneDayTime;
	this.curWeek = this.getWeekData(startTime);
}

WeekData.prototype.getDayIndexInWeekData = function(dayTime){
	var index = -1;
	for(var i=0; i<this.curWeek.length; i++){
		if(this.curWeek[i].formatDate === dayTime){
			index = i;
			break;
		};
	};
	return index;
}

WeekData.prototype.formatDate = function(dayTime){
	var theDate = new Date(dayTime);
	var year = theDate.getFullYear();
	var day = theDate.getDate().toString().length === 1?'0' + theDate.getDate():theDate.getDate();
	var month = (theDate.getMonth() + 1).toString().length === 1?'0' + (theDate.getMonth() + 1):theDate.getMonth();
	return year + '-' + month + '-' + day;
}
var u = navigator.userAgent;
 var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
  if (isIOS) {
  	window.confirm = function (message) {
	   var iframe = document.createElement("IFRAME");
	   iframe.style.display = "none";
	   iframe.setAttribute("src", 'data:text/plain,');
	   document.documentElement.appendChild(iframe);
	   var alertFrame = window.frames[0];
	   var result = alertFrame.window.confirm(message);
	   iframe.parentNode.removeChild(iframe);
	   return result;
	}; 
	window.alert = function(name){
		var iframe = document.createElement("IFRAME");
		iframe.style.display="none";
		iframe.setAttribute("src", 'data:text/plain,');
		document.documentElement.appendChild(iframe);
		window.frames[0].window.alert(name);
		iframe.parentNode.removeChild(iframe);
	};
  	
  	
  	
  }
