		//测试数据
		mui.plusReady(function() {

			var role = localStorage.getItem("role");
			//url = "wybx.html";
			if(role.indexOf("基地领导") != -1) {
				var statusType = false; //true用户
				var usertype = 3;
			} else if(role.indexOf("基地经理") != -1) {
				var statusType = false; //true用户
				var usertype = 2;
			} else {
				var usertype = 1;
				var statusType = true; //true用户
			}
			var self = plus.webview.currentWebview();
			var bxId = self.bxId;
			var username = localStorage.getItem("userName");
			//获取详情数据
			function getRepairDetails() {
				mui.ajax(ajaxDomain + "/repair/getRepairDetails.do", {
					data: {
						id: bxId
					},
					dataType: 'json',
					type: 'post', //HTTP请求类型
					success: function(data) {
						//创建头部内容
						var data1 = data.data.repair;

						if(data1.urge == 1) {
							$(".topRow1_span2").show().html("催办");
						}

						/*if(role.indexOf("基地领导")!=-1) {
											$(".assessDivs").hide();
									};
									*/
						$(".topRow1_span1").html("报修人: " + data1.username);
						$(".topRow2_span1").html(data1.createtime);
						$(".topRow3_span1").html("隶属基地: " + data1.basename);
						//创建中间内容
						$(".centerRow1").html(data1.content);
						var data2 = data.data.files;
						if(data2.length > 0) {
							for(var i = 0; i < data2.length; i++) {
								var img = $("<img/>").attr({
									"src": ajaxDomain + data2[i].photoname
								});
								$(".centerImg").append(img);
							}
						}
						var bottomData = data.data.messages;
						/*for(var i=0;i<5;i++){
							bottomData = bottomData.concat(bottomData);
						}*/
						var items = [];
						for(var i = 0, len = bottomData.length; i < len; i++) {
							var item = [
								'<li class="bottomLi">',
								'<div class="li_left">',
								'<img src="', ajaxDomain + bottomData[i].photoname, '"/>',
								'</div>',
								'<div class="li_right">',
								'<div class="li_right_row1">',
								'<span class="li_right_span1">', bottomData[i].username, '</span>',
								'<span class="li_right_span2 ', (function() {
									if(bottomData[i].usertype == "1") {
										return 'cls0';
									} else {
										return 'cls1';
									}
								})(), '">', (function() {
									if(bottomData[i].usertype == "1") {
										return "报修人";
									} else if(bottomData[i].usertype == "2") {
										return "基地经理";
									} else if(bottomData[i].usertype == "3") {
										return "基地领导";
									} else if(bottomData[i].usertype == "4") {
										return "维修工人";
									}
								})(), '</span>',
								'<span class="li_right_span3">', (function() {
									var item = bottomData[i].createtime.substring(0, 11);

									return item;
								})(), '</span>',
								'</div>',
								'<p class="li_right_row2">', !bottomData[i].content ? "" : bottomData[i].content, '</p>',
								'<div class="imgBoxs">',
								(function(i) {
									if(bottomData[i].jpgs == undefined || bottomData[i].jpgs.length == 0) {
										return;
									}
									if(bottomData[i].jpgs.length != 0) {
										var node = [];
										for(var j = 0; j < bottomData[i].jpgs.length; j++) {
											if(j == 0) {
												var temp1 = "0px"
											} else {
												var temp1 = "13px";
											}
											var img = "<img src='" + ajaxDomain + bottomData[i].jpgs[j].photoname +
												"' data-preview-src='' data-preview-group='1'  style='display:inline-block;width:1.76rem;height:1.48rem;margin-left:" + temp1 + "'/>";
											node.push(img);
										};
										return node.join('');
									}
								})(i), '</div>',
								'</div>',
								'</li>',
							].join('');
							items.push(item);
						}
						$(".bottomUl").append(items.join(''));
						switch(usertype) {
							case 1: //普通用户
								if(data1.status == 0) {
									$(".leaderDiv").hide();
									$(".assessBox").hide();
									$(".btnDivBox").hide();
								} else if(data1.status == 1) {
									$(".leaderDiv").hide();
									$(".assessBox").show();
									$(".btnDivBox").hide();
								} else if(data1.status == 2) {
									$(".leaderDiv").hide();
									$(".assessBox").show();
									$(".btnDivBox").hide();
								} else if(data1.status == 3) {
									$(".leaderDiv").hide();
									$(".assessBox").show();
									$(".btnDivBox").hide();
								} else if(data1.status == 4) {

									$(".leaderDiv").hide();
									$(".assessBox").hide();
									$(".assessBoxRow2").css("height", "0.8rem");
									if(data1.degreestatus == 0) {
										getWordBookByCode("1002", data1, true);
									}
									//getWordBookByCode("1002",data1,true);
									$(".assessBoxRow3").hide();
									$(".assessCommit").addClass("finalCommit");

								}
								break;
							case 2: //基地经理
								if(data1.status == 0) {
									$(".leaderDiv").hide();
									$(".assessBox").hide();
									$(".btnDivBox").show();
								} else if(data1.status == 1) {
									$(".leaderDiv").show();
									$(".assessBox").hide();
									$(".btnDivBox").hide();
								} else if(data1.status == 2) {
									$(".leaderDiv").show();
									$(".assessBox").hide();
									$(".btnDivBox").hide();
								} else if(data1.status == 3) {
									$(".leaderDiv").show();
									$(".assessBox").hide();
									$(".btnDivBox").hide();
								} else if(data1.status == 4) {
									//if(data1.userid==userId){
									$(".leaderDiv").hide();
									$(".assessBox").hide();
									$(".assessBoxRow2").css("height", "0.8rem");
									if(data1.degreestatus == 0) {
										getWordBookByCode("1002", data1, true);
									}
									//getWordBookByCode("1002",data1,true);
									$(".assessBoxRow3").hide();
									$(".assessCommit").addClass("finalCommit");
									/*}else{
										$(".leaderDiv").hide();
										$(".assessBox").hide();
										$(".btnDivBox").hide();
									}*/

								}
								break;
							case 3: //基地领导
								if(data1.status == 0) {
									$(".leaderDiv").hide();
									$(".assessBox").hide();
									$(".btnDivBox").hide();
								} else if(data1.status == 1) {
									$(".leaderDiv").hide();
									$(".assessBox").show();
									$(".btnDivBox").hide();
								} else if(data1.status == 2) {
									$(".leaderDiv").hide();
									$(".assessBox").show();
									$(".btnDivBox").hide();
								} else if(data1.status == 3) {
									$(".leaderDiv").hide();
									$(".assessBox").show();
									$(".btnDivBox").hide();
								} else if(data1.status == 4) {
									$(".leaderDiv").hide();
									$(".assessBox").hide();
									$(".assessBoxRow2").css("height", "0.8rem");
									if(data1.degreestatus == 0) {
										getWordBookByCode("1002", data1, true);
									}
									//	getWordBookByCode("1002",data1,true);
									$(".assessBoxRow3").hide();
									$(".assessCommit").addClass("finalCommit");
								}
								break;
							default:
								break;
						}

					},
					error: function(xhr, type, errorThrown) {
						//异常处理；
					}
				});
			}
			//获取报修评价
			function getEvaluate(value, itemid, id, degreestatus) {
				var djson = {
					value: value,
					itemid: itemid,
					id: id,
					degreestatus: degreestatus
				}
				mui.ajax(ajaxDomain + "/repair/evaluate.do", {
					data: {
						data: JSON.stringify(djson)
					},
					dataType: 'json',
					type: 'post', //HTTP请求类型
					success: function(data) {

					},
					error: function(xhr, type, errorThrown) {
						//异常处理；
					}
				});
			}
			//报修确认
			function getsubmitData(isOk) {
				var djson = {
					id: bxId,
					isOk: isOk
				}
				mui.ajax(ajaxDomain + "/repair/submit.do", {
					data: {
						data: JSON.stringify(djson)
					},
					dataType: 'json',
					type: 'post', //HTTP请求类型
					success: function(data) {

					},
					error: function(xhr, type, errorThrown) {
						//异常处理；
					}
				});
			}
			//处理完毕
			function getFinish() {
				mui.ajax(ajaxDomain + "/repair/finish.do", {
					data: {
						id: bxId
					},
					dataType: 'json',
					type: 'post', //HTTP请求类型
					success: function(data) {},
					error: function(xhr, type, errorThrown) {
						//异常处理；
					}
				});
			}
			//获取回复评价
			function getAddRepairMsg(content) {
				var jsona = {
					reid: bxId,
					userid: userId,
					username: username,
					usertype: usertype,
					content: content
				};
				mui.ajax(ajaxDomain + "/repair/addRepairMsg.do", {
					data: {
						reid: bxId,
						userid: userId,
						username: username,
						usertype: usertype,
						content: content
					},
					dataType: 'json',
					type: 'post', //HTTP请求类型
					success: function(data) {
						var bottomData = data.data;
						var item = [
							'<li class="bottomLi">',
							'<div class="li_left">',
							'<img src="', ajaxDomain + bottomData.file["PHOTONAME"], '"/>',
							'</div>',
							'<div class="li_right">',
							'<div class="li_right_row1">',
							'<span class="li_right_span1">', bottomData.obj.username, '</span>',
							'<span class="li_right_span2 ', (function() {
								if(bottomData.obj.usertype == "1") {
									return 'cls0';
								} else {
									return 'cls1';
								}
							})(), '">', (function() {
								if(usertype == "1") {
									return "报修人";
								} else if(usertype == "2") {
									return "基地经理";
								} else if(usertype == "3") {
									return "基地领导";
								}
							})(), '</span>',
							'<span class="li_right_span3">', bottomData.obj.createtime, '</span>',
							'</div>',
							'<p class="li_right_row2">', !bottomData.obj.content ? "" : bottomData.obj.content, '</p>',
							'</div>',
							'</li>',
						].join('');
						$(".bottomUl").append(item);
					},
					error: function(xhr, type, errorThrown) {
						//异常处理；
					}
				});
			}
			//拿到满意数据
			function getWordBookByCode(code, data1, statusDiv) {
				mui.ajax(ajaxDomain + "/wordbook/getWordBookByCode.do", {
					data: {
						code: code,
					},
					dataType: 'json',
					type: 'post', //HTTP请求类型
					success: function(data) {
						var needData = data.data;
						for(var i = 0; i < needData.length; i++) {
							/*if(i==0){
								var active = "active";
							}else{
								var active = "";
							}*/
							var active = "";
							var span = '<span class="assessSpan ' + active + '" codeId="' + needData[i].id + '" code="' + needData[i].code + '" status="' + (i + 1) + '" >' + needData[i].name + '</span>';
							$(".assessBoxRow2").append(span);
						}
						if(data1.degreestatus == 0) {
							//$(".assessBox").show(); 
							if(data1.userid == userId) {
								$(".assessBox").show();
								$(".assessBoxRow2").show();
								$(".assessBoxRow1").show();
							} else {
								$(".assessBoxs").hide();
							}

						}
						if(data1.degreestatus == 1) {
							$(".assessBox").show();
							$(".assessText").hide();
							$(".assessCommit").hide();
							$(".assessSpan").eq(0).addClass("active").siblings().removeClass("active");
							$(document).off('tap', ".assessSpan");
							$(".assessBox").css({
								'height': '1rem',
							});
							$(".assessBoxRow2").css({
								'margin-top': '-0.3rem'
							})
						} else if(data1.degreestatus == 2) {
							$(".assessBox").show();
							$(".assessText").hide();
							$(".assessCommit").hide();
							$(".assessSpan").eq(1).addClass("active").siblings().removeClass("active");
							$(document).off('tap', ".assessSpan");
							$(".assessBox").css({
								'height': '1rem',
							});
							$(".assessBoxRow2").css({
								'margin-top': '-0.3rem'
							})
						} else if(data1.degreestatus == 3) {
							$(".assessBox").show();
							$(".assessText").hide();
							$(".assessCommit").hide();
							$(".assessSpan").eq(2).addClass("active").siblings().removeClass("active");
							$(".assessBox").css("height", "3rem");
							$(".assessBoxRow3").show();
							$(".assessBoxRow3").val(data1.value)
							$(".assessBoxRow3").attr("readonly", true);
							$(document).off('tap', ".assessSpan");
						}
						/*if(statusDiv){
							$(".leaderDiv").show();
							$(".assessDivs").show(); 
						}*/
					},
					error: function(xhr, type, errorThrown) {
						//异常处理；
					}
				});
			}
			//获取完成状态
			//getWordBookByCode("1002");
			//getWordBookItemListByWbId("c7bc06eaba91412b844861c5507ddc06");
			var h = $(window).height();
			$("body,html").css({
				"overflow": "hidden",
				"height": h + "px"
			});
			var u = navigator.userAgent;
			var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
			if(isIOS) {
				var originalHeight = document.documentElement.clientHeight || document.body.clientHeight;
				window.onresize = function() {
					var resizeHeight = document.documentElement.clientHeight || document.body.clientHeight;
					//软键盘弹起与隐藏  都会引起窗口的高度发生变化
					if(resizeHeight * 1 < originalHeight * 1) { //resizeHeight<originalHeight证明窗口被挤压了
						mui.scrollTo("10%", 0)
					}
				}

				plus.webview.currentWebview().setStyle({　　
					softinputMode: "adjustResize" // 弹出软键盘时自动改变webview的高度
				});

			}
			var resNumber = 0;
			var firstLoad = true;
			//点击报修评价
			$(document).on('tap', ".assessSpan", function(e) {
				e.stopPropagation();
				if(!$(this).hasClass("active")) {
					$(this).addClass("active");
					$(this).siblings().removeClass("active");
					if($(this).attr("status") == 3) {
						$(".assessBoxRow3").show();
					} else {
						$(".assessBoxRow3").hide();
					}
					if($(this).attr("codeId") == "c7bc06eaba91412b844861c5507ddc06") {
						$(".assessBox").css("height", "3rem");
						$(".assessBoxRow3").show();
					} else {
						$(".assessBox").css("height", "2rem");
						$(".assessBoxRow3").hide();
					}
					return;
					var result = confirm("是否确认提交？");
					if(result) {
						$(this).addClass("active");
						$(this).siblings().removeClass("active");
						if($(this).attr("codeId") == "c7bc06eaba91412b844861c5507ddc06") {
							$(".assessBox").css("height", "3rem");
							$(".assessBoxRow3").show();
						} else {
							$(".assessBox").css("height", "2rem");
							$(".assessBoxRow3").hide();
						}
					}
				}
			});
			//点击提交
			$(document).on('tap', ".assessCommit", function(e) {
				e.stopPropagation();
				if($(this).hasClass("finalCommit")) { //最终评价
					var result = confirm("是否确认提交？");
					if(result) {
						var value = $(".assessBoxRow3").val();
						var itemid = $(".assessSpan.active").attr("codeId");
						var id = bxId;
						var degreestatus = $(".assessSpan.active").attr("status");
						getEvaluate(value, itemid, id, degreestatus);
						$(".assessBox").hide();
						mui.back();
					}

				} else {
					var content = $(".assessBoxRow3").val();
					if(content != "") {
						getAddRepairMsg(content);
						$(".assessBoxRow3").val('');
						$(".assessBoxRow3").blur();
					}
				}
			});
			if(statusType) { //--用户层
				$(".assessBox").show();
				$(".assessBoxRow3").show();

			} else { //--领导层

				$(".mainBody").addClass('mainBody1');
				$(".leaderDiv").show();
				//submit--报修确认
				$(document).on('tap', ".btnDivBox div", function(e) {
					if($(this).hasClass("bohuiBtn")) { //驳回
						getsubmitData(false);
						mui.back();
					} else {
						getsubmitData(true);
						mui.back();
					}
				});

				//点击确认
				$(document).on('tap', "#leaderText", function(e) {
					e.stopPropagation();
					if(resNumber == 1) { //最终评价
						getFinish();
						$(".leaderDiv").hide();
						setTimeout(function() {
							mui.back();
						}, 1000)
					} else {
						var content = $(".leaderDivRow1").val();
						if(content != "") {
							getAddRepairMsg(content);
							$(".leaderDivRow1").val('');
							$(".leaderDivRow1").blur();
						} else {
							alert("请输入处理结果!");
						}
					}

				});
				//点击按钮
				$(document).on('tap', ".toggle", function(e) {
					resNumber = resNumber == 0 ? 1 : 0;
					if(resNumber == 1) {
						$(".leaderDiv").css({
							"height": '1rem',
							"padding-top": '0.2rem'
						})
						$(".leaderDivRow1").hide();
						$("#leaderText").css({
							"top": "0.2rem"
						});
					} else {
						$(".leaderDiv").css({
							"height": '3rem',
							"padding-top": "0px"
						})
						$(".leaderDivRow1").show();
						$("#leaderText").css({
							"top": "2rem"
						})
					}
				});
			}
			getRepairDetails();
			mui.init({
				beforeback: function() {　　　　 //获得父页面的webview
					var list = plus.webview.currentWebview().opener();　　　　 //触发父页面的自定义事件(refresh),从而进行刷新
					mui.fire(list, 'refresh1');
					//返回true,继续页面关闭逻辑
					return true;
				},
				swipeBack: true //启用右滑关闭功能
			});
			var deceleration = mui.os.ios ? 0.003 : 0.0009;
			mui('.mui-scroll-wrapper').scroll({
				deceleration: deceleration,
				indicators: false
			});
			//返回按钮
			mui('.smailNav').on('tap', '.back', function(e) {
				mui.init({
					beforeback: function() {　　　　 //获得父页面的webview
						var list = plus.webview.currentWebview().opener();
						mui.fire(list, 'refresh2');
						//返回true,继续页面关闭逻辑
						return true;
					},
					swipeBack: true //启用右滑关闭功能
				});
				mui.back();
			});

		});