$(function(){
	var testData = {
	//头部数据
	lunboData:[
		
	],
	fontTextLength:25,
	//用户数据
	bottomData:[
			
	],
}
//获取数据
var pageStart = 1;
var firstLoad = true;
var count; 
function getfindData(){
		setTimeout(function() {
				mui.ajax(ajaxDomain + "/appearance/findData.do",{
								data:{
									page:1,
									limit:5
								},
			 					dataType:'json',
								type:'post',//HTTP请求类型
								success:function(data){ 
									//底部数据
									count = data.data.count;
									var nodes = data.data.data; 
									//渲染底部数据
									var items = [];
									$(".mainBox").empty();
									for (var i=0,len=nodes.length;i<len;i++) {
										var item = [
											'<li class="bottomLi">',
												'<div class="',(function(){
													if(nodes[i]["IMAGENAMESTR"]!= ""&&nodes[i]["IMAGENAMESTR"]!=undefined) {
															return "li_left";
														}else{
															return "li_left1";
														}
												})(),'">',
													'<img src="',(function(){
														if(nodes[i]["IMAGENAMESTR"]!= ""&&nodes[i]["IMAGENAMESTR"]!=undefined) {
															return ajaxDomain+nodes[i]["IMAGENAMESTR"];
														}
													})(),'"/>',
												'</div>',
												'<div class="li_right_com ',(function(){
													if(nodes[i]["IMAGENAMESTR"]!= ""&&nodes[i]["IMAGENAMESTR"]!=undefined) {
															return "li_right0";
														}else{
															return "li_right1";
														}
												})(),'" $id="'+nodes[i]["ID"]+'">',
													'<div class="li_right_row1">',
														'<p class="li_right_span1" >',(
															function(){
																if(nodes[i]["TITLE"]&&nodes[i]["TITLE"].length>testData.fontTextLength){
																	var node= nodes[i]["TITLE"].substr(0,testData.fontTextLength)+"...";
																	return node;
																}else{
																	return nodes[i]["TITLE"];
																}
															}
														)(),'</p>',
													'</div>',
													'<div class="li_right_row2">',
														'<span class="li_right_span2">',nodes[i]["CREATETIME"],'</span>',
														/*'<span class="li_right_span3" $id="'+nodes[i]["ID"]+'">...</span>',*/
													'</div>',
												'</div>',
														
											'</li>',
										].join('');
										items.push(item);
									}
									$(".mainBox").append(items.join(''));
									$(".mui-pull-bottom-tips").hide();
									pageStart=2;
								},
								error:function(xhr,type,errorThrown){
									console.log("请求异常")
								}
							});
		},100)				
}

function getfindData1(){
		setTimeout(function() {
				mui.ajax(ajaxDomain + "/appearance/findData.do",{
								data:{
									page:pageStart,
									limit:5
								},
			 					dataType:'json', 
								type:'post',//HTTP请求类型
								success:function(data){ 
									//底部数据 
									if(data.data==null){
										return;
									}
									var nodes = data.data.data;
									//渲染底部数据 
									var items = [];  
									//console.log(testData.bottomData)
									for (var i=0,len=nodes.length;i<len;i++) {
										var item = [
											'<li class="bottomLi">',
												'<div class="',(function(){
													if(nodes[i]["IMAGENAMESTR"]!= ""&&nodes[i]["IMAGENAMESTR"]!=undefined) {
															return "li_left";
														}else{
															return "li_left1";
														}
												})(),'">',
													'<img src="',(function(){
														if(nodes[i]["IMAGENAMESTR"]!= ""&&nodes[i]["IMAGENAMESTR"]!=undefined) {
															return ajaxDomain+nodes[i]["IMAGENAMESTR"];
														}
													})(),'"/>',
												'</div>',
												'<div class="li_right_com ',(function(){
													if(nodes[i]["IMAGENAMESTR"]!= ""&&nodes[i]["IMAGENAMESTR"]!=undefined) {
															return "li_right0";
														}else{
															return "li_right1";
														}
												})(),'" $id="'+nodes[i]["ID"]+'">',
													'<div class="li_right_row1">',
														'<p class="li_right_span1" >',(
															function(){
																if(nodes[i]["TITLE"]&&nodes[i]["TITLE"].length>testData.fontTextLength){
																	var node= nodes[i]["TITLE"].substr(0,testData.fontTextLength)+"...";
																	return node;
																}else{
																	return nodes[i]["TITLE"];
																}
															}
														)(),'</p>',
													'</div>',
													'<div class="li_right_row2">',
														'<span class="li_right_span2">',nodes[i]["CREATETIME"],'</span>',
														/*'<span class="li_right_span3" $id="'+nodes[i]["ID"]+'">...</span>',*/
													'</div>',
												'</div>',
														
											'</li>',
										].join('');
										items.push(item);
									}
									$(".mainBox").append(items.join(''));
									
									pageStart++;
								}, 
								error:function(xhr,type,errorThrown){
									console.log("请求异常")
								}
							});
		},100)				
}	
getfindData();
getfindBannerData();
function getfindBannerData(){
				mui.ajax(ajaxDomain + "/appearance/findBannerData.do",{
								data:{
									page:1,
									limit:10
								},
			 					dataType:'json',
								type:'post',//HTTP请求类型
								success:function(data){ 
									//创建轮播图
									var cops = data.data.data;
									if(cops.length>=1){
										for(var i=0;i<cops.length;i++){
											var obj  = {};
											obj.$imgUrl = ajaxDomain+cops[i]["IMAGENAMESTR"];
											obj.$title = cops[i].TITLE;
											obj.$id = cops[i].ID;
											testData.lunboData.push(obj)
										} 
									}else{
										$(".roundBox").remove();
										$(".mainBody").css('top','20px');
										return;
									}
									var doms = [];
									for(var i=0;i<testData.lunboData.length;i++){
										var dom1 = [
												'<div class="swiper-slide "  $id="',testData.lunboData[i].$id,'"  style="background-image: url(',testData.lunboData[i].$imgUrl,');">',
													'<div class="swiper-slide-shadow-left" >',
														
													'</div>',
													'<div class="swiper-slide-shadow-right" >',
														
													'</div>',
													'<span class="swiper-text" >',
														testData.lunboData[i].$title,
													'</span>',
												'</div>',
											]
										doms.push(dom1.join(''))
										
									}
									$(".swiper-wrapper").append(doms.join(''));
									  var swiper = new Swiper('.swiper-container', {
									      effect: 'coverflow',
									      grabCursor: true,
									      centeredSlides: true,
									      slidesPerView: 'auto',
									      //autoplay:true,
									      
									      coverflowEffect: {
									        rotate: 50,
									        stretch: 1,
									        depth: 100,
									        modifier: 1,
									        slideShadows : true,
									      },
									      loop:true
									      /*pagination: {
									        el: '.swiper-pagination',
									      },*/
								    });
									 var myEvent = new Event('resize');
    								  window.dispatchEvent(myEvent);
    
									
								},
								error:function(xhr,type,errorThrown){
									console.log("请求异常")
								}
							});
}

//
(function($,jq) {
				//阻尼系数
				var deceleration = mui.os.ios?0.003:0.0009;
				$('.mui-scroll-wrapper').scroll({
					bounce: false,
					indicators: true, //是否显示滚动条
					deceleration:deceleration
				});
				$.ready(function() {
					$('#test').pullToRefresh({
							down: {
								callback: function() {
									var self = this;
									jq(".mui-pull-bottom-tips").hide();
									self.endPullUpToRefresh(false);
									
									setTimeout(function() {
										getfindData();
										self.endPullDownToRefresh(true);
										self.refresh(true);
									}, 100); 
								}
							},
							up: {
								callback: function() {
									var self = this;
									jq(".mui-pull-bottom-tips").show();
									getfindData1();
									setTimeout(function() {
										var list = jq(".bottomLi").length;
										if(count<=list){ 
											 self.endPullUpToRefresh(true)
										}else{
											 self.endPullUpToRefresh(false)
										}
									}, 1000);
								}
							}
						});
				});
})(mui,jQuery);
    mui.init({
        swipeBack:true //启用右滑关闭功能【一旦取值为false，左右触控滑动将会失效！】
    });
    var slider = mui("#slider");
    slider.slider({
        interval: 100000
    });
	$(document).on('tap','.li_right_com',function(){
		var id = $(this).attr("$id");
				mui.openWindow({
						url: "fc_tzgg.html",
						id: id,
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"tag": "fc_tzgg"
						},
						waiting: {
							autoShow: false
						}
				});
		
		
	});
	
	$(document).on('tap','.swiper-slide',function(){
		var id = $(this).attr("$id");
				mui.openWindow({
						url: "fc_tzgg.html",
						id: id,
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"tag": "fc_tzgg"
						},
						waiting: {
							autoShow: false
						}
				});
		
		
	});	
	
	//点击文字详细
	$(document).on('tap','.li_right_span3',function(){
		var id = $(this).attr("$id");
				mui.openWindow({
						url: "fc_tzgg.html",
						id: id,
						styles: {
							popGesture: "close"
						},
						show: {
							aniShow: "pop-in"
						},
						extras: {
							"tag": "fc_tzgg"
						},
						waiting: {
							autoShow: false
						}
				});
		
		
	});
	mui('.smailNav').on('tap', '.back', function(e) {
			mui.back();
	});

	var deceleration = mui.os.ios ? 0.003 : 0.0009;
})
