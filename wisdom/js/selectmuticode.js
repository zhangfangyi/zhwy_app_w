var Drop = (function() {
	var finishArr = [];

	function $getId(id) {
		return $("#" + id)
	}

	function $getClassclass(id) {
		return $("." + id)
	}

	function $getClass(dom) {
		return $('.' + dom)
	}

	function $appendHtml(config) {
		var arr = config.dataArr,
			type = config.type,
			title = config.title,
			bodyDom = $('body'),
			arrLen = arr.length;
		var html = '';
		html += "<div class='drop display_none' style='width:100%;'>";
		html += "<div class='down_cont' style='width:100%;'>";
		html += "  <div class='title'>";
		html += "    <span class='cacel' id='cacel'>取消</span>";
		html += "    <span class='name' id='titleName'>" + title + "</span>";
		html += "    <span class='finsih' id='finsih' data-type='" + type + "'>完成</span>";
		html += "  </div>";
		html += "  <div class='content' style='height: 260px;overflow-y:scroll;width:100%'>";
		if(arr) {
			for(var i = 0; i < arrLen; i++) {
				html += "<div class='mui-input-row mui-checkbox mui-left " + type + "' style='border-bottom: 1px solid #ededed;width:100%;'>";
				html += "<label class='tag'>" + arr[i].text + "</label>";
				html += "<input name='checkbox1' value='" + arr[i].text + "' class='incheck' type='checkbox' id='" + arr[i].code + "'>";
				html += "</div>";
			}
		} else {
			html += "请传入数据！！"
		}
		html += "  </div>";
		html += "</div>";
		//		html += "<div class='other_box'>";
		html += "</div>";
		html += "</div>";
		bodyDom.append(html);
		//		$initActiveClass()
	}

	function $dropdwonShow(bool) {
		let drop = $getClass('drop');
		return bool ? drop.show(0) : drop.hide(0)
	}

	$('body').on('click', '#cacel', function() {
		$dropdwonShow(false)
	});
	$('body').on('click', '.other_box', function() {
		$dropdwonShow(false)
	});
	function domClick(config) {
		this.success = config.success;
		this.type = config.type;
		$appendHtml(config);
		$dropdwonShow(true);
		var $this = this;
		$('body').on('click', '#finsih', function(config) {
			var res = getcheckBoxRes('incheck');
			if(res.length > 0) {
				$dropdwonShow(false);
			}else{
				$dropdwonShow(false);
			}
			$this.success(res);
			return res
		})
		
	}

	function getcheckBoxRes(className) {
		var rdsobj = document.getElementsByClassName(className);
		var checkVal;
		var checkid;
		var vaid = [];
		var vaidli =[];
		for(i = 0; i < rdsobj.length; i++) {
			if(rdsobj[i].checked) {
				checkVal = rdsobj[i].value;
				checkid = rdsobj[i].id;
				vaid.push(checkid)
				vaidli.push(checkVal)
			}
		}
		return vaid;
		

	}
	return domClick
})()