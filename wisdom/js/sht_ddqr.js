mui.init({
	swipeBack: true, //启用右滑关闭功能
	keyEventBind: {
		backbutton: false //关闭back按键监听
	},
	beforeback: function() {　　　　 //获得父页面的webview
		var list = plus.webview.currentWebview().opener();　　　　 //触发父页面的自定义事件(refresh),从而进行刷新
		mui.fire(list, 'refreshshtIndex');
		//返回true,继续页面关闭逻辑
		return true;
	}
});
//设置自定义刷新事件
window.addEventListener('refreshshtDdqr', function(e) { //执行刷新
	location.reload();
});
var baseId = localStorage.getItem('baseId');
var cardNumber = localStorage.getItem('cardnumber');
var merchantId = localStorage.getItem('merchantId');
var merchantName = localStorage.getItem('merchantName');
//获取主要内容数据
var contentData = {};
mui.ajax(ajaxDomain + '/orderInfo/createOrder.do', {
	data: {
		cardnumber: cardNumber,
		baseId: baseId,
		merchantCode: merchantId
	},
	crossDomain: true,
	type: 'post', //HTTP请求类型
	async: false,
	success: function(data) {
		console.log(JSON.stringify(data))
		if(data.result === 'success') {
			contentData = data.data;
		};
	},
	error: function(xhr, type, errorThrown) {
		//异常处理；
		console.log(type);
	}
});
//滚动初始化
var deceleration = mui.os.ios ? 0.000003 : 0.0009;

//取货信息
var addressInfoApp = new Vue({
	el: '#addressInfo',
	data: function() {
		return {
			part1Main: '地址准备中......',
			part1Sub: '',
			ZTSJ_DATE: '时间就绪中......',
			ZTSJ_TIME: '',
			showTimeDom: contentData.MERCHANTTYPE == 1 ? true : false,
			showBtn: contentData.MERCHANTTYPE == 1 ? false : true
		};
	},
	methods: {
		toAddressPage: function() {
			mui.openWindow({
				url: 'wddzsht.html'
			});
		}
	},
	mounted: function() {
		if(contentData.MERCHANTTYPE == 1) {
			$('.address-detail').addClass('styleAlert');
		}
	},
	created: function() {
		if(contentData !== null) {
			var sesstionAddress = localStorage.getItem('shtAddressCheckData');
			if(sesstionAddress) {
				sesstionAddress = JSON.parse(sesstionAddress);
				contentData.ZTDZ = sesstionAddress.area + sesstionAddress.streetAddress;
				contentData.CONSIGNEE = sesstionAddress.consignee;
				contentData.PHONENUMBER = sesstionAddress.phone;
			} else {

			}
			this.part1Sub = contentData.ZTDZ !== '' ? contentData.ZTDZ : '暂无地址信息'; //
			//食堂
			if(contentData.MERCHANTTYPE == 1) {
				this.part1Main = '自提地址: ';
			};
			//商傢
			if(contentData.MERCHANTTYPE == 2) {
				this.part1Main = contentData.CONSIGNEE + ' ' + contentData.PHONENUMBER; //
			};
			if(this.showTimeDom) {
				this.ZTSJ_DATE = contentData.ZTSJ;
//				this.ZTSJ_TIME = contentData.ZTSJ.split(' ')[1];
				console.log(contentData.ZTSJ)
			}
		};
	}
})
//订单列表
var goodsApp = new Vue({
	el: '#goodsContainer',
	data: function() {
		return {
			items: []
		}
	},
	created: function() {
		var result = contentData.GOODS || [];
		for(var i = 0; i < result.length; i++) {
			result[i].PHOTONAME && (result[i].PHOTONAME = ajaxDomain + result[i].PHOTONAME);
		};
		this.items = result;
	},
	mounted: function() {
		mui('.scroll-goods').scroll({
			deceleration: deceleration,
			indicators: false
		});
	}

})

var balanceApp = new Vue({
	el: '#balanceContainer',
	data: function() {
		return {

		}
	},
	methods: {
		openConfirm: function() {
			if(goodsApp.items.length === 0) {
				mui.toast('亲,您当前没有选购任何商品,~');
				return;
			};
			if(contentData.ZTDZ === '') {
				mui.toast('无有效地址信息');
				return;
			}
			tipApp.showTip = true;
			console.log(tipApp);
		}
	},
	computed: {
		getTotal: function() {
			var result = 0;
			var data = contentData.GOODS || [];
			for(var i = 0; i < data.length; i++) {
				result += (data[i].PRICE * data[i].SHOPPINGNUM);
			};
			return result.toFixed(2);
		}
	},
	mounted: function() {

	}
})

var tipApp = new Vue({
	el: '#tipContainer',
	data: function() {
		return {
			showTip: false,
			tipContent: contentData.POPUPPROMPT ? contentData.POPUPPROMPT : '提示类信息'
		}
	},
	methods: {
		tipSubmitHandle: function() {
			this.showTip = false;
			passwordApp.showDom = true;
			passwordApp.msg = '';
			passwordApp.msgLength = 0;

		},
		tipCancelHandle: function() {
			this.showTip = false;
		}
	}
})

var passwordApp = new Vue({
	el: '#payPwd',
	data: function() {
		return {
			msg: '',
			msgLength: 0,
			showDom: false
		}
	},
	methods: {
		hideCurApp: function() {
			this.showDom = false;
			this.msg = '';
			this.msgLength = 0;
		},
		closeplhandle: function() {
			this.hideCurApp();
		},
		handleTheKey: function(theKey) {
			if(this.msg.length >= 6) {
				return;
			};
			this.msg += theKey;
			this.msgLength = this.msg.length;
		},
		cancelInput: function() {
			if(this.msg.length === 0) {
				return;
			};
			var msgArr = this.msg.split('');
			msgArr.pop();
			this.msg = msgArr.join('');
			this.msgLength = this.msg.length;
		},
		toPay: function() {
			var self = this;
			if(this.msg.length === 6) {
				mui.toast('支付操作进行中...')
				//密码加密
				mui.ajax(ajaxDomain + "/app/getRSAPublicKey.do", {
					data: {},
					type: 'post', //HTTP请求类型
					timeout: 8000, //超时时间设置为10秒；
					headers: {
						'Content-Type': 'application/json'
					},
					success: function(data) {
						var password = segmentEncrypt(self.msg, data);
						mui.ajax(ajaxDomain + '/orderInfo/payOrder.do', {
							data: {
								cardnumber: cardNumber,
								payPassword: password,
								address: contentData.ZTDZ,
								orderType: 1, //默认食堂类型
								payType: 1, //默认员工卡
								merchantCode: merchantId,
								merchantName: merchantName
							},
							type: 'post', //HTTP请求类型
							success: function(data) {
								//支付成功时
								if(data.result === 'success') {
									mui.toast('支付成功,即将跳转...');
									localStorage.setItem('orderPayer', data.data.PAYSUM);
									localStorage.setItem('orderList', data.data.ORDERID);
									localStorage.setItem('merchantId', merchantId);
									setTimeout(function() {
										mui.openWindow({
											url: 'sht-zhfjg.html',
											extras: {
												merchantId: merchantId
											}
										});
									}, 500);
									self.showDom = false;
								};
								//
								if(data.result === 'fail') {
									mui.toast(data.resume);
								};
								if(data.result === 'error') {
									mui.toast(data.resume);
									self.msg = '';
									self.msgLength = 0;
								};
							},
							error: function(xhr, type, errorThrown) {
								//异常处理；
								mui.toast(type)
							}
						});

					},
					error: function(xhr, type, errorThrown) {
						//异常处理；
						mui.toast(type)
					}
				});
			} else {
				mui.toast('请输入六位密码');
			};
		}
	}
})

//对数据分段加密
function segmentEncrypt(enStr, publikKey) {
	window.rsa = new JSEncrypt();
	window.rsa.setPublicKey(publikKey);
	var result = [],
		offSet = 0,
		cache = '',
		i = 1,
		inputLen = enStr.length;

	while(inputLen - offSet > 0) {
		if(inputLen - offSet > 117) {
			cache = window.rsa.encrypt(enStr.substring(offSet, 117 * i));
		} else {
			cache = window.rsa.encrypt(enStr.substring(offSet));
		}
		// 加密失败返回原数据
		if(cache === false) {
			return enStr;
		}
		offSet = i++ * 117;

		result.push(cache);
	}
	return JSON.stringify(result);
}