//获取登录用户相关信息
var userId = localStorage.getItem('userId');
var userName = localStorage.getItem('userName');
mui.plusReady(function () {
	//获取页面参数
	var params = plus.webview.currentWebview();
	var opener = params.opener();
	//mui初始化
	mui.init({
		swipeBack: false, //启用右滑关闭功能
		gestureConfig:{
			tap: true //默认为true
	   }
	});
	
	var pageApp = new Vue({
		el:'#pageContent',
		data:function(){
			return {
				foodName:params.foodData.name,
				comList1:[],
				list1CheckIndex:-1,
				comList2:[],
				content:''
			}
		},
		created:function(){
			var self = this;
			mui.ajax(ajaxDomain + '/wordbook/getWordBookByCode.do',{
				data:{
					code:1001
				},
				type:'post',//HTTP请求类型
				success:function(data){
					if(data.result === 'success'){
						self.comList1 = data.data;
					};
				},
				error:function(xhr,type,errorThrown){
					//异常处理；
					mui.toast('无法连接网络');
				}
			});
		},
		methods:{
			listHandle:function(index){
				var self = this;
				this.list1CheckIndex = index;
				mui.ajax(ajaxDomain + '/wordbook/getWordBookItemListByWbId.do',{
					data:{
						wbId:self.comList1[index].id
					},
					type:'post',//HTTP请求类型
					success:function(data){
						if(data.result === 'success'){
							var result = data.data;
							for(var i=0; i<result.length; i++){
								result[i].checked = false;
							};
							self.comList2 = result;
						};
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('无法连接网络');
					}
				});
			},
			columnHandle:function(index){
				this.comList2[index].checked = !this.comList2[index].checked; 
			},
			saveHandle:function(){
				var self = this;
				if(self.list1CheckIndex === -1){
					mui.toast('请先评价');
					return;
				};
				//获取参数
				var _params = {
					cfId:params.foodData.id,
					userId:userId,
					userName:userName,
					type:self.comList1[self.list1CheckIndex].content,
					content:self.content,
					rel:(function(){
						var result = [];
						for(var i=0; i<self.comList2.length; i++){
							if(self.comList2[i].checked){
								result.push({
									itemId:self.comList2[i].id,
									name:self.comList2[i].name,
									value:self.content
								});
							};
						};
						return result;
					})()
				};
				mui.ajax(ajaxDomain + '/cook/addEvaluate.do',{
					data:{
						data : JSON.stringify(_params)
					},
					type:'post',//HTTP请求类型
					success:function(data){
						if(data.result === 'success'){
							mui.toast('点评成功');
							mui.fire(opener, 'refreshMRSP');
					        //返回true,继续页面关闭逻辑
					        return true;
						};
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('无法连接网络');
					}
				});
				
				
			}
		}
	})

	
})
