	 function openShare(){
	 	shareWebview();
	 }
	 
	 
	 mui.plusReady(function() {
			ws = plus.webview.currentWebview();
			//关闭splash页面；
			plus.navigator.closeSplashscreen();
		})
		var sharew;
		var ws = null;

	 /** 
		 *分享窗口
		 */
		function shareWebview() {
			ws = plus.webview.currentWebview();
			console.log(ws);
			if (sharew) { // 避免快速多次点击创建多个窗口
				return;
			}
			var top = plus.display.resolutionHeight - 134;
			var href = "share.html"; 
			sharew = plus.webview.create(href, "share.html", {
				width: '100%',
				height: '134',
				top: top,
				scrollIndicator: 'none',
				scalable: false,
				popGesture: 'none'
			}, {
				shareInfo: {
					"href": "www.baidu.com",
					"title": "智慧物业",
					"content": "欢迎使用智慧物业APP,点击查看订单详情！",
					"pageSourceId": ws.id
				} 
			});
			sharew.addEventListener("loaded", function() {
				sharew.show('slide-in-bottom', 300);
			}, false);
			// 显示遮罩层  
			ws.setStyle({
				mask: "rgba(0,0,0,0.5)"
			});
			// 点击关闭遮罩层
			ws.addEventListener("maskClick", closeMask, false);
		}
		


function closeMask() {
			ws.setStyle({
				mask: "none"
			});
			//避免出现特殊情况，确保分享页面在初始化时关闭 
			if (!sharew) {
				sharew = plus.webview.getWebviewById("share.html");
			}
			if (sharew) {
				sharew.close();
				sharew = null;
			}
		}

