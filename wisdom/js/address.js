 mui.plusReady(function() {
 	var type = plus.webview.currentWebview().type; 
 	if(type =="add"){
 		$(".zfNav").text("新增地址");
 		btnClick()//新增
 	}else{
 		var id = plus.webview.currentWebview().addressId;  
 		$(".delAddress").show();
 		draw(id);
 	}
 	//点击设置默认地址
 	$(".szmrdz").on('tap',function(){
		if($(this).find(".noSelected").hasClass("selected")){ 
			$(this).find(".noSelected").removeClass("selected"); 
		}else{
			$(this).find(".noSelected").addClass("selected"); 
		}
	});
	
	//城市联动
	var cityPicker3 = new mui.PopPicker({
						layer: 3
					});
	cityPicker3.setData(cityData3);
	var showCityPickerButton = document.getElementById('area');
	showCityPickerButton.addEventListener('tap', function(event) {
		$("input").blur();
		cityPicker3.show(function(items) {
			var param = items[0].text + " " + items[1].text + " " + items[2].text;
			$("#area").val(param);
			});
		}, false);
})
 function isPoneAvailable($poneInput) {
            var myreg=/^[1][3,4,5,7,8][0-9]{9}$/;
            if (!myreg.test($poneInput)) {
                return false;
            } else {
                return true;
            }
        }
//更具收货地址ID查询详情
function draw(id){
	//查询
		mui.ajax(ajaxDomain + "/deliveryAddress/findById.do",{ 
					data:{
						id:id
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						var data = data.data;
						$("#name").val(data.consignee);
						$("#phone").val(data.phone);
						$("#area").val(data.area);
						$("#streetAddress").val(data.streetAddress);
						$("#postalcode").val(data.postalcode);
						if(data.isDefault == 1){
							$(".noSelected").addClass("selected");
						}
					},
					error:function(xhr,type,errorThrown){
						mui.toast('网络无法连接', {
								duration: 'long', 
								type: 'div'
							});
					}
				});
				
				
	//删除
	$(".delAddress").on('tap',function(){
			mui.ajax(ajaxDomain + "/deliveryAddress/delete.do",{
					data:{ 
						id:id
					}, 
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						mui.toast('删除成功');
						setTimeout(function(){ 
							mui.back();
						},400)
					},
					error:function(xhr,type,errorThrown){
						mui.toast('网络无法连接', {
								duration: 'long', 
								type: 'div'
							});
					}
				});
	})		
	//修改
	$(".dzadd").on('tap',function(){
			var name = $("#name").val();
			var phone = $("#phone").val();
			var area = $("#area").val();
			var streetAddress = $("#streetAddress").val();
			var postalcode = $("#postalcode").val();
			var cardnumber = localStorage.getItem("cardnumber");
			var isDefault;
			if($(".szmrdz").find(".noSelected").hasClass("selected")){
				isDefault = 1;
			}else{
				isDefault = 0;
			}
			
			
		if($("#name").val() == ''){
			mui.toast("姓名必须填写");
			return false; 
		}
		if($("#phone").val() == "" || isNaN($("#phone").val()) || $("#phone").val().length != 11 ){  
		     
		      mui.toast('请填写正确手机号');
		        return false;  
		}else{
			var temp =  isPoneAvailable($("#phone").val());
		}
		if($("#area").val() == ''){
			mui.toast("地区不能为空")
			return false;
		}
		if($("#streetAddress").val() ==''){
			mui.toast("街道地址不能为空");
			return false;
		}
			mui.ajax(ajaxDomain + "/deliveryAddress/update.do",{
					data:{ 
						id:id,
						consignee:name,
						phone:phone,
						area:area,
						streetAddress:streetAddress,
						postalcode:postalcode,
						isDefault:isDefault,
						cardnumber:cardnumber
					}, 
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						mui.toast('修改成功');
						setTimeout(function(){ 
							mui.back();
						},400)
					},
					error:function(xhr,type,errorThrown){
						mui.toast('网络无法连接', {
								duration: 'long', 
								type: 'div'
							});
					}
				});
	})		
}
 
 
function btnClick(){
	//保存 新增收获地址
	$(".dzadd").on('tap',function(){
		var name = $("#name").val();
		var phone = $("#phone").val();
		var area = $("#area").val();
		var streetAddress = $("#streetAddress").val();
		var postalcode = $("#postalcode").val();
		var cardnumber = localStorage.getItem("cardnumber");
		var isDefault;
		if($(".szmrdz").find(".noSelected").hasClass("selected")){
			isDefault = 1;
		}else{
			isDefault = 0;
		}
		
		if($("#name").val() == ''){
			mui.toast("姓名必须填写");
			return false; 
		}
		if($("#phone").val() == "" || isNaN($("#phone").val()) || $("#phone").val().length != 11 ){  
		     
		      mui.toast('请填写正确手机号');
		        return false;  
		}else{
			var temp =  isPoneAvailable($("#phone").val());
			if(!temp){
				 mui.toast('请填写正确手机号');
				  return false;  
			}
		}
		if($("#area").val() == ''){
			mui.toast("地区不能为空")
			return false;
		}
		if($("#streetAddress").val() ==''){
			mui.toast("街道地址不能为空");
			return false;
		}
		
		
		mui.ajax(ajaxDomain + "/deliveryAddress/add.do",{
					data:{
						consignee:name,
						phone:phone,
						area:area,
						streetAddress:streetAddress,
						postalcode:postalcode,
						isDefault:isDefault,
						cardnumber:cardnumber
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						mui.toast('提交成功', {
							duration: 'long', 
							type: 'div'
						});
						setTimeout(function(){ 
							mui.back();
						},400)
					},
					error:function(xhr,type,errorThrown){
						mui.toast('网络无法连接', {
								duration: 'long', 
								type: 'div'
							});
					}
				});
	})
	
}
 