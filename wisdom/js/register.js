 var type;
 mui.plusReady(function() {
 	mui.init();
			var deceleration = mui.os.ios ? 0.003 : 0.0009;
			mui('.mui-scroll-wrapper').scroll({
				deceleration: deceleration,
				indicators: false
			});	
			//返回按钮
			
			
		type = plus.webview.currentWebview().typepwd; 
		if(type =="edit"){
			$(".editPwd").hide(); 
			$(".btnRes").text("确定");
		}
		
		mui('.zhwy').on('tap', '.resBack', function(e) {
			mui.back();
		});
 	
 	
 	btnClick();
 	
 	
//验证码
$(".generate_code").on('tap',function(){ 
	var disabled = $(".generate_code").attr("disabled");  
    if(disabled){  
        return false;  
    }  
	if($("#phone").val() == "" || isNaN($("#phone").val()) || $("#phone").val().length != 11 ){  
        mui.toast('请填写正确手机号');
        return false;  
    } 
    var phoneNumber = $("#phone").val();
      if(type == "edit"){
        		mui.ajax(ajaxDomain + "/app/getForgetPassVerityCode.do",{ 
						data:{
							phonenumber:phoneNumber
						},
	   					dataType:'json',
						type:'post',//HTTP请求类型
						success:function(data){ 
							if(data.result != "fail"){
								mui.toast('发送成功');
								settime();  
							}
						},
						error:function(xhr,type,errorThrown){
							mui.toast('网络无法连接');
						}
					});	
        	}else{
        		mui.ajax(ajaxDomain + "/app/getVerityCode",{ 
						data:{
							phonenumber:phoneNumber
						},
	   					dataType:'json',
						type:'post',//HTTP请求类型
						success:function(data){  
							if(data.result != "fail"){
								mui.toast('发送成功');
								settime();  
							}
						},
						error:function(xhr,type,errorThrown){
							mui.toast('网络无法连接');
						}
					});
			}
	});
});
 
var countdown=60;  
function settime() { 
      if (countdown == 0) {  
        $(".generate_code").attr("disabled",false);  
        $(".generate_code").val("获取验证码");
        countdown = 60;  
        return false;  
      } else {  
        $(".generate_code").attr("disabled", true);  
        $(".generate_code").val("重新发送(" + countdown + ")");
        countdown--;  
      }  
      setTimeout(function() {  
        settime();  
      },1000);  
}  

//18351853650

 
 //按钮事件
function btnClick(){
	$(".btnRes").on('tap',function(){ 
		var password = $("#password").val();
		mui.ajax(ajaxDomain + "/app/getRSAPublicKey.do", {
			data: {},
			//					dataType:'json',//服务器返回json格式数据
			type: 'post', //HTTP请求类型
			timeout: 5000, //超时时间设置为10秒；
			clossDomin: true,
			headers: {
				'Content-Type': 'application/json'
			},
			success: function(data) {
				password = segmentEncrypt(password, data);
				register(password);
			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
				mui.toast('网络无法连接');
			}
		});
		
	});
	
}


function register(password){
	var cardnumber=$("#cardnumber").val();
//	var userpass=$("#password").val();
	var phonenumber=$("#phone").val();
	var email=$("#email").val();
	var verifyCode=$("#yzm").val();
	if(type == "edit"){
			mui.ajax(ajaxDomain + "/app/forgetPass.do",{ 
						data:{
							cardnumber:cardnumber,
							userpass:password,
							phonenumber:phonenumber,
							verifyCode:verifyCode
						},
	   					dataType:'json',
						type:'post',//HTTP请求类型
						success:function(data){ 
							if(data.result !== "fail"){
								plus.webview.currentWebview().reload();
								mui.toast('修改成功');
									mui.openWindow({
									url: "login.html",
									styles: {
										popGesture: "close"
									},
									show: {
										aniShow: "pop-in"
									},
									extras: {
									},
								 	waiting: {
										autoShow: false
									}
								});
							}else{
								mui.toast('修改失败');
							}
						
						},
						error:function(xhr,type,errorThrown){
							mui.toast('网络无法连接');
						}
					});
	}else{
		mui.ajax(ajaxDomain + "/app/register.do",{ 
						data:{
							cardnumber:cardnumber,
							userpass:password,
							phonenumber:phonenumber,
							email:email,
							verifyCode:verifyCode
						},
	   					dataType:'json',
						type:'post',//HTTP请求类型
						success:function(data){ 
							if(data.result !== "fail"){
								plus.webview.currentWebview().reload();
								mui.toast('注册成功');
									mui.openWindow({
									url: "login.html",
									styles: {
										popGesture: "close"
									},
									show: {
										aniShow: "pop-in"
									},
									extras: {
									},
								 	waiting: {
										autoShow: false
									}
								});
							}else{
								mui.toast('注册失败');
							}
						
						},
						error:function(xhr,type,errorThrown){
							mui.toast('网络无法连接');
						}
					});
	}
	
}



//对数据分段加密
function segmentEncrypt(enStr, publikKey) {
	window.rsa = new JSEncrypt();
	window.rsa.setPublicKey(publikKey);
	var result = [],
		offSet = 0,
		cache = '',
		i = 1,
		inputLen = enStr.length;

	while(inputLen - offSet > 0) {
		if(inputLen - offSet > 117) {
			cache = window.rsa.encrypt(enStr.substring(offSet, 117 * i));
		} else {
			cache = window.rsa.encrypt(enStr.substring(offSet));
		}
		// 加密失败返回原数据
		if(cache === false) {
			return enStr;
		}
		offSet = i++ * 117;

		result.push(cache);
	}
	return JSON.stringify(result);
}