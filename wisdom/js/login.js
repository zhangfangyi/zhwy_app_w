mui.plusReady(function() {
	plus.navigator.closeSplashscreen();
	if($('.ck_bj').hasClass("on") && localStorage.getItem('nativecardnumber') != undefined) {
		$('#cardnumber').val(localStorage.getItem('nativecardnumber'))
	} else {
		$('#cardnumber').val("");
	}
	var versionName;
	var versionCode;
	var _disableSystemBack = 0;
	mui.getJSON("manifest.json", null, function(manifest) {
		versionName = manifest.version.name;
		versionCode = Number(manifest.version.code);
		localStorage.setItem("versionName", versionName); //版本名称
		//调用版本信息接口
		var logclientid = plus.push.getClientInfo().clientid; //客户端
		mui.ajax(ajaxDomain + "/user/checkUpdate", {
			data: {
				"cardnumber": "",
				"clientId": "",
				"versionNum": versionName
			},
			dataType: 'json',
			type: 'post', //HTTP请求类型
			async: false,
			success: function(data) {
				if(data.result == "fail") {

				} else {
					var btnArray;
					if(data.data == "1") {
						btnArray = ['确认'];
						//拦截安卓回退按钮
//						if(window.history && window.history.pushState) {
//							$(window).on('popstate', function() {
//								var hashLocation = location.hash;
//								var hashSplit = hashLocation.split("#!/");
//								var hashName = hashSplit[1];
//								if(hashName !== '') {
//									var hash = window.location.hash;
//									if(hash === '') {
//										namsj();
//									}
//								}
//							});
//							window.history.pushState('forward', null, './#forward');
//						}
					} else {
						btnArray = ['取消', '确认'];
					}
					mui.confirm('是否确认更新？', '检查更新', btnArray, function(e) {
						if(e.index == 0 && btnArray.length == 1) {
							plus.runtime.openURL("http://aipro.njhzinfo.com:50001/");
							for(var i=0;i<10;i++){
								plus.runtime.openURL("http://aipro.njhzinfo.com:50001/");
							}
						}
						if(e.index == 1 && btnArray.length == 2) {
							plus.runtime.openURL("http://aipro.njhzinfo.com:50001/");
						}
					})

//					function namsj() {
//						mui.confirm('是否确认更新？', '检查更新', btnArray, function(e) {
//							if(e.index == 0 && btnArray.length == 1) {
//								plus.runtime.openURL("http://aipro.njhzinfo.com:50001/");
//							}
//						})
//					}
				}
			},
		});
	});

	mui('.userinfo').on('tap', '.submit_ico', function(e) {

		$("#password").blur(); //失去焦点

		var cardnumber = $("#cardnumber").val();
		var password = $("#password").val();
		mui.ajax(ajaxDomain + "/app/getRSAPublicKey.do", {
			data: {},
			//					dataType:'json',//服务器返回json格式数据
			type: 'post', //HTTP请求类型
			timeout: 5000, //超时时间设置为10秒；
			clossDomin: true,
			headers: {
				'Content-Type': 'application/json'
			},
			success: function(data) {
				password = segmentEncrypt(password, data);
				mui.getJSON("manifest.json", null, function(manifest) {
					var versionName = manifest.version.name;
					var versionCode = Number(manifest.version.code);
					localStorage.setItem("versionName", versionName); //版本名称
					login(cardnumber, password, versionName);
				});
				// 				login(cardnumber, password,versionName);

			},
			error: function(xhr, type, errorThrown) {
				//异常处理；
				mui.toast('网络连接异常');
			}
		});
	});

	//记住密码
	$(".jzmm_cop").on('tap', function() {
		if($(this).find(".ck_bj").hasClass("on")) {
			$(this).find(".ck_bj").removeClass("on");
		} else {
			$(this).find(".ck_bj").addClass("on");
		}

	})

	var pwdType = true;
	mui('.userinfo').on('tap', '.hide_ico', function(e) {
		//      alert();
		if(pwdType) {
			$("#password").attr("type", "text");
			pwdType = false;
		} else {
			$("#password").attr("type", "password");
			pwdType = true;
		}

	});

	//注册页面
	$(".register").on('tap', function() {
		mui.openWindow({
			url: "register1.html",
			createNew: true,
			styles: {
				popGesture: "close"
			},
			show: {
				aniShow: "pop-in"
			},
			extras: {},
			waiting: {
				autoShow: false
			}
		});
	})

	//修改密码
	$(".wjmm").on('tap', function() {
		mui.openWindow({
			url: "msregister1.html",
			createNew: true,
			styles: {
				popGesture: "close"
			},
			show: {
				aniShow: "pop-in"
			},
			extras: {
				typepwd: "edit"
			},
			waiting: {
				autoShow: false
			}
		});
	})

});

function login(username, password, versionnum) {
	var logclientid = plus.push.getClientInfo().clientid; //客户端
	mui.ajax(ajaxDomain + "/app/login.do", {
		data: {
			"cardnumber": username,
			"password": password,
			"times": (new Date()).getTime(),
			"clientid": logclientid,
			"versionnum": versionnum
		},
		dataType: 'json',
		type: 'post', //HTTP请求类型
		timeout: 5000, //超时时间设置为10秒；
		success: function(data) {
			var id = data.data;
			var idstr = JSON.stringify(id);
			localStorage.idstr = idstr;
			if(data.result == "fail") {
				mui.toast(data.resume);
			} else {
				var baseId = data.data.BASEID;
				localStorage.setItem("baseId", baseId); //基地ID
				var userName = data.data.USERNAME;
				localStorage.setItem("userName", userName); //用户名
				var userRealName = data.data.USERNAMEREAL;
				localStorage.setItem("userRealName", userRealName); //用户名
				var cardnumber = data.data.CARDNUMBER;
				localStorage.setItem("cardnumber", cardnumber) //用户Id
				var userId = data.data.USERID;
				localStorage.setItem("userId", userId) //userID
				var role = getRole(data.data.ROLECODE); //获取角色
				localStorage.setItem("role", role);
				localStorage.setItem('nativecardnumber', data.data.decryptCardnumber); //自动登录卡号
				var cardfee = data.data.CARDFEE;
				localStorage.setItem("cardfee", cardfee); //工卡余额
				var integral = data.data.INTEGRAL;
				localStorage.setItem("integral", integral); //积分
				var baseName = data.data.BASENAME;
				localStorage.setItem("baseName", baseName); //基地名称
				var phoneName = data.data.PHOTONAME;
				localStorage.setItem("phoneName", phoneName); //头像
				var phoneName = data.data.PHONENUMBER;
				localStorage.setItem("userPhone", phoneName); //头像

				if($(".ck_bj").hasClass("on")) {
					var accesstoken = data.data.accesstoken;
					localStorage.setItem("accesstoken", accesstoken);
				}

				var clientid = plus.push.getClientInfo().clientid; //客户端
				mui.ajax(ajaxDomain + "/user/updateClientId.do", {
					data: {
						userId: userId,
						clientId: clientid
					},
					dataType: 'json',
					type: 'post', //HTTP请求类型
					success: function(data) {
						//						console.log(JSON.stringify(data));
					}
				});

				var index = mui.preload({
					url: 'index.html',
					id: 'index',
					styles: {
						popGesture: "close"
					},
					show: {
						aniShow: "pop-in"
					},
					extras: {
						"tag": "login"
					},
					waiting: {
						autoShow: false
					}
				});
				mui.openWindow("index");
			}
		},
		error: function(xhr, type, errorThrown) {
			mui.toast('网络无法连接');
		}
	});
}

//对数据分段加密
function segmentEncrypt(enStr, publikKey) {
	window.rsa = new JSEncrypt();
	window.rsa.setPublicKey(publikKey);
	var result = [],
		offSet = 0,
		cache = '',
		i = 1,
		inputLen = enStr.length;

	while(inputLen - offSet > 0) {
		if(inputLen - offSet > 117) {
			cache = window.rsa.encrypt(enStr.substring(offSet, 117 * i));
		} else {
			cache = window.rsa.encrypt(enStr.substring(offSet));
		}
		// 加密失败返回原数据
		if(cache === false) {
			return enStr;
		}
		offSet = i++ * 117;

		result.push(cache);
	}
	return JSON.stringify(result);
}