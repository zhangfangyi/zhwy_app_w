 mui.plusReady(function() {
 	mui('.mui-scroll').on('tap', '.ddzl', function(e) {
 		var url = $(this).attr("url");
 		var id = $(this).attr("id");
 		mui.openWindow({
 			url: url,
 			styles: {
 				popGesture: "close"
 			},
 			show: {
 				aniShow: "pop-in"
 			},
 			extras: {
 				"xqId": id
 			},
 			waiting: {
 				autoShow: false
 			}
 		});
 	});
 	var merchantCode = plus.webview.currentWebview().code;
 	var code = 0;

 	//选取初始化订单按钮			
 	$(".wdddList").each(function(k, val) {
 		var listCode = $(val).attr("code");
 		if(listCode == code) {
 			$(val).addClass("wdddSelect");
 		} else {
 			$(val).removeClass("wdddSelect");
 		}
 	})
 	//渲染列表
 	var cardNumber = localStorage.getItem("cardnumber");
 	getDatas(code, cardNumber,merchantCode);
 	var count = 2;
 	//按钮切换
 	$(".wddd").on('tap', function() {
 		$(this).find(".wdddList").addClass("wdddSelect");
 		$(this).siblings().find(".wdddList").removeClass("wdddSelect");
 		var code = $(this).find(".wdddList").attr("code");
 		mui('.mui-scroll-wrapper').scroll().scrollTo(0, 0, 100);
 		mui('#wdddList').pullToRefresh().refresh(true);
 		count = 2;
 		getDatas(code, cardNumber,merchantCode);

 	});
 	var deceleration = mui.os.ios ? 0.003 : 0.0009;
 	mui('.mui-scroll-wrapper').scroll({
 		deceleration: deceleration,
 		indicators: false
 	});
 	mui('#wdddList').pullToRefresh({
 		up: {
 			callback: function() {
 				var code = $(".wdddSelect").attr("code");
 				var self = this;
 				setTimeout(function() {
 					var list = $(".wdddxq").size();
 					addDatas(code, list, cardNumber, count, self,merchantCode);
 					count++;
 				}, 400);
 			}
 		}
 	});

 	//查看物流点击事件
 	mui('#wdddList').on('tap', '.ckwl', function(e) {
 		return;
 		var id = $(this).attr("id");
 		if(id) {
 			mui.openWindow({
 				url: viewLogistics + id,
 				styles: {
 					popGesture: "close"
 				},
 				show: {
 					aniShow: "pop-in"
 				},
 				extras: {},
 				waiting: {
 					autoShow: false
 				}
 			});
 		} else {
 			mui.toast("暂无物流信息");
 		}
 	});

 	//查看物流点击事件
 	mui('#wdddList').on('tap', '.btn-orderCancel', function(e) {
 		e.stopPropagation();
 		var ele = $(this);
 		var orderId = ele.attr("data-orderid");
 		var merchantCode = ele.attr("data-merchantcode");
 		var btnArray = ['是', '否'];
 		mui.confirm('确定要取消订单吗？', '提示', btnArray, function(e) {
 			if(e.index == 0) {
 				mui.ajax(ajaxDomain + "/orderInfo/cancelOrder.do", {
 					data: {
 						orderId: orderId,
 						merchantCode: merchantCode
 					},
 					dataType: 'json',
 					type: 'post', //HTTP请求类型
 					success: function(data) {
 						if(data.result == 'success') {
 							mui.toast(data.resume);
 							ele.parents('.wdddxq').remove();
 						} else {
 							mui.toast(data.resume);
 						}
 					}
 				})
 			} else {

 			}
 		})

 	});

 });

 function addDatas(code, list, cardNumber, count, self,merchantCode) {
 	mui.ajax(ajaxDomain + "/orderInfo/getOrderInfo.do", {
 		data: {
 			orderStatus: code,
 			cardNumber: cardNumber,
 			merchantCode:merchantCode,
 			page: count,
 			limit: 10,
 		},
 		dataType: 'json',
 		type: 'post', //HTTP请求类型
 		success: function(data) {
   			console.log(JSON.stringify(data))
 			var Number = data.count;
 			var listData = data.data;
 			if(listData) {
 				for(var i = 0; i < listData.length; i++) {
 					var str = '<div class="mui-row wdddxq">' +
 						'<div class="mui-row wdstatus">' +
 						'<span>' + listData[i].DATETIME + '</span>' +
 						'<span style="color: #fc7639;">' + listData[i].STATUS + '</span>' +
 						'</div>' +
 						'<div class="mui-row">' +
 						rows(listData[i].GOODSDATA) +
 						'</div>' +
 						'<div class="mui-row sfg">' +
 						'<span>共<i>' + listData[i].GOODSNUM + '</i>件商品</span>' +
 						'<span>实付款：' + listData[i].PAYSUM + '</span>' +
 						'</div>' +
 						'<div class="mui-row ckwl" id=' + listData[i].LOGISTICID + '>' + listData[i].OPERASTATUS +
 						(listData[i].isShowCancel == 1 ? '<em class="btn-orderCancel" data-merchantcode="' + listData[i].MERCHANTCODE + '" data-orderid="' + listData[i].GOODSDATA[0].ID + '">取消</em>' : '') +
 						'</div>' +
 						'<div class="mui-row wlyy"></div>' +
 						'</div>'
 					$("#wdddList .listItem").append(str);
 				}
 			}

 			if(list >= Number) {
 				self.endPullUpToRefresh(true);
 			} else {
 				self.endPullUpToRefresh(false);
 			}
 		},
 		error: function(xhr, type, errorThrown) {
 			mui.toast('网络无法连接', {
 				duration: 'long',
 				type: 'div'
 			});
 		}
 	});
 }

 function getDatas(code, cardNumber,merchantCode) {
 	mui.ajax(ajaxDomain + "/orderInfo/getOrderInfo.do", {
 		data: {
 			orderStatus: code,
 			cardNumber: cardNumber,
 			merchantCode: merchantCode,
 			page: 1,
 			limit: 10,
 		},
 		dataType: 'json',
 		type: 'post', //HTTP请求类型
 		success: function(data) {
 			console.log(JSON.stringify(data))
 			$("#wdddList .listItem").html('');
 			$("#wdddList .listIt").html('');
 			var listData = data.data;
 			if(!listData) {
 				$("#wdddList .listIt").html('暂无订单信息');
 				return;
 			}
 			for(var i = 0; i < listData.length; i++) {
 				var str = '<div class="mui-row wdddxq">' +
 					'<div class="mui-row wdstatus">' +
 					'<span>' + listData[i].DATETIME + '</span>' +
 					'<span style="color: #fc7639;">' + listData[i].STATUS + '</span>' +
 					'</div>' +
 					'<div class="mui-row">' +
 					rows(listData[i].GOODSDATA) +
 					'</div>' +
 					'<div class="mui-row sfg">' +
 					'<span>共<i>' + listData[i].GOODSNUM + '</i>件商品</span>' +
 					'<span>实付款：' + listData[i].PAYSUM + '</span>' +
 					'</div>' +
 					'<div class="mui-row ckwl" id=' + listData[i].LOGISTICID + '>' + listData[i].OPERASTATUS +
 					(listData[i].isShowCancel == 1 ? '<em class="btn-orderCancel" data-merchantcode="' + listData[i].MERCHANTCODE + '" data-orderid="' + listData[i].GOODSDATA[0].ID + '">取消</em>' : '') +
 					'</div>' +
 					'<div class="mui-row wlyy"></div>' +
 					'</div>'
 				$("#wdddList .listItem").append(str);
 			}
 		},
 		error: function(xhr, type, errorThrown) {
 			mui.toast('网络无法连接', {
 				duration: 'long',
 				type: 'div'
 			});
 		}
 	});
 }

 function rows(data) {
 	var str = '';
 	for(var k = 0; k < data.length; k++) {
 		str += '<div class="ddzl" url="ddxq.html" id=' + data[k].ID + '>' +
 			'<div class="ddzlLeft">' +
 			'<div class="ddzlLeftImg" style="background:url(' + ajaxDomain + data[k].PHOTONAME + ') no-repeat;background-size: cover"></div>' +
 			'</div>' +
 			'<div class="ddzlRight">' +
 			'<ul>' +
 			'<li>' + data[k].GOODNAME + '</li>' +
 			'<li>' + data[k].NUMS + data[k].NORMS + '</li>' +
 			'<li>' + data[k].PRICE + '<span>×' + data[k].BAYCOUNT + '</span></li>' +
 			'</ul>' +
 			'</div>' +
 			'</div>';
 	}
 	return str;
 }