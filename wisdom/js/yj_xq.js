mui.plusReady(function () {
	var id = plus.webview.currentWebview().id;  
	var focus = plus.webview.currentWebview().focus;  
	var lCode = plus.webview.currentWebview().lCode;
	
	var baseId;
	var lbase = plus.webview.currentWebview().basid;
	if(lbase){
		baseId = lbase;
	}else{
		baseId = localStorage.getItem("baseId");
	}
	
			var nativeWebview, imm, InputMethodManager;
			var initNativeObjects = function() {
				if (mui.os.android) {
					var main = plus.android.runtimeMainActivity();
					var Context = plus.android.importClass("android.content.Context");
					InputMethodManager = plus.android.importClass("android.view.inputmethod.InputMethodManager");
					imm = main.getSystemService(Context.INPUT_METHOD_SERVICE);
				} else {
					nativeWebview = plus.webview.currentWebview().nativeInstanceObject();
				}
			};
			var showSoftInput = function() {
				if (mui.os.android) {
					imm.toggleSoftInput(0, InputMethodManager.SHOW_FORCED);
				} else {
					nativeWebview.plusCallMethod({
						"setKeyboardDisplayRequiresUserAction": false
					});
				}
				setTimeout(function() {
					var inputElem = document.querySelector('.leaderDivRow1');
					inputElem.focus();
					inputElem.parentNode.classList.add('mui-active'); //第一个是search，加上激活样式
				}, 200);
		};
	
	
//				initNativeObjects();
//				showSoftInput();//默认弹出键盘
				


var u = navigator.userAgent;
 var isIOS = !!u.match(/\(i[^;]+;( U;)? CPU.+Mac OS X/);
  if (isIOS) {
  	var originalHeight=document.documentElement.clientHeight || document.body.clientHeight;
    window.onresize=function(){
        var  resizeHeight=document.documentElement.clientHeight || document.body.clientHeight;
        //软键盘弹起与隐藏  都会引起窗口的高度发生变化
        if(resizeHeight*1<originalHeight*1){ //resizeHeight<originalHeight证明窗口被挤压了
        	mui.scrollTo("10%",0)
//		       　　  plus.webview.currentWebview().setStyle({
//		             　　 height:originalHeight
//		          　});
        }
    }


	plus.webview.currentWebview().setStyle({
	　　softinputMode: "adjustResize"  // 弹出软键盘时自动改变webview的高度
	});
  	
  }



	
	if(!focus){ 
		$(".leaderDivRow1").attr("placeholder","请输入");
	}
	var createId = localStorage.getItem("userId");
	var createName = localStorage.getItem("userName");
	var type;
	var role = localStorage.getItem("role");
	
	if(role.indexOf("基地经理") != -1 || role.indexOf("管理员") != -1 || role.indexOf("基地领导") != -1){
		type = 1;
	}else{
		type = 0;
	}
		mui.ajax(ajaxDomain + "/complain/getComplainDetails.do",{
					data:{
						id : id
					},
   					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						var infoData = data.data.info;
						//创建头部内容
						$(".topRow1_span1").html("报修人: "+ infoData.username);
						$(".topRow2_span1").html(infoData.createtime);
						$(".topRow3_span1").html("隶属基地: "+infoData.basename);
						//创建中间内容
						$(".centerRow1").html(infoData.content);
						
						var filesData = data.data.files;
						for(var i=0;i<filesData.length;i++){
							var img = $("<img src='"+ajaxDomain+filesData[i].photoname+"'>");
							$(".centerImg").append(img);
						}
						
						var answersData = data.data.answers;
						var answerIds = []; 
						for(var i=0;i<answersData.length;i++){
							var li ='<li class="bottomLi">'+
								'<div class="li_left">'+
									'<img src='+ ajaxDomain + answersData[i].photoname +'/>'+
								'</div>'+ 
								'<div class="li_right">'+
									'<div class="li_right_row1">'+
										'<span class="li_right_span1">'+answersData[i].createname+'</span>'+
										'<span class="li_right_span2">'+userSelect(answersData[i].type)+'</span>'+
										'<span class="li_right_span3">'+answersData[i].createtime+'</span>'+
									'</div>'+
									'<p class="li_right_row2">'+answersData[i].content+'</p>'+
								'</div>'+
							'</li>';
							$(".bottomUl").append(li);	
							if(answersData[i].type == 1){
								answerIds.push(answersData[i].id);
							}
						}
						readAnswer(createId,answerIds,id);
						
					},
					error:function(xhr,type,errorThrown){
						mui.toast('网络无法连接', {
								duration: 'long', 
								type: 'div'
							});
					}
				});
	
	
	
	mui('.assessDivs').on('tap', '#fabuText', function(e) {
		var text = $(".leaderDivRow1").val();
		var content;
		if(text == ''){
			content = $(".leaderDivRow1").attr("placeholder");
		}else{
			content = $(".leaderDivRow1").val();
		} 
		var data = {
							cnId:id,
							createId:createId,
							createName:createName,
							type:type,
							content:content,
					};
		mui.ajax(ajaxDomain + "/complain/addComplainAnswer.do",{
					data:{
						data : JSON.stringify(data)
					},
   					dataType:'json', 
					type:'post',//HTTP请求类型
					async:false,
					success:function(data){ 
						var phoneName = localStorage.getItem("phoneName");
						var data = data.data.obj;
				    var li ='<li class="bottomLi">'+
								'<div class="li_left">'+
									'<img src='+ ajaxDomain + phoneName +'/>'+
								'</div>'+
								'<div class="li_right">'+
									'<div class="li_right_row1">'+
										'<span class="li_right_span1">'+data.createname+'</span>'+
										'<span class="li_right_span2">'+userSelect(data.type)+'</span>'+
										'<span class="li_right_span3">'+data.createtime+'</span>'+
									'</div>'+
									'<p class="li_right_row2">'+data.content+'</p>'+
								'</div>'+
							'</li>';
							  
					$(".bottomUl").append(li);		
					mui('.mui-scroll-wrapper').scroll().reLayout();
					mui('.mui-scroll-wrapper').scroll().scrollToBottom(100);   
					$(".leaderDivRow1").val(""); 
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('网络无法连接', {
							duration: 'long', 
							type: 'div'
						});
					} 
			});
		
	})
	
	 
		mui.init({
			swipeBack: true, //启用右滑关闭功能
			beforeback: function() {
		        var list = plus.webview.currentWebview().opener();
		        var returnBaseId = baseId;
		        mui.fire(list, 'refresh1',{
		        	val:lCode,
		        	rbase:returnBaseId
		        });
		        //返回true,继续页面关闭逻辑
		        return true;
			} 
			
		});
		var deceleration = mui.os.ios ? 0.003 : 0.0009;
		mui('.mui-scroll-wrapper').scroll({
			deceleration: deceleration,
			indicators: false
		});
		
		//返回按钮
		mui('.smailNav').on('tap', '.back', function(e) {
			mui.back();
		});
	
})


function userSelect(type){
	if(type == 1){  
		return '<span style="color:#18be04;border:1px solid #18be04;padding: 0 0.1rem">基地经理</span>';
	}else{
		return '<span style="color:#f33306;border:1px solid #fa0e12;padding: 0 0.1rem">报修人</span>'; 
	}
}


function readAnswer(userId,answerIds,id){
				mui.ajax(ajaxDomain + "/complain/readAnswer.do",{
							data:{
								userId : userId,
								answerIds:answerIds,
								complainId:id
							},
		   					dataType:'json',
							type:'post',//HTTP请求类型
							success:function(data){ 
							},
							error:function(xhr,type,errorThrown){
								//异常处理；
								mui.toast('网络无法连接', {
									duration: 'long', 
									type: 'div'
								});
							}
						});	
	
}
