 mui.plusReady(function() {
	mui.init({
  		swipeBack: true //启用右滑关闭功能
	});
	var id = plus.webview.currentWebview().listId;  
	var userId = localStorage.getItem("userId");
	getData(id);
	readNotice(userId,id);
})
 
function getData(id){
		mui.ajax(ajaxDomain + "/noticeInfo/getDetail.do",{
					data:{
						 id:id
						},
 					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
						var scroll = $(".tzgg_cnt").html('');
						var str = $('<h1>'+data.data.title+'</h1>'+
								    '<span>'+data.data.createtime+'</span>'+
									'<p style="text-indent: 1em">'+data.data.content+'</p>');
						scroll.append(str);
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('网络无法连接', {
									duration: 'long', 
									type: 'div'
								});
					}
	});
	
}

function readNotice(userId,id){
		mui.ajax(ajaxDomain + "/noticeInfo/readNotice.do",{
					data:{
						 userId:userId,
						 noticeId:id
						},
 					dataType:'json',
					type:'post',//HTTP请求类型
					success:function(data){ 
					},
					error:function(xhr,type,errorThrown){
						//异常处理；
						mui.toast('网络无法连接', {
									duration: 'long', 
									type: 'div'
								});
					}
	});
}
